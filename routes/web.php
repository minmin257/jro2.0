<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware'=>['maintenance'], 'as'=>'front.'], function() {

    Route::get('/', 'FrontController@index')->name('index');
    Route::get('/salon', 'FrontController@salon')->name('salon');
    Route::get('/category/{category}', 'FrontController@category')->name('category');
    Route::get('/category/{category}/product/{product}', 'FrontController@product')->name('product');
    Route::get('/filter', 'FrontController@filter')->name('filter');
    Route::post('/addToCart', 'FrontController@addToCart')->name('addToCart');
    Route::get('/order', 'FrontController@order')->name('order');
    Route::post('/remittance', 'FrontController@remittance')->name('remittance');
    Route::get('/teach', 'FrontController@teach')->name('teach');
    Route::get('/cart', 'FrontController@cart')->name('cart');
    Route::post('/removeToCart','FrontController@removeToCart')->name('removeToCart');
    Route::get('/updateCart','FrontController@updateCart')->name('updateCart');
    Route::get('/note', 'FrontController@note')->name('note');
    Route::post('/validateCheckout', 'FrontController@validateCheckout')->name('validateCheckout');
    Route::get('/cart2', 'FrontController@cart2')->name('cart2');
    Route::post('/validateOrderer', 'FrontController@validateOrderer')->name('validateOrderer');
    Route::any('/complete', 'FrontController@complete')->name('complete');

});

Route::group(['prefix'=>'laravel-filemanager','middleware'=> ['web','auth']],function(){
    \UniSharp\LaravelFilemanager\Lfm::routes();
});
// 後台
// prefix URL為/webmin/?
// namespace Controllers 預設路徑為 "App\Http\Controllers\backend"
Route::group(['prefix'=>'webmin','namespace'=>'backend'], function() {
    // 登入登出
    Route::get('/','LoginController@index')->name('backend');
    Route::post('/','LoginController@session_in')->name('login');
    Route::get('/logout','LoginController@logout')->name('logout');

    // 登入後迎賓頁
    Route::group(['middleware'=>'auth:web'], function(){
        Route::get('/index','ModuleController@index')->name('backend.index');

        // 首頁管理
        Route::group(['prefix'=>'content', 'namespace'=>'content'], function() {
            // 首頁輪播
            Route::get('/home_banner', 'HomeBannerController@index')->name('backend.home_banner');
            Route::post('/home_banner', 'HomeBannerController@create')->name('create_home_banner');
            Route::post('/update_home_banner', 'HomeBannerController@update')->name('update_home_banner');
            Route::post('/delete_home_banner', 'HomeBannerController@delete')->name('delete_home_banner');


            // 產品及型錄管理
            // 分類管理ProductType
            Route::group(['middleware'=>'HasPermission:產品管理'], function() {
                // 分類管理
                Route::get('/productCategory', 'ProductCategoryController@index')->name('backend.productCategory');
                Route::post('/productCategory', 'ProductCategoryController@create')->name('create_productCategory');
                Route::post('/update_productCategory', 'ProductCategoryController@update')->name('update_productCategory');
                Route::post('/delete_productCategory', 'ProductCategoryController@delete')->name('delete_productCategory');


                // 產品管理
                Route::get('/productCategory/{category}/product', 'ProductController@index')->name('backend.product');
                Route::post('/product', 'ProductController@create')->name('create_product');
                Route::post('/update_product', 'ProductController@update')->name('update_product');
                Route::post('/delete_product', 'ProductController@delete')->name('delete_product');
                Route::get('/productCategory/{category}/product/{product}', 'ProductController@edit')->name('edit_product');
                Route::post('/update_filter', 'ProductController@update_filter')->name('update_filter');
                Route::get('/productCategory/{category}/product/{product}/photo', 'ProductController@photo')->name('category.edit_photo');
                Route::post('/photo', 'ProductController@create_photo')->name('create_photo');
                Route::post('/update_photo', 'ProductController@update_photo')->name('update_photo');
                Route::post('/delete_photo','ProductController@delete_photo')->name('delete_photo');
                Route::get('/productCategory/{category}/eyelash/{product}', 'ProductController@eyelash')->name('eyelash');
                Route::post('/option', 'ProductController@create_option')->name('create_option');
                Route::post('/delete_option', 'ProductController@delete_option')->name('delete_option');
                Route::post('/update_option', 'ProductController@update_option')->name('update_option');
                Route::get('/product/setting', 'ProductController@setting')->name('option.setting');
                Route::post('/update_setting', 'ProductController@update_setting')->name('update_setting');
                Route::get('/productList', 'ProductController@productList')->name('backend.productList');
                Route::get('/productList/{product}/photo', 'ProductController@listPhoto')->name('edit_photo');
                Route::get('/productList/{product}', 'ProductController@listEdit')->name('edit_productList');
                Route::get('/phone', 'ProductController@phone')->name('backend.phone');
                Route::post('/phone', 'ProductController@update_phoneOrder')->name('update_phoneOrder');


                // 睫毛規格管理
                Route::get('/eyelash', 'EyelashController@index')->name('backend.eyelash');
                Route::post('/eyelash', 'EyelashController@create')->name('create_type');
                Route::post('/delete_type', 'EyelashController@delete')->name('delete_type');
                Route::get('/eyelash/curl', 'EyelashController@curl')->name('backend.curl');
                Route::get('/eyelash/thickness', 'EyelashController@thickness')->name('backend.thickness');
                Route::get('/eyelash/length', 'EyelashController@length')->name('backend.length');
                Route::get('/eyelashList', 'EyelashController@eyelashList')->name('backend.eyelashList');
                Route::get('/eyelashList/{product}', 'EyelashController@detail')->name('eyelashList.detail');
            });


            // 圖片管理 - 教學
            Route::get('/teach', 'TeachController@index')->name('backend.teach');
            Route::post('/teach', 'TeachController@update')->name('update_teach');


            // 圖片管理 - 活動訊息
            Route::get('/news', 'NewsController@index')->name('backend.news');
            Route::post('/news', 'NewsController@update')->name('update_news');


            // 圖片管理 - 頁尾
            Route::get('/footer', 'FooterController@index')->name('backend.footer');
            Route::post('/footer', 'FooterController@update')->name('update_footer');

        });


        // 購物管理
        Route::group(['prefix'=>'shopping'], function() {
            Route::get('/setting', 'ShoppingController@index')->name('shopping.free');
            Route::post('/update_free', 'ShoppingController@update')->name('update_free');
            Route::get('/shipping', 'ShoppingController@shipping')->name('shopping.shipping');
            Route::post('/update_shipping', 'ShoppingController@update_shipping')->name('update_shipping');
            Route::get('/receipt', 'ShoppingController@receipt')->name('shopping.receipt');
            Route::post('/update_receipt', 'ShoppingController@update_receipt')->name('update_receipt');
            Route::get('/expired', 'ShoppingController@expired')->name('shopping.expired');
            Route::post('/expired', 'ShoppingController@update_expired')->name('update_expired');
            Route::get('/note', 'ShoppingController@note')->name('shopping.note');
            Route::post('/note', 'ShoppingController@update_note')->name('update_note');
            Route::get('/complete', 'ShoppingController@complete')->name('shopping.complete');
            Route::post('/complete', 'ShoppingController@update_complete')->name('update_complete');
        });


        // 訂單管理
        Route::group(['prefix'=>'order'], function() {
            Route::get('/', 'OrderController@index')->name('backend.order');
            Route::get('/arrearage', 'OrderController@arrearage')->name('backend.arrearage');
            Route::get('/shipment', 'OrderController@shipment')->name('backend.shipment');
            Route::get('/completed', 'OrderController@completed')->name('backend.completed');
            Route::get('/overdue', 'OrderController@overdue')->name('backend.overdue');
            Route::get('/cancel', 'OrderController@cancel')->name('backend.cancel');
            Route::get('/detail/{order}', 'OrderController@edit')->name('edit_order');
            Route::post('/', 'OrderController@update')->name('update_order');
            Route::get('/{id}/remittance', 'OrderController@remittance')->name('backend.remittance');
            Route::post('/update_remittance', 'OrderController@update_remittance')->name('update_remittance');
            Route::get('/export_shipment/{order}', 'OrderController@export_shipment')->name('export_shipment');
        });


        // 權限管理
        Route::group(['prefix'=>'authority'], function() {
            Route::get('/profile', 'AuthorityController@index')->name('profile');
            Route::post('/profile', 'AuthorityController@update')->name('update_profile');
            Route::get('/', 'AuthorityController@management')->name('management');
            Route::post('/', 'AuthorityController@create')->name('create_user');
            Route::get('/profile/{user}/edit', 'AuthorityController@edit')->name('edit_user');
            Route::post('/delete_account', 'AuthorityController@delete')->name('delete_account');
            Route::get('/permission/{user}/edit', 'AuthorityController@permission')->name('permission');
            Route::post('/permission', 'AuthorityController@create_permission')->name('create_permission');
        });


        // 系統設定
        Route::group(['prefix'=>'system'], function() {
            Route::get('/', 'SystemController@index')->name('system');
            Route::post('/', 'SystemController@update')->name('update_system');
            Route::get('/mail_setting','SystemController@mail_setting')->name('mail_setting');
            Route::post('/update_mail_account','SystemController@update_mail_account')->name('update_mail_account');
        });
    });


});
