<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/execution/storage-link',function(){
    // $file_path = public_path('storage');
    // if (File::exists($file_path)) File::deleteDirectory($file_path);    
    Artisan::call('storage:link');        
    return response()->json([
        'result' => true  
    ]);;    
});

Route::get('/execution/clean-cache',function(){  
    Artisan::call('cache:clear');  
	Artisan::call('config:clear');  
	Artisan::call('config:cache');  
    return response()->json([
        'result' => true  
    ]);;    
});