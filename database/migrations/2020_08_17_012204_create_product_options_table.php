<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_options', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_id')->constrained()->onDelete('cascade');
            $table->foreignId('curl_id')->nullable()->constrained('curl_types')->onDelete('cascade');
            $table->foreignId('thickness_id')->nullable()->constrained('thickness_types')->onDelete('cascade');
            $table->foreignId('length_id')->nullable()->constrained('length_types')->onDelete('cascade');
            $table->boolean('state')->default(1)->comment('啟用狀態(0關閉/1顯示)');
            $table->boolean('out')->default(0)->comment('完售(0關閉/1顯示)');
            $table->boolean('delete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eyelashes');
    }
}
