<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('type_id')->constrained('order_types')->onDelete('cascade');
            $table->string('number')->comment('訂單編號');
            $table->string('buyer')->comment('購買人');
            $table->string('buyer_tel');
            $table->string('buyer_addr');
            $table->string('email');
            $table->string('consignee')->comment('收貨人');
            $table->string('consignee_tel');
            $table->string('consignee_addr');
            $table->foreignId('receipt_id')->constrained('receipt_types')->onDelete('cascade');
            $table->string('receipt_number')->nullable()->comment('發票號碼');
            $table->string('title')->nullable()->comment('公司抬頭');
            $table->string('VAT')->nullable()->comment('統一編號');
            $table->string('contribute')->nullable()->comment('捐贈對象');
            $table->string('price')->comment('訂單金額');
            $table->string('discount')->nullable()->comment('折扣後金額');
            $table->foreignId('shipping_id')->constrained()->onDelete('cascade');
            $table->foreignId('payment_id')->constrained('payment_methods')->onDelete('cascade');
            $table->string('total')->comment('總計金額');
            $table->text('demand')->nullable()->comment('需求備註');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
