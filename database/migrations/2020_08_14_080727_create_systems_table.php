<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSystemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('systems', function (Blueprint $table) {
            $table->id();
            $table->string('sitename')->comment('網站名稱');
            $table->string('meta_description');
            $table->string('meta_keyword');
            $table->boolean('maintain')->default(0)->comment('0關閉/1維護');
            $table->string('message')->comment('維護訊息');
            $table->unsignedInteger('expired')->comment('訂單逾期天數');
            $table->boolean('receipt')->default(0)->comment('發票設定(0關閉/1顯示)');
            $table->string('free')->comment('免運標準');
            $table->string('percent')->comment('滿額折扣(百分比)');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('systems');
    }
}
