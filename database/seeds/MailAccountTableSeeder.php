<?php

use Illuminate\Database\Seeder;
use App\MailAccount;

class MailAccountTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MailAccount::create([
            'subject'=>'我是主旨 主旨 主旨',
            'name'=>'我是寄件人名稱',
            'email'=>'your@gmail.com',
            'password'=>encrypt('xxxxxx'),
            'html'=>'<p>感謝您<br>我們已收到您的訂單, 在您於五日之內完成付款動作,<br>匯款完畢請填寫匯款單、我們會於三日內為您出貨。</p>'
        ]);
    }
}
