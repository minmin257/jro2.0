<?php

use Illuminate\Database\Seeder;
use App\ReceiptType;

class ReceiptTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ReceiptType::create([
            'title'=>'不開立發票'
        ]);
        ReceiptType::create([
            'title'=>'二聯式發票'
        ]);
        ReceiptType::create([
            'title'=>'三聯式發票'
        ]);
        ReceiptType::create([
            'title'=>'捐贈發票'
        ]);
    }
}
