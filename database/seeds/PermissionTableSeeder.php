<?php

use Illuminate\Database\Seeder;
use App\Permission;
use App\UserHasPermission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create([
            'title'=>'產品管理',
        ]);
        Permission::create([
            'title'=>'訂單管理',
        ]);
        Permission::create([
            'title'=>'權限管理',
        ]);
        Permission::create([
            'title'=>'網站設定管理',
        ]);

        for($i=1;$i<=2;$i++)
        {
            for($j=1;$j<=4;$j++)
            {
                UserHasPermission::create([
                    'user_id'=>$i,
                    'permission_id'=>$j,
                ]);
            }
        }

    }
}
