<?php

use Illuminate\Database\Seeder;
use App\CurlType;

class CurlTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CurlType::create([
            'title'=>'J'
        ]);
        CurlType::create([
            'title'=>'B'
        ]);
        CurlType::create([
            'title'=>'C'
        ]);
        CurlType::create([
            'title'=>'CC'
        ]);
    }
}
