<?php

use Illuminate\Database\Seeder;
use App\Footer;

class FooterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Footer::create([
            'title'=>'JRO美睫商材株式會社',
            'seal'=>'/images/seal.png',
            'src'=>'/images/phonenumber.jpg',
            'line'=>'http://line.me/ti/p/LOkJ4o4AKe',
            'tel'=>'0985-121-940'
        ]);
    }
}
