<?php

use Illuminate\Database\Seeder;
use App\PhoneOrder;

class PhoneOrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PhoneOrder::create([
            'category_id'=>2,
            'html'=>'<h4 class="product-price">
                              此商品限電話下單
                            </h4>
                            456789'
        ]);
    }
}
