<?php

use Illuminate\Database\Seeder;
use App\Teach;

class TeachTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Teach::create([
            'title'=>'初階教學',
            'src'=>'https://api.fnkr.net/testimg/800x800/00CED1/FFF/'
        ]);
        Teach::create([
            'title'=>'中階教學',
            'src'=>'https://api.fnkr.net/testimg/800x800/00CED1/FFF/'
        ]);
        Teach::create([
            'title'=>'高階教學',
            'src'=>'https://api.fnkr.net/testimg/800x800/00CED1/FFF/'
        ]);
    }
}
