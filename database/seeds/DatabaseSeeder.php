<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(SubjectTableSeeder::class);
        $this->call(HomeBannerTableSeeder::class);
        $this->call(FooterTableSeeder::class);
        $this->call(TeachTableSeeder::class);
        $this->call(NewsTableSeeder::class);
        $this->call(SystemTableSeeder::class);
        $this->call(MailAccountTableSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(ShippingTableSeeder::class);
        $this->call(NoteTableSeeder::class);
        $this->call(CompleteTableSeeder::class);
        $this->call(ReceiptTypeTableSeeder::class);
        $this->call(ReceiptTableSeeder::class);
        $this->call(CurlTypeTableSeeder::class);
        $this->call(ThicknessTypeTableSeeder::class);
        $this->call(LengthTypeTableSeeder::class);
        $this->call(ProductTableSeeder::class);
        $this->call(PhoneOrderTableSeeder::class);
        $this->call(PaymentMethodTableSeeder::class);
        $this->call(OrderTypeTableSeeder::class);
    }
}
