<?php

use Illuminate\Database\Seeder;
use App\System;

class SystemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        System::create([
            'sitename'=>'JRO美睫商材株式會社',
            'meta_description'=>'JRO美睫商材株式會社',
            'meta_keyword'=>'美睫商材',
            'message'=>'本系統於進行設備維護，暫時無法提供服務。造成不便，敬請見諒。',
            'expired'=>5,
            'free'=>2000,
            'percent'=>90
        ]);
    }
}
