<?php

use Illuminate\Database\Seeder;
use App\Note;

class NoteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Note::create([
            'html'=>'<div class="row justify-content-center">
                <div class="col-12 col-lg-10">
                  <table class="table" style="border:2px solid #eab133;color:#ffffff;">
                    <tbody>
                      <tr>
                        <td style="border:1px solid #eab133;text-align:center;vertical-align:middle;" rowspan="2">
                          台灣本島
                        </td>
                        <td style="border:1px solid #eab133;text-align:center;vertical-align:middle;">滿2000元</td>
                        <td style="border:1px solid #eab133;text-align:center;vertical-align:middle;">現打9折 / 免運費 (不含沙龍設備)</td>
                      </tr>
                      <tr>
                        <td style="border:1px solid #eab133;text-align:center;vertical-align:middle;">未滿2000元</td>
                        <td style="border:1px solid #eab133;text-align:center;vertical-align:middle;">
                          台灣本島：運費100元/單次<br>
                          宜蘭、花蓮、台東、屏東、離島：運費200元/單次
                        </td>
                      </tr>
                      <tr>
                        <td style="border:1px solid #eab133;text-align:center;vertical-align:middle;" colspan="3">
                          ◆貨到付款小提醒◆<br>
                          (1) 單筆費用不得超過一萬元。<br>
                          (2) 貨到付款：購物滿2000或不滿2000、皆酌收30元手續費。
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <div style="margin-bottom: 20px;">
                    ATM轉帳資料
                    <ul style="list-style: none;padding:0 15px;">
                      <li>玉山銀行(代碼808)</li>
                      <li>轉入帳號 0989-979-100546</li>
                      <li>轉帳後、請務必填寫匯款單回傳通知(於首頁右上方)、收到款項後我們隨即安排出貨。</li>
                    </ul>
                  </div>
                  <div>
                    <p class="cart-info-important c-da9c12">
                      <img src="/images/list_icon.png" alt="">
                      配送時間約莫3天左右(不含假日)、若貨運因排班問題、或天候等外在因素而有所延遲、還請客戶能有體諒之心再行下單以避免爭議。
                    </p>
                    <p class="cart-info-important c-da9c12">
                      <img src="/images/list_icon.png" alt="">
                      因應一例一休勞動新制、貨運公司遇假日、國定假日、補假、一律停止集貨與配送服務。
                    </p>
                    <p class="cart-info-important c-da9c12">
                      <img src="/images/list_icon.png" alt="">
                      貨品配達後、如商品有瑕疵或品項有誤、辦理退換貨請在收到貨品七天內寄還商品、經我方判定是我們的缺失、運費無條件由我方吸收；若未出現上述條件而要求退換貨品、運費則由買方支付。
                    </p>
                  </div>
                </div>
              </div>'
        ]);
    }
}
