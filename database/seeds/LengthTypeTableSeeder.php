<?php

use Illuminate\Database\Seeder;
use App\LengthType;

class LengthTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LengthType::create([
            'title'=>8
        ]);
        LengthType::create([
            'title'=>9
        ]);
        LengthType::create([
            'title'=>10
        ]);
        LengthType::create([
            'title'=>11
        ]);
        LengthType::create([
            'title'=>12
        ]);
        LengthType::create([
            'title'=>13
        ]);
        LengthType::create([
            'title'=>14
        ]);
        LengthType::create([
            'title'=>'綜合'
        ]);
    }
}
