<?php

use Illuminate\Database\Seeder;
use App\HomeBanner;

class HomeBannerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        HomeBanner::create([
            'src'=>'/images/banner_01.jpg',
            'mobile_src'=>'/images/banner_01s.jpg',
        ]);
        HomeBanner::create([
            'src'=>'/images/banner_04.jpg',
            'mobile_src'=>'/images/banner_04s.jpg',
        ]);
        HomeBanner::create([
            'src'=>'/images/banner_03.jpg',
            'mobile_src'=>'/images/banner_03s.jpg',
        ]);
    }
}
