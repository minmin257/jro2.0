<?php

use Illuminate\Database\Seeder;
use App\Shipping;

class ShippingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Shipping::create([
            'title'=>'台灣本島',
            'fee'=>100
        ]);
        Shipping::create([
            'title'=>'宜蘭,花蓮,台東,離島地區',
            'fee'=>200
        ]);
        Shipping::create([
            'title'=>'滿額現打9折 免運費',
            'fee'=>0
        ]);
    }
}
