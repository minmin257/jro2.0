<?php

use Illuminate\Database\Seeder;
use App\OrderType;

class OrderTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OrderType::create([
            'title'=>'未付款訂單'
        ]);
        OrderType::create([
            'title'=>'逾期未付款訂單'
        ]);
        OrderType::create([
            'title'=>'取消訂單'
        ]);
        OrderType::create([
            'title'=>'待出貨訂單'
        ]);
        OrderType::create([
            'title'=>'完成訂單'
        ]);
    }
}
