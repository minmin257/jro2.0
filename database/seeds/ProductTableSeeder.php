<?php

use Illuminate\Database\Seeder;
use App\ProductCategory;
use App\Product;
use App\ProductOption;
use App\ProductPhoto;
use App\Curl;
use App\Thickness;
use App\Length;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductCategory::create([
            'title'=>'睫毛',
            'src'=>'/images/menu_btn_01.png',
        ]);
        ProductCategory::create([
            'title'=>'沙龍設備',
            'src'=>'/images/menu_btn_06.png',
        ]);
        ProductCategory::create([
            'title'=>'黑膠',
            'src'=>'/images/menu_btn_02.png',
        ]);
        ProductCategory::create([
            'title'=>'植睫專用夾',
            'src'=>'/images/menu_btn_03.png',
        ]);
        ProductCategory::create([
            'title'=>'工具、耗材',
            'src'=>'/images/menu_btn_04.png',
        ]);
        ProductCategory::create([
            'title'=>'保養類',
            'src'=>'/images/menu_btn_05.png',
        ]);

        Product::create([
            'category_id'=>1,
            'title'=>'夜幕黑。霧黑睫毛',
            'spec'=>'長25寬16高20cm',
            'html'=>'<p>
                              分層式箱體設計、利於分類、斜梯開放式雙層收納盤、讓所有的材  料一次展開便於拿取、簡約大方的外型、可說是專業與時尚感兼具。
                              分層式箱體設計、利於分類、斜梯開放式雙層收納盤、讓所有的材  料一次展開便於拿取、簡約大方的外型、可說是專業與時尚感兼具。
                            </p>',
            'price'=>1200,
        ]);
        Product::create([
            'category_id'=>2,
            'title'=>'專業級手提化妝箱',
            'spec'=>'長25寬16高20cm',
            'html'=>'<p>
                              分層式箱體設計、利於分類、斜梯開放式雙層收納盤、讓所有的材  料一次展開便於拿取、簡約大方的外型、可說是專業與時尚感兼具。
                              分層式箱體設計、利於分類、斜梯開放式雙層收納盤、讓所有的材  料一次展開便於拿取、簡約大方的外型、可說是專業與時尚感兼具。
                            </p>',
            'price'=>1200,
        ]);
        Product::create([
            'category_id'=>3,
            'title'=>'S+超速乾持久膠',
            'spec'=>'長25寬16高20cm',
            'html'=>'<p>
                              分層式箱體設計、利於分類、斜梯開放式雙層收納盤、讓所有的材  料一次展開便於拿取、簡約大方的外型、可說是專業與時尚感兼具。
                              分層式箱體設計、利於分類、斜梯開放式雙層收納盤、讓所有的材  料一次展開便於拿取、簡約大方的外型、可說是專業與時尚感兼具。
                            </p>',
            'price'=>1200,
        ]);

        ProductOption::create([
            'product_id'=>1,
            'curl_id'=>1,
            'thickness_id'=>1,
            'length_id'=>1
        ]);
        ProductOption::create([
            'product_id'=>1,
            'curl_id'=>1,
            'thickness_id'=>2,
            'length_id'=>1
        ]);
        ProductOption::create([
            'product_id'=>1,
            'curl_id'=>1,
            'thickness_id'=>2,
            'length_id'=>2
        ]);
        ProductOption::create([
            'product_id'=>2,
        ]);
        ProductOption::create([
            'product_id'=>3,
        ]);

        ProductPhoto::create([
            'product_id'=>1,
            'src'=>'/images/product_item_07.png',
        ]);
        ProductPhoto::create([
            'product_id'=>2,
            'src'=>'/images/product_item_01.png',
        ]);
        ProductPhoto::create([
            'product_id'=>2,
            'src'=>'/images/product_item_02.png',
        ]);
        ProductPhoto::create([
            'product_id'=>3,
            'src'=>'/images/product_item_05s.png',
        ]);

        Curl::create([
            'product_id'=>1,
            'curl_id'=>1,
        ]);

        Thickness::create([
            'product_id'=>1,
            'thickness_id'=>1,
        ]);
        Thickness::create([
            'product_id'=>1,
            'thickness_id'=>2,
        ]);

        Length::create([
            'product_id'=>1,
            'length_id'=>1,
        ]);
        Length::create([
            'product_id'=>1,
            'length_id'=>2,
        ]);
    }
}
