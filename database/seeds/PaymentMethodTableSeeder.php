<?php

use Illuminate\Database\Seeder;
use App\PaymentMethod;

class PaymentMethodTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaymentMethod::create([
            'title'=>'ATM轉帳(轉帳後請填寫匯款單)',
            'fee'=>0
        ]);
        PaymentMethod::create([
            'title'=>'貨到付款(酌收30元手續費)',
            'fee'=>30
        ]);
    }
}
