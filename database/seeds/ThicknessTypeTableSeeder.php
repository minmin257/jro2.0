<?php

use Illuminate\Database\Seeder;
use App\ThicknessType;

class ThicknessTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ThicknessType::create([
            'title'=>'0.05'
        ]);
        ThicknessType::create([
            'title'=>'0.15'
        ]);
        ThicknessType::create([
            'title'=>'0.2'
        ]);
    }
}
