<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'account'=>'admin',
            'password'=>bcrypt('123456'),
            'name'=>'管理員',
        ]);
        User::create([
            'account'=>'yuanpu',
            'password'=>bcrypt('123456'),
            'name'=>'沅樸',
        ]);
    }
}
