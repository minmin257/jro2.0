<?php

use Illuminate\Database\Seeder;
use App\Complete;

class CompleteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Complete::create([
            'html'=>'<p class="cart-info-important">
                <img src="/images/list_icon.png" alt="">
                ATM轉帳資料<br>
                玉山銀行(代碼808)<br>
                轉入帳號 0989-979-100546<br>
                轉帳後、請務必<a href="order.html">填寫匯款單回傳通知(於首頁右上方)</a>、收到款項後我們隨即安排出貨。
              </p>
              <p class="cart-info-important">
                <img src="/images/list_icon.png" alt="">
                配送時間約莫3天(不含假日)、若貨運因排班問題、或天候等外在因素而有所延遲、還請客戶能有體諒之心、避免爭議、也請相信這不會是我們所樂見。
              </p>
              <p class="cart-info-important">
                <img src="/images/list_icon.png" alt="">
                貨品配達後、如商品有瑕疵或品項有誤、辦理退換貨請在七天內寄還商品、經我方判定是我們的缺失、運費無條件由我方吸收；若未出現上述條件而要求退換貨品、運費則由買方支付。
              </p>'
        ]);
    }
}
