<?php

use Illuminate\Database\Seeder;
use App\Subject;

class SubjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subject::create([
            'title'=>'商品',
            'tag'=>'product'
        ]);
        Subject::create([
            'title'=>'填寫匯款單',
            'tag'=>'order'
        ]);
        Subject::create([
            'title'=>'教學',
            'tag'=>'teach'
        ]);
        Subject::create([
            'title'=>'活動訊息',
            'tag'=>'news'
        ]);
        Subject::create([
            'title'=>'購物車',
            'tag'=>'cart'
        ]);
    }
}
