@inject('MailAccount' ,'App\Presenters\MailPresenter')
@component('mail::message')
{!! $MailAccount->getMailAccount()->html !!}

@component('mail::table')
|           |                                      |
| :-------- | :----------------------------------- |
| 訂單編號:  | {{ $order['number'] }}               |
| 購買人:    | {{ $order['buyer'] }}               |
| 電話:      | {{ $order['buyer_tel'] }}              |
| 地址:      | {{ $order['buyer_addr'] }}             |
| 信箱:      | {{ $order['email'] }}                  |
| 收貨人:    | {{ $order['consignee'] }}               |
| 電話:      | {{ $order['consignee_tel'] }}          |
| 地址:      | {{ $order['consignee_addr'] }}         |
| 運費:      | {{ ($order->shipping_id == 3) ? $order->shipping->title : $order->shipping->fee }} |
| 手續費:     | {{ $order->method->fee }}              |
| 訂單總計:   | {{ $order['total'] }}                   |
| 付款方式:   | {{ $order->method->title }}             |
@endcomponent

@component('mail::table')
<h4>訂單明細</h4>
{{--| 單價 | 數量 | 小計 |--}}
| 序號 | 產品名稱 | 規格 | 單價 | 數量 | 小計 |
| :---- | :-------- | :--------- | :-------- | :--------- | :--------- |
@foreach($order->detail as $key => $detail)
| {{ $key + 1 }} | {{ $detail->option->product->title }} | {{ isset($detail->option->curl_id) ? '捲度'.$detail->option->curl->title.' 粗度'
.$detail->option->thickness->title.' 長度'.$detail->option->length->title : '' }} | {{ $detail->option->product->price }} | {{ $detail->quantity }} | {{
($detail->option->product->price * $detail->quantity) }}
@endforeach
@endcomponent

@endcomponent
