@if (count($errors))
	<div class="form-group sweetalert_template" style="display: none">
		<div id="all_contacts" class="alert alert-danger blockquote">
			<ul style="margin: 0">
				@foreach($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
			</ul>
		</div>
	</div>
@endif

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script type="text/javascript">
    @if (count($errors))
        Swal.fire({
            title: '錯誤',
            html: $('.sweetalert_template #all_contacts').html(),
            icon: 'error',
            showCloseButton: true,
            showCancelButton: true,
            showConfirmButton: false,
        })
    @endif
</script>
