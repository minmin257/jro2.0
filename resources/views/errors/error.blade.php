@if (count($errors))
	<div class="form-group sweetalert_template">
		<div id="all_contacts" class="alert alert-danger blockquote">
			<ul style="margin: 0">
				@foreach($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
			</ul>
		</div>
	</div>
@endif
