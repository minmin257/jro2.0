@if(session('success'))
    <div class="form-group sweetalert_template" style="display: none">
        <div id="success" class="alert alert-success blockquote">
            {{ session('success') }}
        </div>
    </div>
@endif

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script type="text/javascript">
    @if (session('success'))
        Swal.fire({
            title: '成功',
            html: $('.sweetalert_template #success').html(),
            icon: 'success',
            showCloseButton: true,
            showCancelButton: true,
            showConfirmButton: false,
        })
    @endif
</script>
