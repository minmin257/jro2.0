@extends('front.order.default')
    @section('content')
        <form class="order-form" action="{{ route('front.remittance') }}" method="post">
            @csrf
            <div class="row justify-content-center h-100 align-items-center">
                <div class="col-12 col-md-8 col-lg-6">
                    <div class="order-title">
                        <img class="left" src="/images/flower_left.png" alt="">
                        填寫匯款單(限ATM轉帳)
                        <img class="right" src="/images/flower_right.png" alt="">
                    </div>

                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label">訂單編號</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="number" autocomplete="off">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label">手機號碼</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="phone">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label">匯款時間</label>
                        <div class="col-sm-9">
                            <input type="date" class="form-control" name="date">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label">匯款金額</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="amount" autocomplete="off">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label">帳號後五碼</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="account" autocomplete="off">
                        </div>
                    </div>

                    <div class="form-submit">
                        <button type="submit">
                            送出
                        </button>
                    </div>
                    @include('errors.alertSuccess')
                    @include('errors.alertError')
                </div>
            </div>
        </form>
    @endsection
