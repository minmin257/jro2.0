@extends('front.product.default')
    @section('content')
        <div class="product-block">
            <div class="row row-out">
                <div class="col-12 list-m">
                    <div class="product-list-m">
                        <a class="product-name" data-toggle="collapse" href="#collapseList" role="button" aria-expanded="false" aria-controls="collapseExample">
                            <i class="fas fa-angle-right"></i>
                            {{ $product->title }}{{ $product->out ? '(售完)' : '' }}
                        </a>
                        <div class="collapse" id="collapseList">

                            <div class="product-list-block">
                                <ul>
                                    @foreach($category->product->where('delete', 0)->where('state', 1) as $item)
                                        <li>
                                            <a href="{{ route('front.product', [$category->id, $item->id]) }}" title="">
                                                {{ $item->title }}{{ $item->out ? '(售完)' : '' }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-5">
                    <div class="product-img">
                        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner align-items-center">
                                @foreach($product->photo as $photo)
                                    <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                                        <img src="{{ $photo->src }}" class="d-block w-100">
                                    </div>
                                @endforeach
                            </div>
                            @if($product->photo->count() > 1)
                                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-7">
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <div class="product-middle-block">
                                    <div class="product-text">
                                        <h1 class="product-name">
                                            {{ $product->title }}{{ $product->out ? '(售完)' : '' }}
                                        </h1>
                                        <div class="product-specifications">
                                            {{ $product->spec }}
                                        </div>
                                        <div class="product-info">
                                            {!! $product->html !!}
                                        </div>
                                        @if($category->id == 2)
                                            <div class="product-phone">
                                                <!-- ↓圖文編輯器↓ -->
                                                {!! $category->phone->html !!}
                                                <!-- ↑圖文編輯器↑ -->
                                            </div>
                                        @else
                                            <h4 class="product-price">
                                                產品售價 <span>{{ $product->price }}</span>
                                            </h4>
                                        @endif
                                    </div>

                                    <div class="product-select">
                                        <form action="{{ route('front.addToCart') }}" method="post">
                                            @csrf
                                            <input type="hidden" name="product" value="{{ $product->id }}">
                                            @switch($category->id)
                                                @case(1)
                                                    <div class="form-group">
                                                        <label class="sr-only">捲度</label>
                                                        <select class="form-control" name="curl" onchange="filterC({{ $product->id }}, this.value)">
                                                            <option value="">捲度</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="sr-only">粗度</label>
                                                        <select class="form-control" name="thickness" onchange="filterT({{ $product->id }}, this.value)">
                                                            <option value="">粗度</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="sr-only">長度</label>
                                                        <select class="form-control" name="length">
                                                            <option value="">長度</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="sr-only">數量</label>
                                                        <input type="text" name="quantity" class="form-control" placeholder="數量(請自行填寫)">
                                                    </div>
                                                    @if(!$category->product->first()->out)
                                                        <div class="tc mb-2">
                                                            <button type="submit" class="brownBTN2">購 買</button>
                                                        </div>
                                                    @endif
                                                    @break
                                                @case(2)
                                                    @break
                                                @default
                                                    <div class="form-group">
                                                        <label class="sr-only">數量</label>
                                                        <input type="text" name="quantity" class="form-control" placeholder="數量(請自行填寫)">
                                                    </div>
                                                    @if(!$category->product->first()->out)
                                                        <div class="tc mb-2">
                                                            <button type="submit" class="brownBTN2">購 買</button>
                                                        </div>
                                                    @endif
                                            @endswitch
                                            @include('errors.alertError')
                                        </form>
                                    </div>
                                </div>

                            </div>
                            <div class="col-12 col-lg-6 list-l">
                                <div class="product-list-block wow fadeInRight">
                                    <ul>
                                        @foreach($category->product->where('delete', 0) as $item)
                                            <li>
                                                <a href="{{ route('front.product', [$category->id, $item->id]) }}" title="">
                                                    {{ $item->title }}{{ $item->out ? '(售完)' : '' }}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    @endsection

    @section('js')
        <script>
            $(function() {
                $.ajax({
                    type : 'get',
                    url: '{{ route('front.filter') }}',
                    data: {
                        product: {{ $product->id }},
                    },
                    success: function(data){
                        var jsonArray = JSON.parse(data);
                        var sold = '';
                        var dis = '';
                        $.each(jsonArray, function(index, value) {
                            sold = '';dis = '';
                            if (value.out) {
                                sold = '(售完)';
                                dis = 'disabled';
                            }
                            $('select[name=curl]').append('<option value="' + value.curl_id + '" ' + dis + '>' + value.title + sold + '</option>');
                        });
                    }
                });
            });

            function filterC(product, curl) {
                if (curl) {
                    $.ajax({
                        type : 'get',
                        url: '{{ route('front.filter') }}',
                        data: {
                            product: product,
                            curl: curl,
                        },
                        success: function(data){
                            $('select[name=thickness]').empty().append('<option value="">粗度</option>');
                            $('select[name=length]').empty().append('<option value="">長度</option>');
                            var jsonArray = JSON.parse(data);
                            var sold = '';
                            var dis = '';
                            $.each(jsonArray, function(index, value) {
                                sold = '';dis = '';
                                if (value.out) {
                                    sold = '(售完)';
                                    dis = 'disabled';
                                }
                                $('select[name=thickness]').append('<option value="' + value.thickness_id + '" ' + dis + '>' + value.title + sold + '</option>');
                            });
                        }
                    });
                }
                else {
                    $('select[name=thickness]').empty().append('<option value="">粗度</option>');
                    $('select[name=length]').empty().append('<option value="">長度</option>');
                }
            }

            function filterT(product, thickness) {
                if (thickness) {
                    let curl = $('select[name="curl"] :selected').val();
                    $.ajax({
                        type : 'get',
                        url: '{{ route('front.filter') }}',
                        data: {
                            product: product,
                            curl: curl,
                            thickness: thickness,
                        },
                        success: function(data){
                            $('select[name=length]').empty().append('<option value="">長度</option>');
                            var jsonArray = JSON.parse(data);
                            var sold = '';
                            var dis = '';
                            $.each(jsonArray, function(index, value) {
                                sold = '';dis = '';
                                if (value.out || value.option_out) {
                                    sold = '(售完)';
                                    dis = 'disabled';
                                }
                                $('select[name=length]').append('<option value="' + value.length_id + '" ' + dis + '>' + value.title + sold + '</option>');
                            });
                        }
                    });
                }
                else {
                    $('select[name=length]').empty().append('<option value="">長度</option>');
                }
            }
        </script>
    @endsection

