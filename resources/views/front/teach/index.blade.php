@extends('front.teach.default')
    @section('content')
        <div class="teach">
            <div class="row justify-content-center h-100 align-items-center">
                <div class="col-12 col-md-7 col-lg-5">
                    @foreach($teaches as $teach)
                        <a class="teach_btn" href="{{ $teach->src }}" data-lightbox="teach" title="{{ $teach->title }}">
                            <img class="left" src="/images/flower_left.png" alt="">
                            {{ $teach->title }}
                            <img class="right" src="/images/flower_right.png" alt="">
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    @endsection

