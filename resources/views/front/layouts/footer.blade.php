@inject('Footer', 'App\Presenters\FooterPresenter')
<div class="footer">
    <div class="row align-items-center">
        <div class="col-12 col-xl-6">
            <div class="social_btn">
                <a class="phone_btn phone_btn-pc" href="{{ $Footer->getFooter()->src }}" data-lightbox="phone" title="{{ $Footer->getFooter()->title }}<br>{{ $Footer->getFooter()->tel }}"></a>
                <a class="phone_btn phone_btn-m" href="tel:{{ $Footer->getFooter()->tel }}" title="{{ $Footer->getFooter()->title }}<br>{{ $Footer->getFooter()->tel }}"></a>
                <a class="line_btn" href="{{ $Footer->getFooter()->line }}" target="_blank"></a>
            </div>

        </div>
        <div class="col-9 col-xl-6">
            <p class="copyright">
                Copyright © JRO Co., Ltd. All Rights Reserved
            </p>
        </div>
    </div>
</div>
