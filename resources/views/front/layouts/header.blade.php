@inject('Header', 'App\Presenters\HeaderPresenter')
<div class="header">
    <nav class="navbar navbar-expand-lg align-items-end">
        <a class="navbar-brand" href="{{ route('front.index') }}">
            <img src="/images/logo.png" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                @foreach($Header->getSubjects() as $subject)
                    @if($subject->tag === 'product')
                        <li class="nav-item dropdown product-mobile">
                            <a class="nav-link dropdown-toggle" href="javascript:;" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ $subject->title }}
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <div class="row no-gutters">
                                    @foreach($Header->getCategories() as $category)
                                        <div class="col-12 col-sm-6">
                                            <a class="dropdown-item" href="{{ route('front.category', $category->id) }}">
                                                {{ $category->title }}
                                                <img class="w-100" src="{{ $category->src }}" alt="">
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </li>
                        <li class="nav-item product-pc">
                            <a class="nav-link" data-toggle="collapse" href="#collapsePC" role="button" aria-expanded="false" aria-controls="collapsePC">
                                {{ $subject->title }}
                            </a>
                        </li>
                    @elseif($subject->tag === 'news')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ $Header->getNews()->src }}" data-lightbox="news" title="{{ $Header->getNews()->title }}">{{ $subject->title }}</a>
                        </li>
                    @elseif($subject->tag === 'cart')
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('front.'.$subject->tag) }}">
                                {{ $subject->title }}
                                <span class="chart-btn">
                                    <span class="icon"></span>
                                    <span class="badge badge-danger badge-pill">{{ session()->get('cart') ? count(session()->get('cart')->items) : '0' }}</span>
                                </span>
                            </a>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('front.'.$subject->tag) }}">{{ $subject->title }}</a>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
    </nav>
    <div class="collapse" id="collapsePC">
        <div class="row no-gutters">
            @foreach($Header->getCategories() as $category)
                <div class="col-12 col-sm-4">
                    <a class="link" href="{{ route('front.category', $category->id) }}">
                        {{ $category->title }}
                        <img class="w-100" src="{{ $category->src }}" alt="">
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</div>
