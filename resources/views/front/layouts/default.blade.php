@inject('Module', 'App\Presenters\FooterPresenter')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- 網站標題icon -->
    <title>{{ $Module->getSystem()->sitename }}</title>
    <link rel="shortcut icon" href="/images/logo_icon.png" />
    <meta name="keywords" content="{{ $Module->getSystem()->meta_keyword }}">
    <meta name="description" content="{{ $Module->getSystem()->meta_description }}">

    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+TC&display=swap" rel="stylesheet">
    <link href="/css/bootstrap_r.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/lightbox.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">

</head>
<body>
<div class="wrap">
    <div class="container">
        <!-- header================================================== -->
        @yield('header')
        <!-- content================================================== -->
        <content>
            <div class="content">

                @yield('carousel')

                @yield('content')

            </div>

            <div class="seal"></div>

        </content>
        <!-- footer================================================== -->
        @yield('footer')

    </div>
</div>

    <!-- JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/wow.js"></script>
    <script src="/js/lightbox.js"></script>
    <script src="https://kit.fontawesome.com/588be6838c.js"></script>
    <script src="/js/yp.js"></script>
    @yield('js')
</body>
</html>
