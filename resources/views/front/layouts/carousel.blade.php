@inject('Carousel', 'App\Presenters\CarouselPresenter')
<div class="banner">
    <div id="carouselExampleControls" class="carousel slide carousel-fade" data-ride="carousel">
        <div class="carousel-inner">
            @foreach($Carousel->getHomeBanners() as $homeBanner)
                <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                    <div class="img img-pc" style="background-image: url({{ $homeBanner->src }});"></div>
                    <div class="img img-m" style="background-image: url({{ $homeBanner->mobile_src }});"></div>
                </div>
            @endforeach
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
