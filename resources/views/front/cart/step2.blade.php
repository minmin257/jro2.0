@extends('front.cart.default')
    @section('content')
        <div class="cart-step">
            <img src="/images/cart_02.png" alt="">
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-lg-11">
                <form id="orderer">
                    <div class="info-title">
                        購買人資訊
                    </div>
                    <table class="table table-borderless table-sm info-table mb-5">
                        <tbody>
                            <tr>
                                <td class="info-td-1">姓名：</td>
                                <td>
                                    <input type="text" class="form-control" name="buyer" value="{{ old('buyer') }}">
                                </td>
                            </tr>
                            <tr>
                                <td class="info-td-1">電話：</td>
                                <td>
                                    <input type="text" class="form-control" name="buyer_tel" value="{{ old('buyer_tel') }}">
                                </td>
                            </tr>
                            <tr>
                                <td class="info-td-1">地址：</td>
                                <td>
                                    <input type="text" class="form-control" name="buyer_addr" value="{{ old('buyer_addr') }}">
                                </td>
                            </tr>
                            <tr>
                                <td class="info-td-1">Email：</td>
                                <td>
                                    <input type="text" class="form-control" name="email" value="{{ old('email') }}">
                                </td>
                            </tr>
                            <tr>
                                <td class="info-td-1">需求備註：</td>
                                <td>
                                    <textarea class="form-control" name="demand" rows="3">{{ old('demand') }}</textarea>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="info-title">
                        收貨人資訊
                    </div>
                    <table class="table table-borderless table-sm info-table mb-5">
                        <tbody>
                            <tr>
                                <td colspan="2">
                                    <label>
                                        <input type="checkbox" name="same"> 同購買人
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td class="info-td-1">姓名：</td>
                                <td>
                                    <input type="text" class="form-control" name="consignee" value="{{ old('consignee') }}">
                                </td>
                            </tr>
                            <tr>
                                <td class="info-td-1">電話：</td>
                                <td>
                                    <input type="text" class="form-control" name="consignee_tel" value="{{ old('consignee_tel') }}">
                                </td>
                            </tr>
                            <tr>
                                <td class="info-td-1">地址：</td>
                                <td>
                                    <input type="text" class="form-control" name="consignee_addr" value="{{ old('consignee_addr') }}">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    @if($system->receipt)
                        <input type="hidden" name="receipt">
                        <div class="info-title">
                            發票資訊
                        </div>
                        <table class="table table-borderless table-sm info-table mb-5">
                            <tbody>
                                @foreach($receiptTypes as $receiptType)
                                    <tr>
                                        <td colspan="2">
                                            <label><input type="radio" name="receipt" value="{{ $receiptType->id }}"> {{ $receiptType->title }}{{ $receiptType->id == 4 ? '(捐贈單位： '.$receipt->title.' )' : '' }}</label>
                                        </td>
                                    </tr>
                                    @if($receiptType->id == 3)
                                        <tr style="display: none">
                                            <td class="info-td-1">抬頭：</td>
                                            <td>
                                                <input type="text" class="form-control" name="title" disabled>
                                            </td>
                                        </tr>
                                        <tr style="display: none">
                                            <td class="info-td-1">統編：</td>
                                            <td>
                                                <input type="text" class="form-control" name="VAT" disabled>
                                            </td>
                                        </tr>
                                    @elseif($receiptType->id  == 4)
                                        <input type="hidden" name="contribute" value="{{ $receipt->title }}" disabled>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    @endif
                    <div class="info-title">
                        請輸入驗證碼
                    </div>
                    <div>
                        {!! NoCaptcha::display() !!}
                    </div>

                    <div class="tr">
                        <a class="brownBTN2" href="{{ route('front.cart') }}" title="">
                            返 回
                        </a>
                        <input type="button" class="brownBTN2" value="完 成" onclick="store()">
                    </div>
                    @include('errors.error')
                    @include('errors.alertError')
                </form>
            </div>
        </div>

        <form id="complete" action="{{ route('front.complete') }}" method="post">
            @csrf
            <input type="hidden" name="id">
        </form>
    @endsection

    @section('js')
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script>
            $('input[type="checkbox"]').on('click', _paste)
            $('input[name^="buyer"]').on('keyup', _paste)

            function _paste() {
                let _consignee = $('input[name="consignee"]');
                let _consignee_tel = $('input[name="consignee_tel"]');
                let _consignee_addr = $('input[name="consignee_addr"]');
                if ($('input[type="checkbox"]').prop('checked')) {
                    _consignee.val($('input[name="buyer"]').val());
                    _consignee_tel.val($('input[name="buyer_tel"]').val());
                    _consignee_addr.val($('input[name="buyer_addr"]').val());
                }
            }

            $('input[name="receipt"]').on('click', function () {
                if ($(this).val() == 3) {
                    $(this).closest('tbody').find(':nth-child(4), :nth-child(5)').css('display', '').find('input').attr('disabled', false);
                    $('input[name="contribute"]').attr('disabled', true);
                } else {
                    $(this).closest('tbody').find(':nth-child(4), :nth-child(5)').css('display', 'none').find('input').attr('disabled', true);
                    if($(this).val() == 4) {
                        $('input[name="contribute"]').attr('disabled', false);
                    } else {
                        $('input[name="contribute"]').attr('disabled', true);
                    }
                }
            })

            function store()
            {
                $('input[type="button"]').attr('disabled', true);
                $.ajax({
                    type : 'POST',
                    url: '{{ route('front.validateOrderer') }}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: $('#orderer').serialize(),
                    error: function(data) {
                        var data = data.responseJSON;
                        errorsHtml = '<ul>';
                        $.each(data.errors, function(key,value){
                            errorsHtml += '<li>'+ value +'</li>';
                        });
                        errorsHtml += '</ul>';
                        Swal.fire({
                            title: '錯誤',
                            html: errorsHtml,
                            icon: 'warning',
                            showCloseButton: true,
                            showCancelButton: true,
                            showConfirmButton: false,
                        }).then(function (result) {
                            if (data.id) {
                                $('#complete input[name="id"]').val(data.id);
                                $('#complete').submit();
                            }
                        })
                        grecaptcha.reset();
                        $('input[type="button"]').attr('disabled', false);
                    },
                    success: function(data) {
                        $('#complete input[name="id"]').val(data);
                        $('#complete').submit();
                    }
                });
            }
        </script>
    @endsection
