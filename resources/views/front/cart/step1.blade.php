@extends('front.cart.default')
    @section('content')
        <div class="cart-step">
            <img src="/images/cart_01.png" alt="">
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-lg-11">
                <p>
                    訂購前請先閱讀
                    <a class="cart-info-btn" href="{{ route('front.note') }}">
                        <img src="/images/flower_left.png" alt="">
                        購物須知
                        <img src="/images/flower_right.png" alt="">
                    </a>
                </p>
                <form action="{{ route('front.validateCheckout') }}" method="post">
                    @csrf
                    <table class="table cart-table">
                        <thead>
                        <tr>
                            <th class="th-1">商品</th>
                            <th class="th-2">單價</th>
                            <th class="th-2">數量</th>
                            <th class="th-2">小計</th>
                            <th class="th-2">移除</th>
                        </tr>
                        </thead>
                        <tbody>
                            @php
                                $order_subtotal = 0;
                            @endphp
                            @if(session()->get('cart'))
                                @foreach(session()->get('cart')->items as $cart)
                                    @php
                                        $product_subtotal = $cart['item']->product->price * $cart['quantity'];
                                        $order_subtotal += $product_subtotal;
                                    @endphp
                                    <input type="hidden" name="id[]" value="{{ $cart['item']->id }}">
                                    <tr>
                                        <td>
                                            <a class="cart-product" href="{{ route('front.product', [$cart['item']->product->category_id, $cart['item']->product_id]) }}">
                                                <img src="{{ $cart['item']->product->photo->first()->src }}" alt="">
                                                <span>
                                                    {{ $cart['item']->product->title }}
                                                    <span style="margin-left: 10px;">
                                                        @if(isset($cart['item']->curl_id) && isset($cart['item']->thickness_id) && isset($cart['item']->length_id))
                                                            捲度{{ $cart['item']->curl->title }}粗度{{ $cart['item']->thickness->title }}長度{{ $cart['item']->length->title }}
                                                        @endif
                                                    </span>
                                                </span>
                                            </a>
                                        </td>
                                        <td>
                                            <span class="title">單價：</span>
                                            {{ $cart['item']->product->price }}
                                        </td>
                                        <td>
                                            <span class="title">數量：</span>
                                            <input type="number" class="form-control" name="quantity[]" data-id="{{ $cart['item']->id }}" data-price="{{
                                            $cart['item']->product->price }}" data-sub="{{ $product_subtotal }}" value="{{ $cart['quantity'] }}" min="0">
                                        </td>
                                        <td>
                                            <span class="title">小計：</span>
                                            <span id="product_sub">
                                                {{ $product_subtotal }}
                                            </span>
                                        </td>
                                        <td>
                                            <a href="javascript:;" class="remove-btn" onclick="remove('{{ $cart['item']->id }}')">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                    <table class="table table-sm table-borderless money-table">
                        <thead>
                            <tr>
                                <input type="hidden" id="order_subtotal" name="subtotal" value="{{ $order_subtotal }}">
                                <th class="th-1">小計：</th>
                                <th class="th-2">
                                    {{ $order_subtotal }}
                                </th>
                                <th class="th-3 ShippingStandard-pc">元
                                    @if($system->free > $order_subtotal)
                                        <span class="ShippingStandard">
                                            (差 {{ $system->free - $order_subtotal }} 符合免運標準)
                                        </span>
                                    @endif
                                </th>
                            </tr>
                            <tr>
                                <th class="th-1"></th>
                                @if($system->free > $order_subtotal)
                                    <th colspan="2" class="ShippingStandard-m">
                                        <span class="ShippingStandard">
                                            (差 {{ $system->free - $order_subtotal }} 符合免運標準)
                                        </span>
                                    </th>
                                @endif
                            </tr>
                            <tr>
                                <th class="th-1">運費：</th>
                                <td colspan="2">
                                    <select class="form-control" name="shipping">
                                        @foreach($shippings as $shipping)
                                            @if($order_subtotal > $system->free)
                                                @if($loop->last)
                                                    <option value="{{ $shipping->id }}" data-fee="{{ $shipping->fee }}" selected>{{ $shipping->title }}</option>
                                                @else
                                                    <option value="{{ $shipping->id }}" data-fee="{{ $shipping->fee }}" style="display: none">{{ $shipping->title }}{{
                                                    $shipping->fee }}元</option>
                                                @endif
                                            @else
                                                @if($loop->last)
                                                    <option value="{{ $shipping->id }}" data-fee="{{ $shipping->fee }}" style="display: none">{{ $shipping->title }}</option>
                                                @else
                                                    <option value="{{ $shipping->id }}" data-fee="{{ $shipping->fee }}">{{ $shipping->title }}{{ $shipping->fee }}元</option>
                                                @endif
                                            @endif
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <input type="hidden" id="order_total" name="total" value="{{ $order_subtotal }}">
                            <tr>
                                <td class="th-1">
                                    付款方式：
                                </td>
                                <td colspan="2">
                                    <select class="form-control" name="payment">
                                        @foreach($payment_methods as $payment_method)
                                            <option value="{{ $payment_method->id }}" data-fee="{{ $payment_method->fee }}">{{ $payment_method->title }}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                            <td class="th-1">
                                應付金額：
                            </td>
                            <td id="total" class="th-2">
                                {{ $order_subtotal }}
                            </td>
                            <td>元</td>
                        </tbody>
                    </table>
                    <div class="tr">
                        <a class="brownBTN2" href="javascript:history.back();" title="">
                            繼 續 購 物
                        </a>
                        <input type="submit" class="brownBTN2" value="結 帳">
                    </div>
                    @include('errors.alertError')
                </form>

            </div>
        </div>
    @endsection

    @section('js')
        <script>
            $(function() {
                showing();
                calc_total();
            });
            $('input[type="number"]').on('change', _input);
            $('select[name="shipping"]').on('change', _shipping);
            $('select[name="payment"]').on('change', _payment);


            function remove(id) {
                $.ajax({
                    type : 'POST',
                    url: '{{ route('front.removeToCart') }}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        id: id,
                    },
                    success: function(data){
                        location.reload();
                    }
                });
            }

            function showing() {
                if (order_subtotal() >= {{ $system->free }}) {
                    $('.ShippingStandard').hide();
                    $('select[name="shipping"] option').each(function () {
                        if ($(this).val() < 3) {
                            $(this).css('display', 'none');
                        } else {
                            $(this).css('display', '');
                            $(this).prop('selected', true);
                        }
                    })
                } else {
                    let left = {{ $system->free }} - order_subtotal();
                    $('.ShippingStandard').show();
                    $('.ShippingStandard').text(`(差 ${left} 符合免運標準)`)
                    $('select[name="shipping"] option').each(function (index, elm) {
                        if ($(this).val() < 3) {
                            $(this).css('display', '');
                        } else {
                            if ($(this).prop('selected')) {
                                $('select[name="shipping"] option[value="1"]').prop('selected', true);
                            }
                            $(this).css('display', 'none');
                        }
                    })
                }
            }

            function order_subtotal() {
                let order_subtotal = 0;
                $('input[type="number"]').each(function () {
                    order_subtotal += parseInt($(this).attr('data-sub'));
                })
                $('#order_subtotal').val(order_subtotal);
                $('.money-table thead tr:first .th-2').text(order_subtotal);

                return order_subtotal;
            }

            function _input() {
                let item_price = $(this).data('price');
                let calc_price = $(this).val() * item_price;
                $(this).attr('data-sub', calc_price);
                $(this).parent().next().find('#product_sub').text(calc_price);
                $.ajax({
                    type : 'get',
                    url: '{{ route('front.updateCart') }}',
                    data: {
                        id: $(this).data('id'),
                        quantity: $(this).val()
                    }
                });
                showing();
                calc_total(order_subtotal());
            }

            function _shipping() {
                let shipping_fee = $(this).find(':selected').data('fee');
                calc_total();
            }

            function _payment() {
                let payment_fee = $(this).find(':selected').data('fee');
                calc_total();
            }

            function calc_total(order_total = null) {
                let shipping_fee = $('select[name="shipping"] option:selected').data('fee');
                let payment_fee = $('select[name="payment"] option:selected').data('fee');
                let total = 0;
                if (order_total != null) {
                    if (order_total >= {{ $system->free }}) {
                        order_total = order_total * {{ $system->percent / 100 }};
                        total = order_total + shipping_fee + payment_fee;
                    } else {
                        total = order_total + shipping_fee + payment_fee;
                    }
                } else {
                    if (parseInt($('.money-table thead tr:first .th-2').text()) >= {{ $system->free }}) {
                        let order_total = parseInt($('.money-table thead tr:first .th-2').text()) * {{ $system->percent / 100 }};
                        total = order_total + shipping_fee + payment_fee;
                    } else {
                        total = parseInt($('.money-table thead tr:first .th-2').text()) + shipping_fee + payment_fee;
                    }
                }
                $('#order_total').val(total);
                $('#total').text(total);
            }
        </script>
    @endsection
