@extends('front.cart.default')
    @section('content')
        <div class="cart-step">
            <img src="/images/cart_03.png" alt="">
        </div>
        <div class="orderNum-block">
            <h3>訂購完成！感謝您訂購JRO美睫商材。</h3>
            <h3>您的訂單編號為<span>{{ $order->number }}</span></h3>
        </div>
        <div>
            <!-- ↓圖文編輯器↓ -->
            {!! $complete->html !!}
            <!-- ↑圖文編輯器↑ -->
        </div>
    @endsection
