@extends('front.cart.default')
    @section('content')
        <div class="row justify-content-center">
            <div class="col-12 col-sm-10 col-md-6 col-lg-4">
                <div class="order-title">
                    <img class="left" src="/images/flower_left.png" alt="">
                    購物須知
                    <img class="right" src="/images/flower_right.png" alt="">
                </div>
            </div>
        </div>
        <div>
            <!-- ↓圖文編輯器↓ -->
            {!! $note->html !!}
            <!-- ↑圖文編輯器↑ -->
        </div>
        <div class="tc pt-3 pb-3">
            <a class="brownBTN" href="{{ route('front.cart') }}" title="">
                返 回
            </a>
        </div>
    @endsection
