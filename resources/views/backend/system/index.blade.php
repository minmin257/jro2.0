@extends('backend.system.default')

	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							網站設定
						</h1>
					</div>
					@include('success')
					<div class="row">
                        <form method="post" action="{{ route('update_system') }}">
                            @csrf
                            <div class="col-xs-12 col-md-6">
								<div class="form-group">
									<label>網站名稱</label>
									<input type="text" name="sitename" class="form-control" value="{{ $system->sitename }}" required>
								</div>
								<div class="form-group">
									<label>description</label>
									<input type="text" name="meta_description" class="form-control" value="{{ $system->meta_description }}" required>
								</div>
                                <div class="form-group">
                                    <label>keywords</label>
                                    <input type="text" name="meta_keyword" class="form-control" value="{{ $system->meta_keyword }}" required>
                                </div>
								<div class="form-group">
									<label>網站維護</label>
									@if($system->maintain)
										<div class="input-wrapper">
											<input type="radio" id="open_maintain" name="maintain" value="1" checked>
											<label for="open_maintain">開啟</label>
											<span></span>
											<input type="radio" id="close_maintain" name="maintain" value="0">
											<label for="close_maintain">關閉</label>
										</div>
									@else
										<div class="input-wrapper">
											<input type="radio" id="open_maintain" name="maintain" value="1">
											<label for="open_maintain">開啟</label>
											<span></span>
											<input type="radio" id="close_maintain" name="maintain" value="0" checked>
											<label for="close_maintain">關閉</label>
										</div>
									@endif
								</div>
								<div class="form-group">
									<label>維護訊息</label>
									<textarea rows="5" name="message" class="form-control" required>{{ $system->message }}</textarea>
								</div>
                                {{-- 錯誤警示位置 --}}
                                @include('errors.error')
                                <input type="submit" class="btn btn-primary" value="送出">
						    </div>
                        </form>
                    </div>
				</div>
			</div>
		</div>
	@endsection
