<div id="sidebar" class="sidebar responsive" data-sidebar="true" data-sidebar-scroll="true" data-sidebar-hover="true">
	<ul class="nav nav-list">
		<li class="{{ active('backend.index') }}">
			<a href="{{ route('backend.index') }}">
				<i class="menu-icon fas fa-tachometer-alt"></i>
				<span class="menu-text">首頁</span>
			</a>
			<b class="arrow"></b>
		</li>

		<li class="{{ Request::segment(2) === 'content' ? 'open' : '' }}">
			<a href="#" class="dropdown-toggle">
				<i class="menu-icon far fa-edit"></i>
				<span class="menu-text">內容管理</span>
				<b class="arrow fas fa-angle-down"></b>
			</a>
			<b class="arrow"></b>

			<ul class="submenu">
                <li class="{{ Request::segment(3) === 'home_banner' ? 'active' : '' }}">
                    <a href="{{ route('backend.home_banner') }}">
                        <i class="menu-icon fas fa-caret-right"></i>
                        <span class="menu-text">首頁輪播管理</span>
                    </a>
                    <b class="arrow"></b>
                </li>
				@if(auth()->user()->hasPermission('產品管理'))
				<li class="{{ (Request::segment(3) === 'productCategory' || Request::segment(3) === 'productList' || Request::segment(3) === 'phone' || Request::segment
				(3) === 'eyelash' || Request::segment(3) === 'eyelashList') ? 'open' : '' }}">
					<a href="#" class="dropdown-toggle">
						<i class="menu-icon fas fa-caret-right"></i>
						產品管理
						<b class="arrow fas fa-angle-down"></b>
					</a>
					<b class="arrow"></b>

					<ul class="submenu nav-hide">
						<li class="{{ Request::segment(3) === 'productCategory' ? 'active' : '' }}">
							<a href="{{ route('backend.productCategory') }}">
								<i class="menu-icon fas fa-caret-right"></i>
								分類管理
							</a>
							<b class="arrow"></b>
						</li>
						<li class="{{ Request::segment(3) === 'productList' ? 'active' : '' }}">
							<a href="{{ route('backend.productList') }}">
								<i class="menu-icon fas fa-caret-right"></i>
								產品列表
							</a>
							<b class="arrow"></b>
						</li>
                        <li class="{{ Request::segment(3) === 'phone' ? 'active' : '' }}">
                            <a href="{{ route('backend.phone') }}">
                                <i class="menu-icon fas fa-caret-right"></i>
                                電話下單
                            </a>
                            <b class="arrow"></b>
                        </li>
                        <li class="{{ Request::segment(3) === 'eyelash' ? 'active' : '' }}">
                            <a href="{{ route('backend.eyelash') }}">
                                <i class="menu-icon fas fa-caret-right"></i>
                                規格管理
                            </a>
                            <b class="arrow"></b>
                        </li>
                        <li class="{{ Request::segment(3) === 'eyelashList' ? 'active' : '' }}">
                            <a href="{{ route('backend.eyelashList') }}">
                                <i class="menu-icon fas fa-caret-right"></i>
                                睫毛規格列表
                            </a>
                            <b class="arrow"></b>
                        </li>
					</ul>
				</li>
				@endif
                <li class="{{ (Request::segment(3) === 'teach' || Request::segment(3) === 'news' || Request::segment(3) === 'footer') ? 'open' : '' }}">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fas fa-caret-right"></i>
                        圖片管理
                        <b class="arrow fas fa-angle-down"></b>
                    </a>
                    <b class="arrow"></b>

                    <ul class="submenu nav-hide">
                        <li class="{{ Request::segment(3) === 'teach' ? 'active' : '' }}">
                            <a href="{{ route('backend.teach') }}">
                                <i class="menu-icon fas fa-caret-right"></i>
                                教學
                            </a>
                        </li>
                        <li class="{{ Request::segment(3) === 'news' ? 'active' : '' }}">
                            <a href="{{ route('backend.news') }}">
                                <i class="menu-icon fas fa-caret-right"></i>
                                活動訊息
                            </a>
                            <b class="arrow"></b>
                        </li>
                        <li class="{{ Request::segment(3) === 'footer' ? 'active' : '' }}">
                            <a href="{{ route('backend.footer') }}">
                                <i class="menu-icon fas fa-caret-right"></i>
                                頁尾
                            </a>
                            <b class="arrow"></b>
                        </li>
                    </ul>
                </li>
			</ul>
		</li>

        <li class="{{ (Request::segment(2) === 'shopping') ? 'open' : '' }}">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fas fa-users-cog"></i>
                購物管理
                <b class="arrow fas fa-angle-down"></b>
            </a>
            <b class="arrow"></b>

            <ul class="submenu nav-hide">
                <li class="{{ (Request::segment(3) === 'setting' || Request::segment(3) === 'shipping') ? 'open' : '' }}">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fas fa-caret-right"></i>
                        金額設定
                        <b class="arrow fas fa-angle-down"></b>
                    </a>
                    <b class="arrow"></b>

                    <ul class="submenu nav-hide">
                        <li class="{{ active('shopping.free') }}">
                            <a href="{{ route('shopping.free') }}">
                                <i class="menu-icon fas fa-caret-right"></i>
                                滿額免運、貨到付款設定
                            </a>
                        </li>
                        <li class="{{ active('shopping.shipping') }}">
                            <a href="{{ route('shopping.shipping') }}">
                                <i class="menu-icon fas fa-caret-right"></i>
                                運費設定
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="{{ active('shopping.receipt') }}">
                    <a href="{{ route('shopping.receipt') }}">
                        <i class="menu-icon fas fa-caret-right"></i>
                        發票設定
                    </a>
                </li>
                <li class="{{ active('shopping.expired') }}">
                    <a href="{{ route('shopping.expired') }}">
                        <i class="menu-icon fas fa-caret-right"></i>
                        訂單逾期設定
                    </a>
                </li>
                <li class="{{ active('shopping.note') }}">
                    <a href="{{ route('shopping.note') }}">
                        <i class="menu-icon fas fa-caret-right"></i>
                        購物須知
                    </a>
                    <b class="arrow"></b>
                </li>
                <li class="{{ active('shopping.complete') }}">
                    <a href="{{ route('shopping.complete') }}">
                        <i class="menu-icon fas fa-caret-right"></i>
                        訂購完成頁面
                    </a>
                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        @if(auth()->user()->hasPermission('訂單管理'))
        <li class="{{  Request::segment(2) === 'order' ? 'active' : '' }}">
            <a href="{{ route('backend.order') }}">
                <i class="menu-icon fas fa-shopping-cart"></i>
                <span class="menu-text">訂單管理</span>
            </a>
            <b class="arrow"></b>
        </li>
        @endif

        <li class="{{  Request::segment(2) === 'authority' ? 'open' : '' }}">
			<a href="#" class="dropdown-toggle">
				<i class="menu-icon fas fa-users-cog"></i>
				<span class="menu-text">權限管理</span>
				<b class="arrow fas fa-angle-down"></b>
			</a>
			<b class="arrow"></b>

			<ul class="submenu">
				<li class="{{ active('profile') }}">
					<a href="{{ route('profile') }}">
						<i class="menu-icon fas fa-caret-right"></i>
						基本資料
					</a>
					<b class="arrow"></b>
				</li>
				@if(auth()->user()->hasPermission('權限管理'))
				<li class="{{ (Request::segment(5) === 'edit' || Request::segment(3) === 'permission') ? 'active' : '' }}{{ active
				('management') }}">
					<a href="{{ route('management') }}">
						<i class="menu-icon fas fa-caret-right"></i>
						帳號管理
					</a>
					<b class="arrow"></b>
				</li>
				@endif
			</ul>
		</li>

		<li class="{{ Request::segment(2) === 'system' ? 'open' : '' }}">
			<a href="#" class="dropdown-toggle">
				<i class="menu-icon fas fa-cogs"></i>
				<span class="menu-text">系統設定</span>
				<b class="arrow fa fa-angle-down"></b>
			</a>
			<b class="arrow"></b>

			<ul class="submenu">
				@if(auth()->user()->hasPermission('網站設定管理'))
				<li class="{{ active('system') }}">
					<a href="{{ route('system') }}">
						<i class="menu-icon fas fa-caret-right"></i>
						網站設定
					</a>
					<b class="arrow"></b>
				</li>
				@endif
                <li class="{{ active('mail_setting') }}">
                    <a href="{{ route('mail_setting') }}">
                        <i class="menu-icon fas fa-caret-right"></i>
                        信箱設定
                    </a>
                    <b class="arrow"></b>
                </li>
			</ul>
		</li>
	</ul>

	<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
		<i id="sidebar-toggle-icon" class="ace-icon fas fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fas fa-angle-double-right"></i>
	</div>
</div>
