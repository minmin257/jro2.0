@extends('backend.authority.default')

	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							{{ $user->name }} - 權限設定
						</h1>
					</div>
					@include('success')
					<div class="row">
						<div class="col-xs-12">
							<div class="table-responsive">
								<table class="table">
									<thead>
										<tr>
											<th>標題</th>
											<th>啟用否</th>
										</tr>
									</thead>

									<tbody>
										{{-- 錯誤警示位置 --}}
										@include('errors.error')
										<form id="form" method="post" action="{{ route('create_permission') }}">
											@csrf
											<input type="hidden" name="id" value="{{ $user->id }}">
											@foreach($permissions as $permission)
											<tr>
												<td data-title="標題">
													{{ $permission->title }}
												</td>
												<td data-title="啟用否">
													@if($user->user_permissions->contains('permission_id',$permission->id))
														<select name="state[]" class="form-control on">
															<option class="on" value="{{ $permission->id }}" selected>啟用</option>
															<option class="off" value="0">停用</option>
														</select>
													@else
														<select name="state[]" class="form-control off">
															<option class="on" value="{{ $permission->id }}">啟用</option>
															<option class="off" value="0" selected>停用</option>
														</select>
													@endif
												</td>
											</tr>
											@endforeach

										</form>
									</tbody>
								</table>
								<div class="form-group">
									<input type="submit" class="btn btn-primary" value="儲存" onclick="$('#form').submit();">
									<a href="{{ route('management') }}" class="btn btn-default" title="">返回</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	@endsection

	@section('js')
	<script type="text/javascript">
		//select 換顏色
	    $('select.form-control').change(function(){
	        var color = $(this).find(':selected').attr('class');
	        if ( color === "on") {
	            $(this).removeClass('off').addClass('on');
	        }else if ( color === "off") {
	            $(this).removeClass('on').addClass('off');
	        }
	    });
	</script>
	@endsection
