@extends('backend.authority.default')

	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							基本資料
						</h1>
					</div>
					@include('success')
					<div class="row">
						<div class="col-xs-12 col-md-6">
							<form method="post" action="{{ route('update_profile') }}">
								@csrf
								<input type="hidden" name="id" value="{{ auth()->user()->id }}">
								<div class="form-group">
									<label>管理帳號</label>
									<input type="text" class="form-control" value="{{ auth()->user()->account }}" readonly>
								</div>
								<div class="form-group">
									<label>姓名</label>
									<input type="text" name="name" class="form-control" value="{{ auth()->user()->name }}" required>
								</div>
								<div class="form-group">
									<label>修改密碼</label>
									<input type="password" name="password" class="form-control">
									<small>若不更改密碼，請空白</small>
								</div>
								<div class="form-group">
									<label>確認密碼</label>
									<input type="password" name="password_confirmation" class="form-control">
								</div>
								{{-- 錯誤警示位置 --}}
					    	    @include('errors.error')
								<input type="submit" class="btn btn-primary" value="送出">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	@endsection
