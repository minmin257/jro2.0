@if(isset($setting))
    <div class="modal-body">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    @foreach($setting as $item)
                        <input type="hidden" name="id[]" value="{{ $item->id }}">
                        <label>{{ $item->type->title }}</label>
                        <div class="form-group">
                            <label>啟用狀態</label>
                            <select class="form-control" name="state[]">
                                @if($item->state)
                                    <option value="1" selected>啟用</option>
                                    <option value="0">關閉</option>
                                @else
                                    <option value="1">啟用</option>
                                    <option value="0" selected>關閉</option>
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <label>銷售狀態</label>
                            <select class="form-control" name="out[]">
                                @if($item->out)
                                    <option value="1" selected>完售</option>
                                    <option value="0">銷售中</option>
                                @else
                                    <option value="1">完售</option>
                                    <option value="0" selected>銷售中</option>
                                @endif
                            </select>
                        </div>
                    @endforeach
                    <div id="error_message"></div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.panel-body -->
    </div>
    <div class="modal-footer">
        <input type="button" class="btn btn-primary" value="確定送出" onclick="update_setting()">
    </div>
@endif
