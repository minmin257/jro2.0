@extends('backend.content.product.default')

    @section('content')
        <div class="main-content">
            <div class="main-content-inner">
                <div class="page-content">
                    <div class="page-header">
                        <h1>
                            {{ $product->title }} - 圖庫
                        </h1>
                    </div>
                    @include('success')
                    <div class="row">
                        <div class="col-xs-12">
                            <a href="#" class="btn btn-default" role="button" data-toggle="modal" data-target="#myModal">新增</a>
                            @if(isset($productCategory))
                                <a href="{{ route('backend.product', $productCategory->id) }}" class="btn btn-default">返回</a>
                            @else
                                <a href="{{ route('backend.productList') }}" class="btn btn-default">返回</a>
                            @endif
                            <div class="space"></div>
                        </div>

                        <div class="col-xs-12">
                            <div class="table-responsive">
                                @foreach($product->photo as $photo)
                                    <input type="checkbox" style="display: none;" id="a{{ $photo->id }}">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3" style="border:solid 1px black;">
                                        <img class="old_src" src="{{ $photo->src }}" style="width: 100%;cursor: pointer;" data-sort="{{ $photo->sort }}" data-id="{{
                                        $photo->id }}">
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- 新增區塊 -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">新增 圖片</h4>
                    </div>
                    <div class="modal-body">
                        <form id="form_create">
                            <input type="hidden" name="id" value="{{ $product->id }}">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>圖片<span style="color: red;">(圖片尺寸：450 * 300px)</span></label>
                                            <button class="form-control btn btn-primary lfm" data-input="thumbnail" data-preview="holder">
                                                <i class="far fa-image"></i>上傳圖片
                                            </button>
                                            <input type="hidden" id="thumbnail" class="form-control" name="filepath">
                                        </div>
                                        <div id="holder" class="form-group"></div>
                                        <div class="form-group">
                                            <label>順序</label>
                                            <input type="number" class="form-control" name="sort" pattern="[0-9]" value="0">
                                        </div>
                                        {{-- 錯誤警示位置 --}}
                                        <div id="error_message"></div>
                                    </div>
                                    <!-- /.col-lg-12 -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.panel-body -->
                        </form>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-primary" value="確定送出" onclick="store()">
                    </div>
                </div>
            </div>
        </div>

        <!-- 修改區塊 -->
        <div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="updateModalLabel">圖片 調整</h4>
                    </div>
                    <div class="modal-body">
                        <form id="form_update">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <input type="hidden" name="id" id="old_id">
                                        <div class="form-group">
                                            <label>順序</label>
                                            <input type="number" class="form-control" name="sort" pattern="[0-9]" id="old_sort">
                                        </div>
                                        {{-- 錯誤警示位置 --}}
                                        <div id="error_message"></div>
                                    </div>
                                    <!-- /.col-lg-12 -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.panel-body -->
                        </form>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-danger" value="刪除" onclick="alert_del()">
                        <input type="button" class="btn btn-primary" value="確定送出" onclick="update()">
                    </div>
                </div>
            </div>
        </div>
    @endsection

    @section('js')
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9 "></script>
        <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
        <script src="/js/ajax.js"></script>
        <script type="text/javascript">
            $(function() {
                $('.lfm').filemanager('image');
            });

            $('.old_src').on('click', function(e) {
                $('#old_sort').val($(this).attr('data-sort'));
                $('#old_id').val($(this).attr('data-id'));
                $('#updateModal').modal('show');
            });

            function store()
            {
                var ajaxRequest = new ajaxCreate('POST','{{ route('create_photo') }}',$('#form_create').serialize());
                ajaxRequest.request();
            }

            function update()
            {
                var ajaxRequest = new ajaxUpdate('POST','{{ route('update_photo') }}',$('#form_update').serialize());
                ajaxRequest.request();
            }

            function alert_del()
            {
                var id = $('#old_id').val();
                var ajaxRequest = new ajaxDel('POST','{{ route('delete_photo') }}',id);
                ajaxRequest.request();
            }
        </script>
    @endsection
