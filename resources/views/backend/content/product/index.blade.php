@extends('backend.content.product.default')

	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							產品管理
						</h1>
					</div>
					@include('success')
					<div class="row">
						<div class="col-xs-12">
                            <a href="{{ route('backend.productCategory') }}" class="btn btn-default">返回</a>
                            <div class="space"></div>
                            <a href="#" class="btn btn-default" role="button" data-toggle="modal" data-target="#myModal">新增</a>
							<input type="button" class="btn btn-primary" value="儲存變更" onclick="$('#form').submit()">
							<div class="space"></div>
						</div>

						<div class="col-xs-12">
							<div class="table-responsive">
								<table class="table">
									<thead>
										<tr>
											<th>修改</th>
											<th>刪除</th>
                                            <th>圖庫</th>
                                            @if($productCategory->id == 1)
                                                <th>睫毛規格</th>
                                            @endif
                                            <th>分類</th>
											<th>標題</th>
											<th>順序</th>
                                            <th>啟用狀態</th>
                                            <th>銷售狀態</th>
                                        </tr>
									</thead>

									<tbody>
										<form id="form" method="post" action="{{ route('update_product') }}">
											@csrf
											@foreach($products as $product)
											<input type="hidden" name="id[]" value="{{ $product->id }}">
											<tr>
												<td data-title="修改">
													<a href="{{ route('edit_product', [$productCategory->id, $product->id]) }}" class="btn btn-info btn-outline btn-circle">
														<i class="fas fa-edit"></i>
													</a>
												</td>
												<td data-title="刪除">
													<button class="btn btn-danger btn-outline btn-circle" type="button" onclick="alert_del('{{ $product->id }}')">
														<i class="far fa-trash-alt"></i>
													</button>
												</td>
                                                <td data-title="圖庫">
                                                    <a href="{{ route('category.edit_photo', [$productCategory->id, $product->id]) }}" class="btn btn-info btn-outline
                                                    btn-circle">
                                                        <i class="fas fa-images"></i>
                                                    </a>
                                                </td>
                                                @if($productCategory->id == 1)
                                                    <td data-title="睫毛規格">
                                                        <a href="{{ route('eyelash', [$productCategory->id, $product->id]) }}" class="btn btn-info btn-outline btn-circle">
                                                            <i class="fas fa-eye"></i>
                                                        </a>
                                                    </td>
                                                @endif
												<td data-title="分類">
                                                    {{ $product->category->title }}
                                                </td>
												<td data-title="標題">
                                                    <input type="text" class="form-control" name="title[]" value="{{ $product->title }}">
												</td>
												<td data-title="順序">
													<input type="number" class="form-control" name="sort[]" pattern="[0-9]" value="{{ $product->sort }}">
												</td>
												<td data-title="啟用狀態">
													<select class="form-control" name="state[]">
														@if($product->state)
                                                            <option value="1" selected>啟用</option>
                                                            <option value="0">關閉</option>
														@else
                                                            <option value="1">啟用</option>
                                                            <option value="0" selected>關閉</option>
														@endif
													</select>
												</td>
                                                <td data-title="銷售狀態">
                                                    <select class="form-control" name="out[]">
                                                        @if($product->out)
                                                            <option value="1" selected>完售</option>
                                                            <option value="0">銷售中</option>
                                                        @else
                                                            <option value="1">完售</option>
                                                            <option value="0" selected>銷售中</option>
                                                        @endif
                                                    </select>
                                                </td>
											</tr>
											@endforeach
										</form>
										{{-- 錯誤警示位置 --}}
				    	    			@include('errors.error')
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


		<!-- 新增區塊 -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">新增產品</h4>
					</div>
					<div class="modal-body">
						<form id="form_create">
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											<label>分類</label>
                                            <select class="form-control" name="category">
                                                <option value=""></option>
                                                @foreach($productCategories as $category)
                                                    @if($productCategory->id == $category->id)
                                                        <option value="{{ $category->id }}" selected>{{ $category->title }}</option>
                                                    @else
                                                        <option value="{{ $category->id }}">{{ $category->title }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
										</div>
                                        <div class="form-group">
                                            <label>圖片<span style="color: red;">(圖片尺寸：450 * 300px)</span></label>
                                            <button class="form-control btn btn-primary lfm" data-input="thumbnail" data-preview="holder">
                                                <i class="far fa-image"></i>上傳圖片
                                            </button>
                                            <input id="thumbnail" class="form-control" type="hidden" name="filepath">
                                        </div>
                                        <div id="holder" class="form-group"></div>
										<div class="form-group">
											<label>標題</label>
											<input type="text" class="form-control" name="title">
										</div>
                                        <div class="form-group">
                                            <label>規格</label>
                                            <input type="text" class="form-control" name="spec">
                                        </div>
                                        <div class="form-group">
                                            <label>金額</label>
                                            <input type="number" class="form-control" name="price">
                                        </div>
										<div class="form-group">
											<label>產品介紹</label>
											<textarea name="html" class="form-control editor"></textarea>
										</div>
										<div class="form-group">
											<label>順序</label>
											<input type="number" class="form-control" name="sort" pattern="[0-9]" value="0">
										</div>
										{{-- 錯誤警示位置 --}}
										<div id="error_message"></div>
									</div>
									<!-- /.col-lg-12 -->
								</div>
								<!-- /.row -->
							</div>
							<!-- /.panel-body -->
						</form>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-primary" value="確定送出" onclick="store()">
					</div>
				</div>
			</div>
		</div>
	@endsection

	@section('js')
        {{-- <script src="//cdn.tinymce.com/4/tinymce.min.js"></script> --}}
        <script type="text/javascript" src="/js/tinymce.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
        <script src="/js/ajax.js"></script>
        <script type="text/javascript">
            $(function() {
                $('.lfm').filemanager('image');

                $(document).on('focusin', function(e) {
                    if ($(e.target).closest(".mce-window").length) {
                        e.stopImmediatePropagation();
                    }
                });
            });

            function store()
            {
                tinyMCE.triggerSave();
                var ajaxRequest = new ajaxCreate('POST','{{ route('create_product') }}',$('#form_create').serialize());
                ajaxRequest.request();
            }

            function alert_del(id)
            {
                var ajaxRequest = new ajaxDel('POST','{{ route('delete_product') }}',id);
                ajaxRequest.request();
            }
        </script>
	@endsection
