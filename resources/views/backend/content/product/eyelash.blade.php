@extends('backend.content.product.default')

    @section('content')
        <div class="main-content">
            <div class="main-content-inner">
                <div class="page-content">
                    <div class="page-header">
                        <h1>
                            {{ $product->title }} - 睫毛規格
                        </h1>
                    </div>
                    @include('success')
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <a href="{{ route('backend.product', $productCategory->id) }}" class="btn btn-default">返回</a>
                            <div class="space"></div>
                        </div>
                        <div class="col-xs-12 col-sm-8 text-right">
                            <a href="#" class="btn btn-success" role="button" data-tag="curl" data-toggle="modal" data-target="#mySetting">捲度設定</a>
                            <a href="#" class="btn btn-success" role="button" data-tag="thickness" data-toggle="modal" data-target="#mySetting">粗度設定</a>
                            <a href="#" class="btn btn-success" role="button" data-tag="length" data-toggle="modal" data-target="#mySetting">長度設定</a>
                        </div>
                        <div class="col-xs-12">
                            <a href="#" class="btn btn-default" role="button" data-toggle="modal" data-target="#myModal">新增</a>
                            <input type="button" class="btn btn-primary" value="儲存變更" onclick="$('#form').submit()">
                            <div class="space"></div>
                        </div>

                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>刪除</th>
                                            <th>捲度</th>
                                            <th>粗度</th>
                                            <th>長度</th>
                                            <th>啟用狀態</th>
                                            <th>銷售狀態</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <form id="form" method="post" action="{{ route('update_option') }}">
                                            @csrf
                                            @foreach($product->option->where('delete', 0) as $option)
                                                <input type="hidden" name="id[]" value="{{ $option->id }}">
                                                <tr>
                                                    <td data-title="刪除">
                                                        <button class="btn btn-danger btn-outline btn-circle" type="button" onclick="alert_del({{ $option->id }})">
                                                            <i class="far fa-trash-alt"></i>
                                                        </button>
                                                    </td>
                                                    <td data-title="捲度">
                                                        {{ $option->curl->title }}
                                                    </td>
                                                    <td data-title="粗度">
                                                        {{ $option->thickness->title }}
                                                    </td>
                                                    <td data-title="長度">
                                                        {{ $option->length->title }}
                                                    </td>
                                                    <td data-title="啟用狀態">
                                                        <select class="form-control" name="state[]">
                                                            @if($option->state)
                                                                <option value="1" selected>啟用</option>
                                                                <option value="0">關閉</option>
                                                            @else
                                                                <option value="1">啟用</option>
                                                                <option value="0" selected>關閉</option>
                                                            @endif
                                                        </select>
                                                    </td>
                                                    <td data-title="銷售狀態">
                                                        <select class="form-control" name="out[]">
                                                            @if($option->out)
                                                                <option value="1" selected>完售</option>
                                                                <option value="0">銷售中</option>
                                                            @else
                                                                <option value="1">完售</option>
                                                                <option value="0" selected>銷售中</option>
                                                            @endif
                                                        </select>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </form>
                                        {{-- 錯誤警示位置 --}}
                                        @include('errors.error')
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- 新增區塊 -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">新增規格</h4>
                    </div>
                    <div class="modal-body">
                        <form id="form_create">
                            <input type="hidden" name="id" value="{{ $product->id }}">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>捲度</label>
                                            <select class="form-control" name="curl">
                                                <option value=""></option>
                                                @foreach($curlTypes as $curlType)
                                                    <option value="{{ $curlType->id }}">{{ $curlType->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>粗度</label>
                                            <select class="form-control" name="thickness">
                                                <option value=""></option>
                                                @foreach($thicknessTypes as $thicknessType)
                                                    <option value="{{ $thicknessType->id }}">{{ $thicknessType->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>長度</label>
                                            <select class="form-control" name="length">
                                                <option value=""></option>
                                                @foreach($lengthTypes as $lengthType)
                                                    <option value="{{ $lengthType->id }}">{{ $lengthType->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div id="error_message"></div>
                                    </div>
                                    <!-- /.col-lg-12 -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!-- /.panel-body -->
                        </form>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-primary" value="確定送出" onclick="store()">
                    </div>
                </div>
            </div>
        </div>

        <!-- 設定區塊 -->
        <div class="modal fade" id="mySetting" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form id="form_update">
                        <input type="hidden" name="tag">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel"></h4>
                        </div>
                        <div id="result">
                            @include('backend.content.product.setting')
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endsection

    @section('js')
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script src="/js/ajax.js"></script>
        <script type="text/javascript">
            $(function() {
                $('.text-right a').on('click', function () {
                    let title = $(this).html();
                    let tag = $(this).data('tag');
                    $('#mySetting #myModalLabel').html(title);
                    $('#mySetting input[name="tag"]').val(tag);

                    $.ajax({
                        type : 'get',
                        url: '{{ route('option.setting') }}',
                        data: {
                            id: {{ $product->id }},
                            tag: tag,
                        },
                        success: function(data) {
                            $('#result').html(data);
                        }
                    });
                })
            });

            function store() {
                var ajaxRequest = new ajaxCreate('POST','{{ route('create_option') }}',$('#form_create').serialize());
                ajaxRequest.request();
            }

            function alert_del(id) {
                var ajaxRequest = new ajaxDel('POST','{{ route('delete_option') }}',id);
                ajaxRequest.request();
            }

            function update_setting() {
                var ajaxRequest = new ajaxUpdate('POST','{{ route('update_setting') }}',$('#form_update').serialize());
                ajaxRequest.request();
            }
        </script>
    @endsection
