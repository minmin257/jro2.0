@extends('backend.content.product.default')

    @section('content')
        <div class="main-content">
            <div class="main-content-inner">
                <div class="page-content">
                    <div class="page-header">
                        <h1>
                            {{ $product->title }}
                        </h1>
                    </div>
                    @include('success')
                    <div class="row">
                        <div class="col-xs-12">
                            @if(isset($productCategory))
                                <a href="{{ route('backend.product', $productCategory->id) }}" class="btn btn-default">返回</a>
                            @else
                                <a href="{{ route('backend.productList') }}" class="btn btn-default">返回</a>
                            @endif
                            <div class="space"></div>

                            <form id="form" method="post" action="{{ route('update_filter') }}">
                                @csrf
                                <input type="hidden" name="id" value="{{ $product->id }}">
                                <div class="form-group">
                                    <label>分類</label>
                                    <select class="form-control" name="category">
                                        <option value=""></option>
                                        @foreach($productCategories as $category)
                                            @if($product->category_id == $category->id)
                                                <option value="{{ $category->id }}" selected>{{ $category->title }}</option>
                                            @else
                                                <option value="{{ $category->id }}">{{ $category->title }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>標題</label>
                                            <input type="text" class="form-control" name="title" value="{{ $product->title }}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>規格</label>
                                            <input type="text" class="form-control" name="spec" value="{{ $product->spec }}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>金額</label>
                                            <input type="number" class="form-control" name="price" value="{{ $product->price }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>順序</label>
                                    <input type="number" class="form-control" name="sort" pattern="[0-9]" value="0">
                                </div>
                                <div class="form-group">
                                    <label>產品介紹</label>
                                    <textarea name="html" class="form-control editor">{!! $product->html !!}</textarea>
                                </div>
                                {{-- 錯誤警示位置 --}}
                                @include('errors.error')
                                <input type="submit" class="btn btn-primary" value="確定送出">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection

    @section('js')
        {{-- <script src="//cdn.tinymce.com/4/tinymce.min.js"></script> --}}
        <script type="text/javascript" src="/js/tinymce.js"></script>
    @endsection
