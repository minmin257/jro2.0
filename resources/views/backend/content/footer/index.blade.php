@extends('backend.content.footer.default')

	@section('content')
        <div class="main-content">
            <div class="main-content-inner">
                <div class="page-content">
                    <div class="page-header">
                        <h1>
                            頁尾
                        </h1>
                    </div>
                    @include('success')
                    <div class="row">
                        <div class="col-xs-12">
                            <input type="button" class="btn btn-primary" value="儲存變更" onclick="$('#form').submit()">
                        </div>
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table class="table" style="table-layout: fixed;">
                                    <thead>
                                        <tr>
                                            <th>標題</th>
                                            <th>鋼印<span style="color: red;">(210 * 210px)</span></th>
                                            <th>Line連結</th>
                                            <th>電話標題</th>
                                            <th>電話圖片<span style="color: red;">(210 * 210px)</span></th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <form id="form" method="post" action="{{ route('update_footer') }}">
                                            @csrf
                                            <input type="hidden" id="thumbnail2" name="seal" value="{{ $footer->seal }}">
                                            <input type="hidden" id="thumbnail" name="filepath" value="{{ $footer->src }}">
                                            <tr>
                                                <td data-title="標題">
                                                    <input type="text" class="form-control" name="title" value="{{ $footer->title }}">
                                                </td>
                                                <td id="holder2" class="lfm" data-input="thumbnail2" data-preview="holder2" data-title="鋼印">
                                                    <img class="img-responsive img-shadow" src="{{ $footer->seal }}">
                                                </td>
                                                <td data-title="Line連結">
                                                    <input type="text" class="form-control" name="line" value="{{ $footer->line }}">
                                                </td>
                                                <td data-title="電話標題">
                                                    <input type="text" class="form-control" name="tel" value="{{ $footer->tel }}">
                                                </td>
                                                <td id="holder" class="lfm" data-input="thumbnail" data-preview="holder" data-title="電話圖片">
                                                    <img class="img-responsive img-shadow" src="{{ $footer->src }}">
                                                </td>
                                            </tr>
                                        </form>
                                        {{-- 錯誤警示位置 --}}
                                        @include('errors.error')
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	@endsection

    @section('js')
        <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
        <script type="text/javascript">
            $(function() {
                $('.lfm').filemanager('image');
            });
        </script>
    @endsection
