@extends('backend.content.eyelash.default')

	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
                            規格管理
						</h1>
					</div>
					@include('success')
					<div class="row">
						<div class="col-xs-12">
                            <a href="#" class="btn btn-default" role="button" data-toggle="modal" data-target="#myModal">新增</a>
							<div class="space"></div>
						</div>

						<div class="col-xs-12">
                            <div class="btn-group btn-group-justified">
                                <a href="{{ route('backend.eyelash') }}" class="btn {{ active('backend.eyelash') ? 'btn-primary' : 'btn-default' }}">全部</a>
                                <a href="{{ route('backend.curl') }}" class="btn {{ active('backend.curl') ? 'btn-primary' : 'btn-default' }}">捲度</a>
                                <a href="{{ route('backend.thickness') }}" class="btn {{ active('backend.thickness') ? 'btn-primary' : 'btn-default' }}">粗度</a>
                                <a href="{{ route('backend.length') }}" class="btn {{ active('backend.length') ? 'btn-primary' : 'btn-default' }}">長度</a>
                            </div>
							<div class="table-responsive">
								<table class="table">
									<thead>
										<tr>
                                            <th>刪除</th>
                                            <th>分類</th>
                                            <th>標題</th>
                                        </tr>
									</thead>

									<tbody>
										<form id="form" method="post" action="{{ route('update_product') }}">
											@csrf
											@foreach($result as $item)
                                            @php
                                                if (get_class($item) == 'App\CurlType') {
                                                    $ca = '捲度';
                                                    $tag = 'curl';
                                                } elseif (get_class($item) == 'App\ThicknessType') {
                                                    $ca = '粗度';
                                                    $tag = 'thickness';
                                                } else {
                                                    $ca = '長度';
                                                    $tag = 'length';
                                                }
                                            @endphp
											<tr>
												<td data-title="刪除">
													<button class="btn btn-danger btn-outline btn-circle" type="button" onclick="alert_del('{{ $tag }}', '{{ $item->id }}')">
														<i class="far fa-trash-alt"></i>
													</button>
												</td>
                                                <td data-title="分類">
                                                    {{ $ca }}
                                                </td>
												<td data-title="標題">
                                                    {{ $item->title }}
												</td>
											</tr>
											@endforeach
										</form>
										{{-- 錯誤警示位置 --}}
				    	    			@include('errors.error')
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


		<!-- 新增區塊 -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">新增規格</h4>
					</div>
					<div class="modal-body">
						<form id="form_create">
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											<label>分類</label>
                                            <select class="form-control" name="category">
                                                <option value=""></option>
                                                <option value="curl">捲度</option>
                                                <option value="thickness">粗度</option>
                                                <option value="length">長度</option>
                                            </select>
										</div>
										<div class="form-group">
											<label>標題</label>
											<input type="text" class="form-control" name="title">
										</div>
										{{-- 錯誤警示位置 --}}
										<div id="error_message"></div>
									</div>
									<!-- /.col-lg-12 -->
								</div>
								<!-- /.row -->
							</div>
							<!-- /.panel-body -->
						</form>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-primary" value="確定送出" onclick="store()">
					</div>
				</div>
			</div>
		</div>
	@endsection

	@section('js')
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script src="/js/ajax.js"></script>
        <script type="text/javascript">
            function store()
            {
                var ajaxRequest = new ajaxCreate('POST','{{ route('create_type') }}',$('#form_create').serialize());
                ajaxRequest.request();
            }

            function alert_del(tag, id)
            {
                var obj = {id: id, tag: tag};
                swal.fire({
	                title: '您確定要刪除嗎？',
	                text: "此動作將不可回復。睫毛規格產品將無法回復",
	                icon: 'warning',
	                allowEscapeKey: false,
	                allowOutsideClick: false,
	                showCloseButton: true,
	                showCancelButton: true,
	                confirmButtonColor: '#3085d6',
	                cancelButtonColor: '#d33',
	                confirmButtonText: '確定',
	                cancelButtonText: '取消',
	            }).then((result) => {
	                if (result.value) {
	                    $.ajax({
	                        type: 'POST',
	                        url: '{{ route('delete_type') }}',
	                        headers: {
	                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                        },
	                        data: {
	                            id: obj,
	                        },
	                        success: function(data) {
	                            swal.fire({
	                                title: '成功刪除!',
	                                icon: 'success',
	                                showConfirmButton: false,
	                                timer: 1000,
	                            }).then(function(){
	                                location.reload();
	                            })
	                        },
	                    });
	                }
	            })
            }
        </script>
	@endsection
