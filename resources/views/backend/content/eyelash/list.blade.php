@extends('backend.content.eyelash.default')

	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
                            睫毛規格列表
						</h1>
					</div>
					@include('success')
					<div class="row">
						<div class="col-xs-12">
							<input type="button" class="btn btn-primary" value="儲存變更" onclick="$('#form').submit()">
							<div class="space"></div>
						</div>

						<div class="col-xs-12">
							<div class="table-responsive">
								<table class="table">
									<thead>
										<tr>
                                            <th>睫毛規格</th>
											<th>標題</th>
											<th>順序</th>
                                            <th>啟用狀態</th>
                                            <th>銷售狀態</th>
                                        </tr>
									</thead>

									<tbody>
										<form id="form" method="post" action="{{ route('update_product') }}">
											@csrf
											@foreach($products as $product)
											<input type="hidden" name="id[]" value="{{ $product->id }}">
											<tr>
                                                <td data-title="睫毛規格">
                                                    <a href="{{ route('eyelashList.detail', $product->id) }}" class="btn btn-info btn-outline btn-circle">
                                                        <i class="fas fa-eye"></i>
                                                    </a>
                                                </td>
												<td data-title="標題">
                                                    <input type="text" class="form-control" name="title[]" value="{{ $product->title }}">
												</td>
												<td data-title="順序">
													<input type="number" class="form-control" name="sort[]" pattern="[0-9]" value="{{ $product->sort }}">
												</td>
												<td data-title="啟用狀態">
													<select class="form-control" name="state[]">
														@if($product->state)
                                                            <option value="1" selected>啟用</option>
                                                            <option value="0">關閉</option>
														@else
                                                            <option value="1">啟用</option>
                                                            <option value="0" selected>關閉</option>
														@endif
													</select>
												</td>
                                                <td data-title="銷售狀態">
                                                    <select class="form-control" name="out[]">
                                                        @if($product->out)
                                                            <option value="1" selected>完售</option>
                                                            <option value="0">銷售中</option>
                                                        @else
                                                            <option value="1">完售</option>
                                                            <option value="0" selected>銷售中</option>
                                                        @endif
                                                    </select>
                                                </td>
											</tr>
											@endforeach
										</form>
										{{-- 錯誤警示位置 --}}
				    	    			@include('errors.error')
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	@endsection
