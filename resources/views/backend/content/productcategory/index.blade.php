@extends('backend.content.productcategory.default')

@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="page-content">
                <div class="page-header">
                    <h1>
                        分類管理
                    </h1>
                </div>
                @include('success')
                <div class="row">
                    <div class="col-xs-12">
                        <a href="#" class="btn btn-default" role="button" data-toggle="modal"
                           data-target="#myModal">新增</a>
                        <input type="button" class="btn btn-primary" value="儲存變更" onclick="$('#form').submit()">
                        <div class="space"></div>
                    </div>
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>產品</th>
                                    <th>刪除</th>
                                    <th>標題</th>
                                    <th>圖片<span style="color: red;">(700 * 221px)</span></th>
                                    <th>順序</th>
                                    <th>啟用狀態</th>
                                </tr>
                                </thead>

                                <tbody>
                                    <form id="form" method="post" action="{{ route('update_productCategory') }}">
                                        @csrf
                                        @foreach($productCategories as $key => $productCategory)
                                        <input type="hidden" name="id[]" value="{{ $productCategory->id }}">
                                        <input type="hidden" id="thumbnail{{$key}}" name="filepath[]" value="{{ $productCategory->src }}">
                                        <tr>
                                            <td data-title="產品">
                                                <a href="{{ route('backend.product', $productCategory->id) }}" class="btn btn-info btn-outline btn-circle">
                                                    <i class="fas fa-folder-open"></i>
                                                </a>
                                            </td>
                                            <td data-title="刪除">
                                                @if($productCategory->id > 2)
                                                    <button class="btn btn-danger btn-outline btn-circle" type="button"
                                                            onclick="alert_del('{{ $productCategory->id }}')">
                                                        <i class="far fa-trash-alt"></i>
                                                    </button>
                                                @endif
                                            </td>
                                            <td data-title="標題">
                                                @if(app()->getLocale() === 'zh-TW')
                                                    <input type="text" class="form-control" name="title[]"
                                                           value="{{ $productCategory->title }}">
                                                @else
                                                    <input type="text" class="form-control" name="title_en[]"
                                                           value="{{ $productCategory->title_en }}">
                                                @endif
                                            </td>
                                            <td id="holder{{$key}}" class="lfm" data-input="thumbnail{{$key}}" data-preview="holder{{$key}}" data-title="圖片">
                                                <img class="img-responsive img-shadow"
                                                      src="{{ $productCategory->src }}">
                                            </td>
                                            <td data-title="順序">
                                                <input type="number" class="form-control" name="sort[]" pattern="[0-9]" value="{{
                                                    $productCategory->sort }}">
                                            </td>
                                            <td data-title="啟用狀態">
                                                <select class="form-control" name="state[]">
                                                    @if($productCategory->state)
                                                        <option value="1" selected>啟用</option>
                                                        <option value="0">關閉</option>
                                                    @else
                                                        <option value="1">啟用</option>
                                                        <option value="0" selected>關閉</option>
                                                    @endif
                                                </select>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </form>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- 新增區塊 -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">新增分類</h4>
                </div>
                <div class="modal-body">
                    <form id="form_create">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>圖片<span style="color: red;">(圖片尺寸：700 * 221px)</span></label>
                                        <button class="form-control btn btn-primary lfm" data-input="thumbnail" data-preview="holder">
                                            <i class="far fa-image"></i>上傳圖片
                                        </button>
                                        <input id="thumbnail" class="form-control" type="hidden" name="filepath">
                                    </div>
                                    <div id="holder" class="form-group"></div>
                                    <div class="form-group">
                                        <label>標題</label>
                                        <input type="text" class="form-control" name="title">
                                    </div>
                                    <div class="form-group">
                                        <label>順序</label>
                                        <input type="number" class="form-control" name="sort" pattern="[0-9]" value="0">
                                    </div>
                                    {{-- 錯誤警示位置 --}}
                                    <div id="error_message"></div>
                                </div>
                                <!-- /.col-lg-12 -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </form>
                </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-primary" value="確定送出" onclick="store()">
                </div>
            </div>
        </div>
    </div>
@endsection


@section('js')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
    <script src="/js/ajax.js"></script>
    <script type="text/javascript">
        $(function() {
            $('.lfm').filemanager('image');
        });

        function store() {
            var ajaxRequest = new ajaxCreate('POST', '{{ route('create_productCategory') }}', $('#form_create').serialize());
            ajaxRequest.request();
        }

        function alert_del(id) {
            var ajaxRequest = new ajaxDel('POST', '{{ route('delete_productCategory') }}', id);
            ajaxRequest.request();
        }
    </script>
@endsection
