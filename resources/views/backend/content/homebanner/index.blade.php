@extends('backend.content.homebanner.default')

	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
                            首頁輪播管理
						</h1>
					</div>
					@include('success')
					<div class="row">
						<div class="col-xs-12">
							<a href="#" class="btn btn-default" role="button" data-toggle="modal" data-target="#myModal">新增</a>
							<input type="button" class="btn btn-primary" value="儲存變更" onclick="$('#form').submit()">
						</div>
						<div class="col-xs-12">
							<div class="table-responsive">
								<table class="table" style="table-layout: fixed;">
									<thead>
										<tr>
											<th>刪除</th>
											<th>電腦版圖片<span style="color: red;">(1024 * 552px)</span></th>
                                            <th>手機板圖片<span style="color: red;">(900 * 1600px)</span></th>
                                            <th>順序</th>
                                            <th>啟用狀態</th>
										</tr>
									</thead>

									<tbody>
										<form id="form" method="post" action="{{ route('update_home_banner') }}">
											@csrf
											@foreach($homeBanners as $key => $homeBanner)
											<input type="hidden" name="id[]" value="{{ $homeBanner->id }}">
											<input type="hidden" id="thumbnail{{$key}}" name="filepath[]" value="{{ $homeBanner->src }}">
                                            <input type="hidden" id="thumbnail-mobile{{$key}}" name="filepath-mobile[]" value="{{ $homeBanner->mobile_src }}">
											<tr>
												<td data-title="刪除">
													<button class="btn btn-danger btn-outline btn-circle" type="button" onclick="alert_del('{{ $homeBanner->id }}')">
														<i class="far fa-trash-alt"></i>
													</button>
												</td>
												<td id="holder{{$key}}" class="lfm" data-input="thumbnail{{$key}}" data-preview="holder{{$key}}" data-title="電腦版圖片">

													<img class="img-responsive img-shadow" src="{{ $homeBanner->src }}">
												</td>
                                                <td id="holder-mobile{{$key}}" class="lfm" data-input="thumbnail-mobile{{$key}}" data-preview="holder-mobile{{$key}}"
                                                    data-title="手機板圖片">
                                                    <img class="img-responsive img-shadow" src="{{ $homeBanner->mobile_src }}">
                                                </td>
                                                <td data-title="順序">
                                                    <input type="number" class="form-control" name="sort[]" pattern="[0-9]" value="{{ $homeBanner->sort }}">
                                                </td>
                                                <td data-title="啟用狀態">
                                                    <select class="form-control" name="state[]">
                                                        @if($homeBanner->state)
                                                        <option value="1" selected>啟用</option>
                                                        <option value="0">關閉</option>
                                                        @else
                                                        <option value="1">啟用</option>
                                                        <option value="0" selected>關閉</option>
                                                        @endif
                                                    </select>
                                                </td>
											</tr>
											@endforeach
										</form>
										{{-- 錯誤警示位置 --}}
				    	    			@include('errors.error')
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


		<!-- 新增區塊 -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">新增輪播</h4>
					</div>
					<div class="modal-body">
						<form id="form_create">
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											<label>電腦版圖片<span style="color: red;">(圖片尺寸：1024 * 552px)</span></label>
											<button class="form-control btn btn-primary lfm" data-input="thumbnail" data-preview="holder">
												<i class="far fa-image"></i>上傳圖片
											</button>
											<input id="thumbnail" class="form-control" type="hidden" name="filepath">
										</div>
										<div id="holder" class="form-group"></div>
                                        <div class="form-group">
                                            <label>手機版圖片<span style="color: red;">(圖片尺寸：900 * 1600px)</span></label>
                                            <button class="form-control btn btn-primary lfm" data-input="thumbnail-mobile" data-preview="holder-mobile">
                                                <i class="far fa-image"></i>上傳圖片
                                            </button>
                                            <input id="thumbnail-mobile" class="form-control" type="hidden" name="filepath-mobile">
                                        </div>
                                        <div id="holder-mobile" class="form-group"></div>
										<div class="form-group">
											<label>順序</label>
											<input type="number" class="form-control" name="sort" pattern="[0-9]" value="0">
										</div>
										<div id="error_message"></div>
									</div>
									<!-- /.col-lg-12 -->
								</div>
								<!-- /.row -->
							</div>
							<!-- /.panel-body -->
						</form>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-primary" value="確定送出" onclick="store()">
					</div>
				</div>
			</div>
		</div>
	@endsection

	@section('js')
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
	<script src="/js/ajax.js"></script>
	<script type="text/javascript">
		$(function() {
			$('.lfm').filemanager('image');
		});

		function store()
		{
			var ajaxRequest = new ajaxCreate('POST','{{ route('create_home_banner') }}',$('#form_create').serialize());
			ajaxRequest.request();
		}

		function alert_del(id)
		{
			var ajaxRequest = new ajaxDel('POST','{{ route('delete_home_banner') }}',id);
			ajaxRequest.request();
		}
	</script>
	@endsection
