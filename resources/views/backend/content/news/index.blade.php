@extends('backend.content.news.default')

	@section('content')
        <div class="main-content">
            <div class="main-content-inner">
                <div class="page-content">
                    <div class="page-header">
                        <h1>
                            活動訊息
                        </h1>
                    </div>
                    @include('success')
                    <div class="row">
                        <div class="col-xs-12">
                            <input type="button" class="btn btn-primary" value="儲存變更" onclick="$('#form').submit()">
                        </div>
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table class="table" style="table-layout: fixed;">
                                    <thead>
                                        <tr>
                                            <th>標題</th>
                                            <th>圖片<span style="color: red;">(800 * 800px)</span></th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <form id="form" method="post" action="{{ route('update_news') }}">
                                            @csrf
                                            <input type="hidden" id="thumbnail" name="filepath" value="{{ $news->src }}">
                                            <tr>
                                                <td data-title="標題">
                                                    <input type="text" class="form-control" name="title" value="{{ $news->title }}">
                                                </td>
                                                <td id="holder" class="lfm" data-input="thumbnail" data-preview="holder" data-title="圖片">
                                                    <img class="img-responsive img-shadow" src="{{ $news->src }}">
                                                </td>
                                            </tr>
                                        </form>
                                        {{-- 錯誤警示位置 --}}
                                        @include('errors.error')
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	@endsection

    @section('js')
        <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
        <script type="text/javascript">
            $(function() {
                $('.lfm').filemanager('image');
            });
        </script>
    @endsection
