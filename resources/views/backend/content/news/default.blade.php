@extends('backend.layouts.default')

	@section('navbar')
		@include('backend.layouts.navbar')
	@endsection

	@section('sidebar')
		@include('backend.layouts.sidebar')
	@endsection

	@section('footer')
		@include('backend.layouts.footer')
	@endsection
