@extends('backend.content.teach.default')

	@section('content')
        <div class="main-content">
            <div class="main-content-inner">
                <div class="page-content">
                    <div class="page-header">
                        <h1>
                            教學
                        </h1>
                    </div>
                    @include('success')
                    <div class="row">
                        <div class="col-xs-12">
                            <input type="button" class="btn btn-primary" value="儲存變更" onclick="$('#form').submit()">
                        </div>
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table class="table" style="table-layout: fixed;">
                                    <thead>
                                        <tr>
                                            <th>標題</th>
                                            <th>圖片<span style="color: red;">(800 * 800px)</span></th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <form id="form" method="post" action="{{ route('update_teach') }}">
                                            @csrf
                                            @foreach($teaches as $key => $teach)
                                                <input type="hidden" name="id[]" value="{{ $teach->id }}">
                                                <input type="hidden" id="thumbnail{{$key}}" name="filepath[]" value="{{ $teach->src }}">
                                                <tr>
                                                    <td data-title="標題">
                                                        <input type="text" class="form-control" name="title[]" value="{{ $teach->title }}">
                                                    </td>
                                                    <td id="holder{{$key}}" class="lfm" data-input="thumbnail{{$key}}" data-preview="holder{{$key}}" data-title="圖片">
                                                        <img class="img-responsive img-shadow" src="{{ $teach->src }}">
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </form>
                                        {{-- 錯誤警示位置 --}}
                                        @include('errors.error')
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	@endsection

    @section('js')
        <script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
        <script type="text/javascript">
            $(function() {
                $('.lfm').filemanager('image');
            });
        </script>
    @endsection
