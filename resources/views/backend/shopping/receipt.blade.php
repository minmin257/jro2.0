@extends('backend.shopping.default')

    @section('content')
        <div class="main-content">
            <div class="main-content-inner">
                <div class="page-content">
                    <div class="page-header">
                        <h1>
                            發票設定
                        </h1>
                    </div>
                    @include('success')
                    <div class="row">
                        <form method="post" action="{{ route('update_receipt') }}">
                            @csrf
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label>是否要開發票</label>
                                    @if($system->receipt)
                                        <div class="input-wrapper">
                                            <input type="radio" id="open_receipt" name="receipt" value="1" checked>
                                            <label for="open_receipt">開啟</label>
                                            <span></span>
                                            <input type="radio" id="close_receipt" name="receipt" value="0">
                                            <label for="close_receipt">關閉</label>
                                        </div>
                                    @else
                                        <div class="input-wrapper">
                                            <input type="radio" id="open_receipt" name="receipt" value="1">
                                            <label for="open_receipt">開啟</label>
                                            <span></span>
                                            <input type="radio" id="close_receipt" name="receipt" value="0" checked>
                                            <label for="close_receipt">關閉</label>
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>發票捐贈單位</label>
                                    <input type="text" name="title" class="form-control" value="{{ $receipt->title }}">
                                </div>
                                {{-- 錯誤警示位置 --}}
                                @include('errors.error')
                                <input type="submit" class="btn btn-primary" value="送出">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endsection
