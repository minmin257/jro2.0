@extends('backend.shopping.default')

    @section('content')
        <div class="main-content">
            <div class="main-content-inner">
                <div class="page-content">
                    <div class="page-header">
                        <h1>
                            滿額免運、貨到付款設定
                        </h1>
                    </div>
                    @include('success')
                    <div class="row">
                        <form method="post" action="{{ route('update_free') }}">
                            @csrf
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label>滿額免運</label>
                                    <input type="number" name="free" class="form-control" pattern="[0-9]" value="{{ $system->free }}">
                                </div>
                                <div class="form-group">
                                    <label>貨到付款</label>
                                    <input type="number" name="fee" class="form-control" pattern="[0-9]" value="{{ $method->fee }}">
                                </div>
                                {{-- 錯誤警示位置 --}}
                                @include('errors.error')
                                <input type="submit" class="btn btn-primary" value="送出">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endsection
