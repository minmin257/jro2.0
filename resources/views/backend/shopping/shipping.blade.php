@extends('backend.shopping.default')

    @section('content')
        <div class="main-content">
            <div class="main-content-inner">
                <div class="page-content">
                    <div class="page-header">
                        <h1>
                            運費設定
                        </h1>
                    </div>
                    @include('success')
                    <div class="row">
                        <div class="col-xs-12">
                            <input type="button" class="btn btn-primary" value="儲存變更" onclick="$('#form').submit()">
                        </div>
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table class="table" style="table-layout: fixed;">
                                    <thead>
                                        <tr>
                                            <th>標題</th>
                                            <th>金額</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <form id="form" method="post" action="{{ route('update_shipping') }}">
                                            @csrf
                                            @foreach($shippings as $shipping)
                                                <input type="hidden" name="id[]" value="{{ $shipping->id }}">
                                                <tr>
                                                    <td data-title="標題">
                                                        <input type="text" class="form-control" name="title[]" value="{{ $shipping->title }}">
                                                    </td>
                                                    <td data-title="金額">
                                                        <input type="number" class="form-control" name="fee[]" pattern="[0-9]" value="{{ $shipping->fee }}">
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </form>
                                        {{-- 錯誤警示位置 --}}
                                        @include('errors.error')
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
