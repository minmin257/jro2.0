@extends('backend.shopping.default')

    @section('content')
        <div class="main-content">
            <div class="main-content-inner">
                <div class="page-content">
                    <div class="page-header">
                        <h1>
                            訂購完成
                        </h1>
                    </div>
                    @include('success')
                    <div class="row">
                        <div class="col-xs-12">
                            <form method="post" action="{{ route('update_complete') }}">
                                @csrf
                                <div class="form-group">
                                    <label>內容</label>
                                    <textarea name="html" class="form-control editor">{!! $complete->html !!}</textarea>
                                </div>
                                {{-- 錯誤警示位置 --}}
                                @include('errors.error')
                                <input type="submit" class="btn btn-primary" value="送出">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection

    @section('js')
        {{-- <script src="//cdn.tinymce.com/4/tinymce.min.js"></script> --}}
        <script type="text/javascript" src="/js/tinymce.js"></script>
    @endsection
