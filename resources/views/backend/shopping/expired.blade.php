@extends('backend.shopping.default')

    @section('content')
        <div class="main-content">
            <div class="main-content-inner">
                <div class="page-content">
                    <div class="page-header">
                        <h1>
                            訂單逾期設定
                        </h1>
                    </div>
                    @include('success')
                    <div class="row">
                        <form method="post" action="{{ route('update_expired') }}">
                            @csrf
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label>逾期天數</label>
                                    <input type="number" name="expired" class="form-control" pattern="[0-9]" value="{{ $system->expired }}">
                                </div>
                                {{-- 錯誤警示位置 --}}
                                @include('errors.error')
                                <input type="submit" class="btn btn-primary" value="送出">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endsection
