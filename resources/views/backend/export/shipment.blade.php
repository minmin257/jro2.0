<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>銷貨單(訂單編號{{ $order->number }})</title>
        <style>
            @font-face {
                font-family: 'msjh';
                src: url('{{ base_path().'/public/' }}fonts/Microsoft-JhengHei.ttf') format('truetype');
            }
            @page {
                size: A4 portrait;/* 直式的A4大小 */
                margin: 0;
            }
            @media print{
                .print-page{
                    outline: none;
                }
            }
            html, body{
                width: 100%;
                height: 100%;
                position: relative;
                display: block;
                font-size: 12pt;
                line-height: 1em;
                font-family: 'msjh';
                margin: 0;
                padding: 0;
                -webkit-print-color-adjust: exact;
            }

        </style>
    </head>
    <body>
        <!-- 列印模板 -->
        <div id="PrintComponent" class="print-template" style=" width: 100%; display: block; position: relative; height: auto; box-sizing: border-box;">
            <!-- 頁面==================================================== -->
            <div class="print-page" style="width: 100%;padding: 1.5cm 1cm;">
                <!-- 頁首 -->
                <div class="print-header" style="width: 100%; display: block; overflow: hidden;">
                    <div class="header-title" style="width: 100%; display: block; position: relative; border-bottom: 1px solid #000000; padding: 0;">
                        <img src="{{ public_path().'/images/logo_icon.png' }}" alt="" style="width:100px;">
                    </div>
                </div>


                <!-- 頁面 -->
                <h2 style="text-align: center;">銷貨單</h2>
                <hr>
                <table style="width:100%;">
                    <tbody>
                        <tr>
                            <td style="width:2.3cm">訂單編號：</td>
                            <td>{{ $order->number }}</td>
                            <td style="width:2.3cm">建立時間：</td>
                            <td>{{ $order->created_at }}</td>
                        </tr>
                        <tr>
                            <td style="width:2.3cm">付款方式：</td>
                            <td>{{ $order->method->title }}</td>
                            <td style="width:2.3cm">運費：</td>
                            <td>{{ $order->method->fee }}</td>
                        </tr>
                        <tr>
                            <td style="width:2.3cm">總金額：</td>
                            <td>{{ $order->total }}</td>
                        </tr>
                        <tr>
                            <td style="width:2.3cm">需求備註：</td>
                            <td colspan="3">
                                <p style="word-wrap: break-word;">
                                {{ $order->demand }}
                                </p>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <h3 style="margin:.5cm 0 0 0;">購買人</h3>
                <table style="width:100%;">
                    <tbody>
                        <tr>
                            <td style="width:2.3cm">姓名：</td>
                            <td>{{ $order->buyer }}</td>
                            <td style="width:2.3cm">電話：</td>
                            <td>{{ $order->buyer_tel }}</td>
                        </tr>
                        <tr>
                            <td style="width:2.3cm">地址：</td>
                            <td colspan="3">{{ $order->buyer_addr }}</td>
                        </tr>
                        <tr>
                            <td style="width:2.3cm">Email：</td>
                            <td colspan="3">{{ $order->email }}</td>
                        </tr>
                    </tbody>
                </table>
                <h3 style="margin:.5cm 0 0 0;">收貨人</h3>
                <table style="width:100%;">
                    <tbody>
                        <tr>
                            <td style="width:2.3cm">姓名：</td>
                            <td>{{ $order->consignee }}</td>
                            <td style="width:2.3cm">電話：</td>
                            <td>{{ $order->consignee_tel }}</td>
                        </tr>
                        <tr>
                            <td style="width:2.3cm">地址：</td>
                            <td colspan="3">{{ $order->consignee_addr }}</td>
                        </tr>
                    </tbody>
                </table>
                <hr>
                <h3 style="text-align: center;margin:.5cm 0 .2cm 0;">商品明細</h3>
                <table class="print-table" style=" width: 100%; border-collapse: collapse;">
                    <thead>
                        <tr>
                            <th style="width:1cm;border: 1px solid #333333; padding: 2px 3px; text-align: center;">序號</th>
                            <th style="width:65%;border: 1px solid #333333; padding: 2px 3px; text-align: center;">商品</th>
                            <th style="width:10%;border: 1px solid #333333; padding: 2px 3px; text-align: center;">單價</th>
                            <th style="width:10%;border: 1px solid #333333; padding: 2px 3px; text-align: center;">數量</th>
                            <th style="width:10%;border: 1px solid #333333; padding: 2px 3px; text-align: center;">小計</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $quantity = 0;
                        @endphp
                        @foreach($order->detail as $key => $detail)
                            @php
                                $key++;
                                $sub = $detail->option->product->price * $detail->quantity;
                                $quantity += $detail->quantity;
                            @endphp
                            <tr>
                                <td style="border: 1px solid #333333; padding: 2px 3px; text-align: center;">{{ $key }}</td>
                                <td style="border: 1px solid #333333; padding: 2px 3px; text-align: left;">
                                    {{ $detail->option->product->title }}
                                    @if(isset($detail->option->curl_id) && isset($detail->option->thickness_id) && isset($detail->option->length_id))
                                    (捲度{{ $detail->option->curl->title }}粗度{{ $detail->option->thickness->title }}長度{{ $detail->option->length->title }})
                                    @endif
                                </td>
                                <td style="border: 1px solid #333333; padding: 2px 3px; text-align: center;">{{ $detail->option->product->price }}</td>
                                <td style="border: 1px solid #333333; padding: 2px 3px; text-align: center;">{{ $detail->quantity }}</td>
                                <td style="border: 1px solid #333333; padding: 2px 3px; text-align: center;">{{ $sub }}</td>
                            </tr>
                        @endforeach

                        <tr>
                            <td colspan="2" style="border: 1px solid #333333; padding: 2px 3px; text-align: center;">
                            </td>
                            <td style="text-align:center; border: 1px solid #333333; padding: 2px 3px; text-align: center;">
                                <strong>總計</strong>
                            </td>
                            <td style="border: 1px solid #333333; padding: 2px 3px; text-align: center;">
                                <strong>{{ $quantity }}</strong>
                            </td>
                            <td style="border: 1px solid #333333; padding: 2px 3px; text-align: center;">
                                <strong>{{ $order->price }}</strong>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <br>
                <br>
                <hr>
                <h3 style="margin:.5cm 0 .2cm 0;">注意事項</h3>
                <ul style="word-wrap: break-word;padding-left: 15px;">
                    <li>貨品配達後、如商品有瑕疵或品項有誤、辦理退換貨請在七天內寄還商品。經我方判定是我們的缺失、運費無條件由我方吸收；若未出現上述條件而要求退換貨品、運費則由買方支付。</li>
                    <li>客服電話：{{ $footer->tel }}</li>
                </ul>

            </div>
        </div>
    </body>
</html>
