@extends('backend.content.eyelash.default')

    @section('content')
        <div class="main-content">
            <div class="main-content-inner">
                <div class="page-content">
                    <div class="page-header">
                        <h1>
                            訂單管理
                        </h1>
                    </div>
                    @include('success')
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="btn-group btn-group-justified">
                                <a href="{{ route('backend.order') }}" class="btn {{ active('backend.order') ? 'btn-primary' : 'btn-default' }}">所有訂單</a>
                                <a href="{{ route('backend.arrearage') }}" class="btn {{ active('backend.arrearage') ? 'btn-primary' : 'btn-default' }}">未付款訂單</a>
                                <a href="{{ route('backend.shipment') }}" class="btn {{ active('backend.shipment') ? 'btn-primary' : 'btn-default' }}">待出貨訂單</a>
                                <a href="{{ route('backend.completed') }}" class="btn {{ active('backend.completed') ? 'btn-primary' : 'btn-default' }}">已完成訂單</a>
                                <a href="{{ route('backend.overdue') }}" class="btn {{ active('backend.overdue') ? 'btn-primary' : 'btn-default' }}">逾期未付訂單</a>
                                <a href="{{ route('backend.cancel') }}" class="btn {{ active('backend.cancel') ? 'btn-primary' : 'btn-default' }}">取消訂單</a>
                            </div>

                            <div class="table-responsive">
                                <table class="table" style="table-layout: fixed;">
                                    <thead>
                                        <tr>
                                            <th>查看</th>
                                            <th>銷貨單</th>
                                            <th>訂單編號</th>
                                            <th>購買人姓名</th>
                                            <th>連絡電話</th>
                                            <th>付款方式</th>
                                            <th>發票</th>
                                            <th>狀態</th>
                                            <th>建立時間</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($orders as $order)
                                        <tr>
                                            <td data-title="查看">
                                                <a href="{{ route('edit_order', $order->id) }}" class="btn btn-info btn-outline btn-circle">
                                                    <i class="fas fa-edit"></i>
                                                </a>
                                            </td>
                                            <td data-title="銷貨單">
                                                @if($order->type_id == 4)
                                                    <a href="{{ route('export_shipment', $order->id) }}" target="_blank" class="btn btn-info btn-outline btn-circle">
                                                        <i class="fas fa-file-pdf"></i>
                                                    </a>
                                                @endif
                                            </td>
                                            <td data-title="訂單編號">
                                                {{ $order->number }}
                                            </td>
                                            <td data-title="購買人姓名">
                                                {{ $order->buyer }}
                                            </td>
                                            <td data-title="連絡電話">
                                                {{ $order->buyer_tel }}
                                            </td>
                                            <td data-title="付款方式">
                                                {{ $order->method->title }}
                                            </td>
                                            <td data-title="發票">
                                                {{ $order->receipt->title }}
                                            </td>
                                            <td data-title="狀態">
                                                {{ $order->type->title }}
                                            </td>
                                            <td data-title="建立時間">
                                                {{ $order->created_at }}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
