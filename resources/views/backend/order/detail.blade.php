@extends('backend.order.default')

    @section('content')
        <div class="main-content">
            <div class="main-content-inner">
                <div class="page-content">
                    <div class="page-header">
                        <h1>
                            訂單編號 - {{ $order->number }}
                        </h1>
                    </div>
                    @include('success')
                    <div class="row">
                        <div class="col-xs-12">
                            <a href="{{ route('backend.order') }}" class="btn btn-default">返回</a>
                            @if($order->payment_id == 1)
                                @if($order->remittance)
                                    <a href="{{ route('backend.remittance', $order->id) }}" class="btn btn-success" style="float: right">修改匯款單</a>
                                @endif
                            @endif
                            <div class="space"></div>
                        </div>
                    </div>
                    <div class="row">
                        <form method="post" action="{{ route('update_order') }}">
                            @csrf
                            <input type="hidden" name="id" value="{{ $order->id }}">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label>購買人姓名</label>
                                    <p>{{ $order->buyer }}</p>
                                </div>
                                <div class="form-group">
                                    <label>購買人電話</label>
                                    <p>{{ $order->buyer_tel }}</p>
                                </div>
                                <div class="form-group">
                                    <label>購買人地址</label>
                                    <p>{{ $order->buyer_addr }}</p>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <p>{{ $order->email }}</p>
                                </div>
                                <div class="form-group">
                                    <label>需求備註</label>
                                    <p>{{ $order->demand }}</p>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label>收貨人姓名</label>
                                    <p>{{ $order->consignee }}</p>
                                </div>
                                <div class="form-group">
                                    <label>收貨人電話</label>
                                    <p>{{ $order->consignee_tel }}</p>
                                </div>
                                <div class="form-group">
                                    <label>收貨人地址</label>
                                    <p>{{ $order->consignee_addr }}</p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label>運費</label>
                                    <p>{{ $order->shipping->title }} {{ $order->shipping->fee }}元</p>
                                </div>
                                <div class="form-group">
                                    <label>總計</label>
                                    <p>{{ $order->total }}</p>
                                </div>
                                <div class="form-group">
                                    <label>匯款金額</label>
                                    @if($order->payment_id == 1)
                                        @if($order->remittance)
                                            <p>{{ $order->remittance->amount }}</p>
                                        @else
                                            <input type="text" class="form-control" name="amount" autocomplete="off">
                                        @endif
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>帳號末5碼</label>
                                    @if($order->payment_id == 1)
                                        @if($order->remittance)
                                            <p>{{ $order->remittance->account }}</p>
                                        @else
                                            <input type="text" class="form-control" name="account" autocomplete="off">
                                        @endif
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>匯款時間</label>
                                    @if($order->payment_id == 1)
                                        @if($order->remittance)
                                            <p>{{ $order->remittance->date }}</p>
                                        @else
                                            <input type="date" class="form-control" name="date">
                                        @endif
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>付款方式</label>
                                    <p>{{ $order->method->title }}</p>
                                </div>
                                <div class="form-group">
                                    <label>發票</label>
                                    <p>{{ $order->receipt->title }}</p>
                                </div>
                                <div class="form-group">
                                    <label>發票號</label>
                                    @if($order->type_id < 5)
                                        <input type="text" class="form-control" name="receipt_number" value="{{ $order->receipt_number }}" autocomplete="off">
                                    @else
                                        <p>{{ $order->receipt_number }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>發票抬頭</label>
                                    @if($order->type_id < 5)
                                        <input type="text" class="form-control" name="title" value="{{ $order->title }}" autocomplete="off">
                                    @else
                                        <p>{{ $order->title }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>統一編號</label>
                                    @if($order->type_id < 5)
                                        <input type="text" class="form-control" name="VAT" value="{{ $order->VAT }}" autocomplete="off">
                                    @else
                                        <p>{{ $order->VAT }}</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>狀態</label>
                                    <p>
                                        @foreach($types as $type)
                                            @if($order->type_id == $type->id)
                                                <label>
                                                    <input type="radio" name="type" value="{{ $type->id }}" checked>{{ $type->title }}
                                                </label>
                                            @else
                                                <label>
                                                    <input type="radio" name="type" value="{{ $type->id }}">{{ $type->title }}
                                                </label>
                                            @endif
                                        @endforeach
                                    </p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-12">
                                <div class="table-responsive">
                                    <table class="table" style="table-layout: fixed;">
                                        <thead>
                                            <tr>
                                                <th>產品名稱</th>
                                                <th>單價</th>
                                                <th>數量</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @foreach($order->detail as $detail)
                                                <tr>
                                                    <td data-title="產品名稱">
                                                        {{ $detail->option->product->title }}
                                                        <span style="margin-left: 10px;">
                                                            @if(isset($detail->option->curl_id) && isset($detail->option->thickness_id) && isset($detail->option->length_id))
                                                                    捲度{{ $detail->option->curl->title }}粗度{{ $detail->option->thickness->title }}長度{{
                                                                    $detail->option->length->title }}
                                                                @endif
                                                        </span>
                                                    </td>
                                                    <td data-title="單價">
                                                        {{ $detail->option->product->price }}
                                                    </td>
                                                    <td data-title="數量">
                                                        {{ $detail->quantity }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="form-group">
                                    <label>建立時間</label>
                                    <p>{{ $order->created_at }}</p>
                                </div>

                                {{-- 錯誤警示位置 --}}
                                @include('errors.error')
                                <input type="submit" class="btn btn-primary" value="送出">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endsection
