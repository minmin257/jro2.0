@extends('backend.order.default')

    @section('content')
        <div class="main-content">
            <div class="main-content-inner">
                <div class="page-content">
                    <div class="page-header">
                        <h1>
                            {{ $order->number }} - 修改匯款單
                        </h1>
                    </div>
                    @include('success')
                    <div class="row">
                        <div class="col-xs-12">
                            <a href="{{ route('edit_order', $order->id) }}" class="btn btn-default">返回</a>
                            <div class="space"></div>
                        </div>
                    </div>
                    <div class="row">
                        <form method="post" action="{{ route('update_remittance') }}">
                            @csrf
                            <input type="hidden" name="id" value="{{ $order->id }}">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label>匯款金額</label>
                                    <input type="text" class="form-control" name="amount" value="{{ $order->remittance->amount }}" autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <label>帳號末5碼</label>
                                    <input type="text" class="form-control" name="account" value="{{ $order->remittance->account }}" autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <label>匯款時間</label>
                                    <input type="date" class="form-control" name="date" value="{{ $order->remittance->date }}" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-12">
                                {{-- 錯誤警示位置 --}}
                                @include('errors.error')
                                <input type="submit" class="btn btn-primary" value="送出">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endsection
