<!DOCTYPE html>

<html lang="en">

  <head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">



    <!-- 網站標題icon -->

    <title>香草市場街 美睫職人</title>

    <link rel="shortcut icon" href="/images/salon/logo-icon.png" />

    <meta name="keywords" content="美睫、接睫毛、台北市中正區、台電大樓接睫毛、公館接睫毛">

    <meta name="description" content="香草市場街 美睫職人，台北市中正區接睫毛職人，羅斯福路接睫毛，近捷運台電大樓站、捷運公館站，專業、美麗、自信">



		<link rel="stylesheet" href="{{ asset('/css/salon/bootstrap.min.css') }}">

    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+TC&display=swap" rel="stylesheet">

		<link rel="stylesheet" href="{{ asset('/css/salon/bootstrap_r.css') }}">

		<link rel="stylesheet" href="{{ asset('/css/salon/animate.css') }}">

		<link rel="stylesheet" href="{{ asset('/css/salon/lightbox.css') }}">

		<link rel="stylesheet" href="{{ asset('/css/salon/style.css') }}">



  </head>

  <body>

    <div class="wrap">

      <!-- header===================================================== -->

      <div class="header">

        <nav class="navbar navbar-expand-lg">

          <div class="container">

            <a class="navbar-brand" href="javascript:;">

              <img src="/images/salon/logo.png" alt="">

              <h1 class="sr-only">香草市場街 美睫職人</h1>

            </a>

            <div>

              <ul class="navbar-nav ml-auto">

                <li class="nav-item">

                  <a class="nav-link tel" href="tel:+8860936194600">

                    <i class="fas fa-phone-square-alt"></i>

                  </a>

                </li>

                <li class="nav-item">

                  <a class="nav-link line" href="https://line.me/ti/p/QWgu2e3IFF" target="_blank">

                    <i class="fab fa-line"></i>

                  </a>

                </li>

              </ul>

            </div>

          </div>

        </nav>

      </div>



      <content>

        <div class="container">







          <!-- banner -->

          <div class="banner mb-5">

            <div id="carouselBanner" class="carousel slide" data-ride="carousel">

              <!-- <ol class="carousel-indicators">

                <li data-target="#carouselBanner" data-slide-to="0" class="active"></li>

                <li data-target="#carouselBanner" data-slide-to="1"></li>

                <li data-target="#carouselBanner" data-slide-to="2"></li>

                <li data-target="#carouselBanner" data-slide-to="3"></li>

                <li data-target="#carouselBanner" data-slide-to="4"></li>

                <li data-target="#carouselBanner" data-slide-to="5"></li>

              </ol> -->

              <div class="carousel-inner">

                <div class="carousel-item active">

                  <img src="/images/salon/banner-05.jpg" class="d-block w-100">

                </div>

                <div class="carousel-item">

                  <img src="/images/salon/banner-02.jpg" class="d-block w-100">

                </div>

                <div class="carousel-item">

                  <img src="/images/salon/banner-03.jpg" class="d-block w-100">

                </div>

                <div class="carousel-item">

                  <img src="/images/salon/banner-01.jpg" class="d-block w-100">

                </div>

                <div class="carousel-item">

                  <img src="/images/salon/banner-04.jpg" class="d-block w-100">

                </div>

                <div class="carousel-item">

                  <img src="/images/salon/banner-07.jpg" class="d-block w-100">

                </div>

                <div class="carousel-item">

                  <img src="/images/salon/banner-08.jpg" class="d-block w-100">

                </div>

              </div>

              <a class="carousel-control-prev" href="#carouselBanner" role="button" data-slide="prev">

                <span class="carousel-control-prev-icon" aria-hidden="true"></span>

                <span class="sr-only">Previous</span>

              </a>

              <a class="carousel-control-next" href="#carouselBanner" role="button" data-slide="next">

                <span class="carousel-control-next-icon" aria-hidden="true"></span>

                <span class="sr-only">Next</span>

              </a>

            </div>

          </div>









          <!-- line ID -->

          <div class="line-contact">

            <div class="row align-items-center justify-content-center">

              <div class="col-6 col-sm-4 col-md-3 col-lg-2 mb-3">

                <a href="https://line.me/ti/p/QWgu2e3IFF" target="_blank"><img class="w-100" src="/images/salon/line-qr.jpg" alt=""></a>

              </div>

              <div class="col-6 col-sm-8 col-md-9 col-lg-8 mb-3">

                <div class="line-contact-text tc">

                  <p>

                    完全預約制‧僅接受LINE預約 <img style="height: 30px;" src="/images/salon/icon-eye.png" alt="">

                  </p>

                </div>

              </div>

            </div>

          </div>



          <hr class="bc-bda6c4 my-5">









          <!-- information -->

          <div class="information">

            <h3 class="block-title bl-bda6c4">INFORMATION <small class="d-inline-block c-bda6c4">インフォメーション</small></h3>

            <div class="table-responsive">

              <table class="table table-style">

                <tbody>

                  <tr>

                    <td>名称</td>

                    <td>香草市場街 美睫職人</td>

                  </tr>

                  <tr>

                    <td>Line ID</td>

                    <td>

                      <a href="https://line.me/ti/p/IenqTqYRGr" target="_blank">woddy0321</a>

                    </td>

                  </tr>

                  <tr>

                    <td>住所</td>

                    <td>

                      <a href="https://goo.gl/maps/VpYr8dhdq3sTwghk6" target="_blnak">台北市中正區羅斯福路三段210巷8弄2號1樓</a>

                    </td>

                  </tr>

                  <tr>

                    <td>営業時間</td>

                    <td>14:00~21:30(最終受付20:00)</td>

                  </tr>

                  <tr>

                    <td>定休日</td>

                    <td>無</td>

                  </tr>

                  <tr>

                    <td>座席</td>

                    <td>1</td>

                  </tr>

                  <tr>

                    <td>駐車場</td>

                    <td>羅斯福路三段辛亥路口</td>

                  </tr>

                </tbody>

              </table>

            </div>

          </div>



          <hr class="bc-d99ca8 my-5">







          <!-- Price -->

          <div class="Price">

            <h3 class="block-title bl-d99ca8">PRICE LIST <small class="d-inline-block c-d99ca8">料金表</small></h3>

          </div>

          <div class="row">

            <div class="col-12 col-md-4 mb-3">

              <div class="card h-100  bc-d99ca8">

                <div class="card-body price-info">

                  <strong class="title c-d99ca8" style="border-bottom:1px solid #d99ca8;">最初回</strong>

                  <p>

                    <strong style="font-size: 20px;">$ 819元</strong><br>

                    (不限根數)

                  </p>

                </div>

              </div>

            </div>

            <div class="col-12 col-md-4 mb-3">

              <div class="card h-100 bc-bda6c4">

                <div class="card-body price-info">

                  <strong class="title c-bda6c4" style="border-bottom:1px solid #bda6c4;">只卸不接</strong>

                  <p>

                    <strong style="font-size: 20px;">$ 300元</strong>

                  </p>

                </div>

              </div>

            </div>

            <div class="col-12 col-md-4 mb-3">

              <div class="card h-100 bc-96c7e1">

                <div class="card-body price-info">

                  <strong class="title c-96c7e1" style="border-bottom:1px solid #96c7e1;">全卸重接</strong>

                  <p>

                    <strong style="font-size: 20px;">$ 920元起</strong><br>

                    (卸除不收費 / 詳下表)

                  </p>

                </div>

              </div>

            </div>

          </div>



          <div class="table-responsive">

            <table class="table tc" style="border:2px solid #96c7e1;">

              <thead>

                <tr>

                  <th colspan="2" style="width: 50%;border-right: 2px solid #96c7e1;border-bottom:1px solid #96c7e1;">

                    兩目本數<span class="d-inline-block">(含眼膜)</span>

                  </th>

                  <th colspan="2" style="width: 50%;border-left: 2px solid #96c7e1;border-bottom:1px solid #96c7e1;">

                    兩目本數<span class="d-inline-block">(山茶花)</span>

                  </th>

                </tr>

              </thead>

              <tbody>

                <tr>

                  <td>160本</td>

                  <td style="border-right: 2px solid #96c7e1;">920元</td>

                  <td style="border-left: 2px solid #96c7e1;">300本</td>

                  <td>920元</td>

                </tr>

                <tr>

                  <td>200本</td>

                  <td style="border-right: 2px solid #96c7e1;">1120元</td>

                  <td style="border-left: 2px solid #96c7e1;">500本</td>

                  <td>1120元</td>

                </tr>

                <tr>

                  <td>250本</td>

                  <td style="border-right: 2px solid #96c7e1;">1320元</td>

                  <td style="border-left: 2px solid #96c7e1;">700本</td>

                  <td>1320元</td>

                </tr>

              </tbody>

            </table>

          </div>



          <hr class="bc-adbb50 my-5">







          <!-- Price -->

          <div class="Price">

            <h3 class="block-title bl-adbb50">MAP＆ACCESS <small class="d-inline-block c-adbb50">アクセス</small></h3>

          </div>

          <div class="row">
            <div class="col-12 col-lg-5">
              <img class="w-100" src="/images/salon/map.jpg" alt="">
            </div>
            <div class="col-12 col-lg-7">
              <div class="map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d903.86575080884!2d121.52847458812124!3d25.018356714265924!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442a98c2f521ce7%3A0x6de6e526b45372b4!2z6aaZ6I2J5biC5aC06KGXLeaXpeW8j-e-juedqw!5e0!3m2!1szh-TW!2stw!4v1627046868974!5m2!1szh-TW!2stw" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
              </div>
              
            </div>
          </div>










        </div>

      </content>







    </div>

    <!-- footer======================================================= -->

    <div class="footer">

      <div class="container">

        <div class="text">

          Copyright © 香草市場街 美睫職人. All Rights Reserved.

        </div>

      </div>

    </div>



    <!-- JavaScript -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

    <script type="text/javascript" src="{{ asset('/js/salon/bootstrap.min.js') }}"></script>


    <script src="https://kit.fontawesome.com/588be6838c.js"></script>



</body>

</html>