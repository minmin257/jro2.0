<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teach extends Model
{
    protected $fillable = [
        'title', 'src'
    ];
}
