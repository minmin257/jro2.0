<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode as Middleware;
use Closure;
use App\Repositories\SystemRepository;

class CheckForMaintenanceMode extends Middleware
{
    protected $System;

    public function __construct(SystemRepository $System)
    {
        $this->System = $System;
    }

    public function handle($request, Closure $next)
    {
        $system = $this->System->get_first();
        if($system->maintain)
        {
            abort(503,$system->message);
        }

        return $next($request);
    }

}
