<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class SystemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sitename' => ['sometimes', 'required'],
            'meta_description' => ['sometimes', 'required'],
            'meta_keyword' => ['sometimes', 'required'],
            'maintain' => ['sometimes', 'required', 'numeric'],
            'message' => ['sometimes', 'required'],
            'name' => ['sometimes', 'required'],
            'tel' => ['sometimes', 'required'],
            'fax' => ['sometimes', 'required'],
            'email' => ['sometimes', 'required', 'email'],
        ];
    }

}
