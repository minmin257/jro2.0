<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class UpdateFreeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'free' => ['filled', 'numeric'],
            'fee' => ['filled', 'numeric']
        ];
    }

    public function attributes()
    {
        return [
            'free' => '滿額免運',
            'fee' => '貨到付款手續費',
        ];
    }

}
