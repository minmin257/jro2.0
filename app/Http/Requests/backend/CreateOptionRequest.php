<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;
use App\ProductOption;

class CreateOptionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $Exist = ProductOption::where('product_id', $this->id)->where('curl_id', $this->curl)->where('thickness_id', $this->thickness)->where('length_id', $this->length)->first();

        if ($Exist) {
            $this->merge([
                'exist' => null
            ]);
        } else {
            $this->merge([
                'exist' => 1
            ]);
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => ['filled', 'exists:products,id'],
            'curl' => ['filled'],
            'thickness' => ['filled'],
            'length' => ['filled'],
            'exist' => ['filled']
        ];
    }

    public function attributes()
    {
        return [
            'curl' => '捲度',
            'thickness' => '粗度',
            'length' => '長度'
        ];
    }

    public function messages()
    {
        return [
            'exist.filled' => '已有此規格'
        ];
    }
}
