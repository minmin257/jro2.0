<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRemittanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => ['filled', 'numeric'],
            'account' => ['filled', 'numeric', 'digits:5'],
            'date' => ['filled', 'date']
        ];
    }

    public function attributes()
    {
        return [
            'amount' => '匯款金額',
            'account' => '帳號末5碼',
            'date' => '匯款時間',
        ];
    }

}
