<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class UpdateFilterProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => ['filled', 'exists:products,id'],
            'title' => ['filled'],
            'spec' => ['filled'],
            'price' => ['filled', 'numeric'],
            'sort' => ['filled', 'numeric'],
            'html' => ['filled'],
        ];
    }

    public function attributes()
    {
        return [
            'category' => '分類',
            'spec' => '規格',
            'price' => '金額',
            'html' => '產品介紹'
        ];
    }
}
