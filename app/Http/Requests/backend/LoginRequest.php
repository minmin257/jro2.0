<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\Activated;
use App\Rules\Deleted;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'account' => ['required', 'regex:/(^([a-zA-Z0-9]+)(\d+)?$)/u', new Activated, new Deleted],
            'password' => ['required', 'between:4,10', 'regex:/(^([a-zA-Z0-9]+)(\d+)?$)/u'],
            'g-recaptcha-response' => ['required', 'captcha'],
        ];
    }

}
