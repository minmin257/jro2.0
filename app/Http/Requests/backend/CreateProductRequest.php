<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category' => ['filled', 'exists:product_categories,id'],
            'filepath' => ['filled'],
            'title' => ['filled'],
            'spec' => ['filled'],
            'price' => ['filled', 'numeric'],
            'html' => ['filled'],
            'sort' => ['filled', 'numeric'],
        ];
    }

    public function attributes()
    {
        return [
            'category' => '分類',
            'filepath' => '圖片',
            'spec' => '規格',
            'price' => '金額',
            'html' => '產品介紹'
        ];
    }

}
