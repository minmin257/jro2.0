<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id.*' => ['filled', 'exists:products,id'],
            'title.*' => ['filled'],
            'sort.*' => ['filled', 'numeric'],
            'state.*' => ['filled'],
            'out.*' => ['filled']
        ];
    }

}
