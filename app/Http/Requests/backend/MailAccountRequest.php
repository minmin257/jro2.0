<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class MailAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => ['filled', 'email'],
            'password' => ['filled'],
            'name' => ['filled'],
            'subject' => ['filled'],
            'recipient' => ['nullable','regex:/^(\s?[^\s,%$#!~+=*]+@[^\s,%$#!~+=*]+\.[^\s,%$#!~+=*]+\s?,)*(\s?[^\s,%$#!~+=*]+@[^\s,%$#!~+=*]+\.[^\s,%$#!~+=*]+)$/'],
        ];
    }

    public function attributes()
    {
        return [
            'name' => '寄件者名稱',
            'subject' => '寄件主旨',
        ];
    }

}
