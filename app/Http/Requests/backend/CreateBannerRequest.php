<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class CreateBannerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'filepath' => ['filled'],
            'filepath-mobile' => ['filled'],
            'sort' => ['filled', 'numeric'],
        ];
    }

    public function attributes()
    {
        return [
            'filepath' => '電腦版圖片',
            'filepath-mobile' => '手機版圖片',
            'sort' => '順序'
        ];
    }

}
