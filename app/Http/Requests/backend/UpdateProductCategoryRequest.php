<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id.*' => ['filled', 'exists:product_categories,id'],
            'filepath.*' => ['filled'],
            'title.*' => ['filled'],
            'sort.*' => ['filled', 'numeric'],
            'state.*' => ['filled'],
        ];
    }

    public function attributes()
    {
        return [
            'filepath.*' => '圖片',
        ];
    }

}
