<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBannerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id.*' => ['filled', 'exists:home_banners,id'],
            'filepath.*' => ['filled'],
            'filepath-mobile.*' => ['filled'],
            'sort.*' => ['filled', 'numeric'],
            'state.*' => ['filled'],
        ];
    }

    public function attributes()
    {
        return [
            'filepath.*' => '電腦版圖片',
            'filepath-mobile.*' => '手機版圖片',
            'sort.*' => '順序'
        ];
    }

}
