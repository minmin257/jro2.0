<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CheckoutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => ['required', 'array'],
            'id.*' => ['filled', 'exists:product_options,id'],
            'quantity' => ['required', 'array'],
            'quantity.*' => ['filled', 'numeric', 'min:1'],
            'subtotal' => ['numeric'],
            'shipping' => ['filled', 'exists:shippings,id'],
            'total' => ['filled', 'numeric'],
            'payment' => ['filled', 'exists:payment_methods,id']
        ];
    }

    public function attributes()
    {
        return [
            'id' => '購物車',
            'quantity' => '數量',
            'quantity.*' => '數量',
            'subtotal' => '小計',
            'shipping' => '運費',
            'total' => '應負金額',
            'payment' => '付款方式'
        ];
    }

}
