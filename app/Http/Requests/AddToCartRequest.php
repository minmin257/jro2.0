<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddToCartRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'curl' => ['filled'],
            'thickness' => ['filled'],
            'length' => ['filled'],
            'quantity' => ['filled']
        ];
    }

    public function attributes()
    {
        return [
            'curl' => '捲度',
            'thickness' => '粗度',
            'length' => '長度',
            'quantity' => '數量'
        ];
    }

}
