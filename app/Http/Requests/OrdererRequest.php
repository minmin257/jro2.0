<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrdererRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'buyer' => ['filled'],
            'buyer_tel' => ['filled'],
            // 'buyer_tel' => ['filled', 'regex:/(^(\d{2,3}-\d{7,8}|09\d{2}-\d{3}-\d{3})?$)/u'],
            'buyer_addr' => ['filled'],
            'email' => ['filled'],
            'consignee' => ['filled'],
            'consignee_tel' => ['filled'],
            // 'consignee_tel' => ['filled', 'regex:/(^(\d{2,3}-\d{7,8}|09\d{2}-\d{3}-\d{3})?$)/u'],
            'consignee_addr' => ['filled'],
            'receipt' => ['filled', 'exists:receipt_types,id'],
            'title' => ['sometimes', 'required_without:VAT'],
            'VAT' => ['sometimes', 'required_without:title'],
            'g-recaptcha-response' => ['filled', 'captcha'],
        ];
    }

    public function attributes()
    {
        return [
            'buyer' => '購買人姓名',
            'buyer_tel' => '購買人電話',
            'buyer_addr' => '購買人地址',
            'email' => 'Email',
            'consignee' => '收貨人姓名',
            'consignee_tel' => '收貨人電話',
            'consignee_addr' => '收貨人地址',
            'receipt' => '發票資訊',
            'title' => '抬頭',
            'VAT' => '統編',
            'g-recaptcha-response' => '驗證碼'
        ];
    }

    public function messages()
    {
        return [
            'buyer_tel.regex' => '購買人電話 的格式為xx-xxxxxxx 或 09xx-xxx-xxx。',
            'consignee_tel.regex' => '收貨人電話 的格式為xx-xxxxxxx 或 09xx-xxx-xxx。'
        ];
    }

}
