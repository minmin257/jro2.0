<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RemittanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number' => ['filled', 'numeric'],
            'phone' => ['filled'],
            // 'phone' => ['filled', 'regex:/(^(\d{2,3}-\d{7,8}|09\d{2}-\d{3}-\d{3})?$)/u'],
            'date' => ['filled', 'date'],
            'amount' => ['filled', 'numeric'],
            'account' => ['filled', 'numeric', 'digits:5']
        ];
    }

    public function attributes()
    {
        return [
            'number' => '訂單編號',
            'phone' => '手機號碼',
            'date' => '匯款時間',
            'amount' => '匯款金額',
            'account' => '帳號後五碼',
        ];
    }

}
