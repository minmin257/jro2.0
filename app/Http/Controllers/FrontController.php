<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\TeachRepository;
use App\Repositories\ProductCategoryRepository;
use App\Repositories\ProductRepository;
use App\Repositories\ProductOptionRepository;
use App\Repositories\SystemRepository;
use App\Repositories\ShippingRepository;
use App\Repositories\PaymentMethodRepository;
use App\Repositories\NoteRepository;
use App\Repositories\ReceiptTypeRepository;
use App\Repositories\ReceiptRepository;
use App\Repositories\OrderRepository;
use App\Repositories\CompleteRepository;
use App\Services\MailService;
use App\Http\Requests\AddToCartRequest;
use App\Http\Requests\CheckoutRequest;
use App\Http\Requests\OrdererRequest;
use App\Http\Requests\RemittanceRequest;
use App\Cart;

class FrontController extends Controller
{
    protected $Teach, $ProductCategory, $Product, $ProductOption, $System, $Shipping, $PaymentMethod, $Note, $Receipt, $Order, $Complete;

    public function __construct(TeachRepository $Teach, ProductCategoryRepository $ProductCategory, ProductRepository $Product, ProductOptionRepository $ProductOption, SystemRepository $System, ShippingRepository $Shipping, PaymentMethodRepository $PaymentMethod, NoteRepository $Note, ReceiptTypeRepository $ReceiptType, ReceiptRepository $Receipt, OrderRepository $Order, CompleteRepository $Complete)
    {
        $this->Teach = $Teach;
        $this->ProductCategory = $ProductCategory;
        $this->Product = $Product;
        $this->ProductOption = $ProductOption;
        $this->System = $System;
        $this->Shipping = $Shipping;
        $this->PaymentMethod = $PaymentMethod;
        $this->ReceiptType = $ReceiptType;
        $this->Note = $Note;
        $this->Receipt = $Receipt;
        $this->Order = $Order;
        $this->Complete = $Complete;
    }

    public function index()
    {
        return view('front.home.index');
    }

    public function salon(){
        return view('salon.index');
    }

    public function category($category_id)
    {
        $category = $this->ProductCategory->all_query()->where('state', 1)->findOrFail($category_id);
        return view('front.category.index', compact('category'));
    }

    public function product($category_id, $product_id)
    {
        $category = $this->ProductCategory->all_query()->where('state', 1)->findOrFail($category_id);
        $product = $category->product()->where('state', 1)->findOrFail($product_id);
        return view('front.product.index', compact('category', 'product'));
    }

    public function filter(Request $request)
    {
        if ($request->filled('thickness') && $request->filled('curl')) {
            $product = $this->ProductOption->all_query()->where('curl_id', $request->curl)->join('length_types', 'product_options.length_id', 'length_types.id')->join('lengths', 'product_options.length_id', 'lengths.length_id')->where('product_options.state', 1)->where('lengths.state', 1)->where('product_options.product_id', $request->product)->where('thickness_id', $request->thickness)->select('product_options.length_id', 'title', 'lengths.out', 'product_options.out AS option_out')->distinct()->orderByRaw('LENGTH(title)', 'asc')->orderBy('title', 'asc')->get()->toJson();
        } elseif ($request->filled('curl')) {
            $product = $this->ProductOption->all_query()->join('thickness_types', 'product_options.thickness_id', 'thickness_types.id')->join('thicknesses', 'product_options.thickness_id', 'thicknesses.thickness_id')->where('product_options.state', 1)->where('thicknesses.state', 1)->where('product_options.product_id', $request->product)->where('curl_id', $request->curl)->select('product_options.thickness_id', 'title', 'thicknesses.out')->distinct()->orderBy('title', 'asc')->get()->toJson();
        } else {
            $product = $this->ProductOption->all_query()->join('curl_types', 'product_options.curl_id', 'curl_types.id')->join('curls', 'product_options.curl_id', 'curls.curl_id')->where('product_options.state', 1)->where('curls.state', 1)->where('product_options.product_id', $request->product)->select('product_options.curl_id', 'title', 'curls.out')->distinct()->orderByRaw('LENGTH(title)', 'asc')->orderBy('title', 'asc')->get()->toJson();
        }

        return $product;
    }

    public function addToCart(AddToCartRequest $request)
    {
        $product = $this->ProductOption->all_query()->where('product_id', $request->product)->where('curl_id', $request->curl)->where('thickness_id', $request->thickness)->where('length_id', $request->length)->first();
        $oldCart = session()->has('cart') ? session()->get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->add($product, $product->id, $request->quantity);

        session()->put('cart', $cart);
        return back();
    }

    public function order()
    {
        return view('front.order.index');
    }

    public function remittance(RemittanceRequest $request)
    {
        $find = $this->Order->get_all()->where('payment_id', 1)->where('type_id', 1)->where('number', $request->number)->where('buyer_tel', $request->phone)->where('total', $request->amount)->first();
        if (isset($find)) {
            $this->Order->update_type($find->id, $request);

            return back()->with('success', '填寫成功，我們會盡速為您出貨');
        } else {
            return back()->withErrors([
                'message'=>'找不到訂單'
            ]);
        }
    }

    public function teach()
    {
        $teaches = $this->Teach->get_all();
        return view('front.teach.index', compact('teaches'));
    }

    public function cart()
    {
        $system = $this->System->get_first();
        $shippings = $this->Shipping->get_all();
        $payment_methods = $this->PaymentMethod->get_all();
        return view('front.cart.step1', compact('system', 'shippings', 'payment_methods'));
    }

    public function removeToCart(Request $request)
    {
        $product = $this->ProductOption->all_query()->find($request->id);
        $oldCart = session()->has('cart') ? session()->get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->remove($product->id);

        session()->put('cart', $cart);
    }

    public function updateCart(Request $request)
    {
        $oldCart = session()->has('cart') ? session()->get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->update($request->id, $request->quantity);

        session()->put('cart', $cart);
    }

    public function note()
    {
        $note = $this->Note->get_first();
        return view('front.cart.note', compact('note'));
    }

    public function validateCheckout(CheckoutRequest $request)
    {
        session()->put('checkout', $request->all());
        return redirect()->route('front.cart2');
    }

    public function cart2()
    {
        if (session()->get('checkout')) {
            $system = $this->System->get_first();
            $receiptTypes = $this->ReceiptType->get_all();
            $receipt = $this->Receipt->get_first();
            return view('front.cart.step2', compact('system', 'receiptTypes', 'receipt'));
        } else {
            return redirect()->route('front.cart');
        }
    }

    public function validateOrderer(OrdererRequest $request, MailService $MailService)
    {
        session()->put('orderer', $request->all());
        $order = $this->create($MailService);

        return $order;
    }

    public function create($MailService)
    {
        \Log::debug(session()->get('checkout'));
        \Log::debug(session()->get('orderer'));

        \DB::beginTransaction();
        try {
            $order = $this->Order->create();

            \DB::commit();
        }
        catch (\Exception $e) {
            \DB::rollback();
        }

        $jsonResponse = $MailService->OrderCompleted($order);
        if ($jsonResponse) {
            $content = $jsonResponse->getContent();
            $array = json_decode($content, true);
            return $jsonResponse;
        } else {
            return $order->id;
        }

    }

    public function complete(Request $request)
    {
        if ($request->filled('id')) {
            session()->forget('cart');
            session()->forget('checkout');
            session()->forget('orderer');
            $order = $this->Order->get_all()->find($request->id);
            $complete = $this->Complete->get_first();
            return view('front.cart.complete', compact('order', 'complete'));
        } else {
            return redirect()->route('front.cart');
        }
    }

}
