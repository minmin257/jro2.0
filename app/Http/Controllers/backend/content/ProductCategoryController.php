<?php

namespace App\Http\Controllers\backend\content;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ProductCategoryRepository;
use App\Http\Requests\backend\CreateProductCategoryRequest;
use App\Http\Requests\backend\UpdateProductCategoryRequest;
use App\Http\Requests\backend\UpdateProductCategoryDetailRequest;

class ProductCategoryController extends Controller
{
    protected $ProductCategory;

	public function __construct(ProductCategoryRepository $ProductCategory)
    {
        $this->ProductCategory = $ProductCategory;
    }

    public function index()
    {
        $productCategories = $this->ProductCategory->all_query()->get();
        return view('backend.content.productcategory.index', compact('productCategories'));
    }

    public function create(CreateProductCategoryRequest $request)
    {
    	$this->ProductCategory->create($request);
    }

    public function update(UpdateProductCategoryRequest $request)
    {
    	$this->ProductCategory->update($request);
    	return back()->with('message', '更新完成');
    }

    public function delete(Request $request)
    {
        $this->ProductCategory->delete($request);
    }

}
