<?php

namespace App\Http\Controllers\backend\content;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\TeachRepository;
use App\Http\Requests\backend\UpdateTeachRequest;

class TeachController extends Controller
{
    protected $Teach;

	public function __construct(TeachRepository $Teach)
    {
        $this->Teach = $Teach;
    }

    public function index()
    {
    	$teaches = $this->Teach->get_all();
    	return view('backend.content.teach.index',compact('teaches'));
    }

    public function update(UpdateTeachRequest $request)
    {
        $this->Teach->update($request);
        return back()->with('message', '更新完成');
    }

}
