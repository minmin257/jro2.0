<?php

namespace App\Http\Controllers\backend\content;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\HomeBannerRepository;
use App\Http\Requests\backend\CreateBannerRequest;
use App\Http\Requests\backend\UpdateBannerRequest;

class HomeBannerController extends Controller
{
    protected $HomeBanner;

	public function __construct(HomeBannerRepository $HomeBanner)
    {
        $this->HomeBanner = $HomeBanner;
    }

    public function index()
    {
        $homeBanners = $this->HomeBanner->all_query()->get();
        return view('backend.content.homebanner.index', compact('homeBanners'));
    }

    public function create(CreateBannerRequest $request)
    {
        $this->HomeBanner->create($request);
    }

    public function update(UpdateBannerRequest $request)
    {
        $this->HomeBanner->update($request);
        return back()->with('message', '更新完成');
    }

    public function delete(Request $request)
    {
        $this->HomeBanner->delete($request);
    }
}
