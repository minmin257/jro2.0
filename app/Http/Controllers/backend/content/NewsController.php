<?php

namespace App\Http\Controllers\backend\content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\NewsRepository;
use App\Http\Requests\backend\UpdateNewsRequest;

class NewsController extends Controller
{
    protected $News;

    public function __construct(NewsRepository $News)
    {
        $this->News = $News;
    }

    public function index()
    {
        $news = $this->News->get_first();
        return view('backend.content.news.index',compact('news'));
    }

    public function update(UpdateNewsRequest $request)
    {
        $this->News->update($request);
        return back()->with('message', '更新完成');
    }

}
