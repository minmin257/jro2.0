<?php

namespace App\Http\Controllers\backend\content;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ProductCategoryRepository;
use App\Repositories\ProductRepository;
use App\Repositories\ProductPhotoRepository;
use App\Repositories\CurlTypeRepository;
use App\Repositories\ThicknessTypeRepository;
use App\Repositories\LengthTypeRepository;
use App\Repositories\ProductOptionRepository;
use App\Repositories\CurlRepository;
use App\Repositories\ThicknessRepository;
use App\Repositories\LengthRepository;
use App\Repositories\PhoneOrderRepository;
use App\Http\Requests\backend\CreateProductRequest;
use App\Http\Requests\backend\CreatePhotoRequest;
use App\Http\Requests\backend\CreateOptionRequest;
use App\Http\Requests\backend\UpdateProductRequest;
use App\Http\Requests\backend\UpdateFilterProductRequest;
use App\Http\Requests\backend\UpdatePhotoRequest;
use App\Http\Requests\backend\UpdateEditProductRequest;
use App\Http\Requests\backend\UpdatePhoneOrderRequest;

class ProductController extends Controller
{
    protected $ProductCategory, $Product, $ProductPhoto, $CurlType, $ThicknessType, $LengthType, $ProductOption, $Curl, $Thickness, $Length, $PhoneOrder;

	public function __construct(ProductCategoryRepository $ProductCategory, ProductRepository $Product, ProductPhotoRepository $ProductPhoto, CurlTypeRepository $CurlType, ThicknessTypeRepository $ThicknessType, LengthTypeRepository $LengthType, ProductOptionRepository $ProductOption, CurlRepository $Curl, ThicknessRepository $Thickness, LengthRepository $Length, PhoneOrderRepository $PhoneOrder)
    {
        $this->ProductCategory = $ProductCategory;
        $this->Product = $Product;
        $this->ProductPhoto = $ProductPhoto;
        $this->CurlType = $CurlType;
        $this->ThicknessType = $ThicknessType;
        $this->LengthType = $LengthType;
        $this->ProductOption = $ProductOption;
        $this->Curl = $Curl;
        $this->Thickness = $Thickness;
        $this->Length = $Length;
        $this->PhoneOrder = $PhoneOrder;
    }

    public function index($category)
    {
        $productCategories = $this->ProductCategory->all_query()->get();
        $productCategory = $productCategories->find($category);
        $products = $this->Product->all_query()->where('category_id', $category)->get();
        return view('backend.content.product.index', compact('productCategories', 'productCategory', 'products'));
    }

    public function create(CreateProductRequest $request)
    {
        $this->Product->create($request);
    }

    public function update(UpdateProductRequest $request)
    {
        $this->Product->update($request);
        return back()->with('message', '更新完成');
    }

    public function delete(Request $request)
    {
        $this->Product->delete($request);
    }

    public function edit($category, $id)
    {
        $productCategories = $this->ProductCategory->all_query()->get();
        $productCategory = $productCategories->find($category);
        $product = $this->Product->all_query()->findOrFail($id);
        return view('backend.content.product.edit', compact('productCategories', 'productCategory', 'product'));
    }

    public function update_filter(UpdateFilterProductRequest $request)
    {
        $this->Product->update_detail($request);
        return back()->with('message', '更新完成');
    }

    public function photo($category, $id)
    {
        $productCategory = $this->ProductCategory->all_query()->find($category);
        $product = $this->Product->all_query()->findOrFail($id);
        return view('backend.content.product.photo', compact('productCategory', 'product'));
    }

    public function create_photo(CreatePhotoRequest $request)
    {
        $this->ProductPhoto->create($request);
    }

    public function update_photo(UpdatePhotoRequest $request)
    {
        $this->ProductPhoto->update($request);
    }

    public function delete_photo(Request $request)
    {
        $this->ProductPhoto->delete($request);
    }

    public function eyelash($category, $id)
    {
        $productCategory = $this->ProductCategory->all_query()->find($category);
        $product = $this->Product->all_query()->findOrFail($id);
        $curlTypes = $this->CurlType->all_query()->get();
        $thicknessTypes = $this->ThicknessType->all_query()->get();
        $lengthTypes = $this->LengthType->all_query()->get();
        return view('backend.content.product.eyelash', compact('productCategory', 'product', 'curlTypes', 'thicknessTypes', 'lengthTypes'));
    }

    public function create_option(CreateOptionRequest $request)
    {
        $this->ProductOption->create($request);
        $this->Curl->create($request);
        $this->Thickness->create($request);
        $this->Length->create($request);
    }

    public function delete_option(Request $request)
    {
        $this->ProductOption->delete($request);
    }

    public function update_option(Request $request)
    {
        $this->ProductOption->update($request);
        return back()->with('message', '更新完成');
    }

    public function setting(Request $request)
    {
        if ($request->tag == 'curl') {
            $setting = $this->Curl->get_all()->where('product_id', $request->id);
        } elseif ($request->tag == 'thickness') {
            $setting = $this->Thickness->get_all()->where('product_id', $request->id);
        } else {
            $setting = $this->Length->get_all()->where('product_id', $request->id);
        }

        return view('backend.content.product.setting', compact('setting'));
    }

    public function update_setting(Request $request)
    {
        if ($request->tag == 'curl') {
            $this->Curl->update($request);
        } elseif ($request->tag == 'thickness') {
            $this->Thickness->update($request);
        } else {
            $this->Length->update($request);
        }
    }

    public function productList()
    {
        $productCategories = $this->ProductCategory->all_query()->get();
        $products = $this->Product->all_query()->orderBy('category_id', 'asc')->get();
        return view('backend.content.product.list', compact('productCategories', 'products'));
    }

    public function listPhoto($id)
    {
        $product = $this->Product->all_query()->findOrFail($id);
        return view('backend.content.product.photo', compact('product'));
    }

    public function listEdit($id)
    {
        $productCategories = $this->ProductCategory->all_query()->get();
        $product = $this->Product->all_query()->findOrFail($id);
        return view('backend.content.product.edit', compact('productCategories', 'product'));
    }

    public function phone()
    {
        $phoneOrder = $this->PhoneOrder->get_first();
        return view('backend.content.product.phone', compact('phoneOrder'));
    }

    public function update_phoneOrder(UpdatePhoneOrderRequest $request)
    {
        $this->PhoneOrder->update($request);
        return back()->with('message', '更新完成');
    }

}
