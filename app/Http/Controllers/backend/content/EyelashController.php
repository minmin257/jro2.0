<?php

namespace App\Http\Controllers\backend\content;

use App\Http\Controllers\Controller;
use App\Repositories\CurlTypeRepository;
use App\Repositories\ThicknessTypeRepository;
use App\Repositories\LengthTypeRepository;
use App\Repositories\ProductCategoryRepository;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;
use App\Http\Requests\backend\CreateTypeRequest;

class EyelashController extends Controller
{
    protected $CurlType, $ThicknessType, $LengthType, $ProductCategory, $Product;

    public function __construct(CurlTypeRepository $CurlType, ThicknessTypeRepository $ThicknessType, LengthTypeRepository $LengthType, ProductCategoryRepository $ProductCategory, ProductRepository $Product)
    {
        $this->CurlType = $CurlType;
        $this->ThicknessType = $ThicknessType;
        $this->LengthType = $LengthType;
        $this->ProductCategory = $ProductCategory;
        $this->Product = $Product;
    }

    public function index()
    {
        $curlTypes = $this->CurlType->all_query()->get();
        $thicknessTypes = $this->ThicknessType->all_query()->get();
        $lengthTypes = $this->LengthType->all_query()->get();
        $result = collect();
        $result = $result->merge($curlTypes)->merge($thicknessTypes)->merge($lengthTypes);
        return view('backend.content.eyelash.index', compact('result'));
    }

    public function create(CreateTypeRequest $request)
    {
        if ($request->category == 'curl') {
            $this->CurlType->create($request);
        } elseif ($request->category == 'thickness') {
            $this->ThicknessType->create($request);
        } else {
            $this->LengthType->create($request);
        }
    }

    public function delete(Request $request)
    {
        if ($request->id['tag'] == 'curl') {
            $this->CurlType->delete($request);
        } elseif ($request->id['tag'] == 'thickness') {
            $this->ThicknessType->delete($request);
        } else {
            $this->LengthType->delete($request);
        }
    }

    public function curl()
    {
        $result = $this->CurlType->all_query()->get();
        return view('backend.content.eyelash.index', compact('result'));
    }

    public function thickness()
    {
        $result = $this->ThicknessType->all_query()->get();
        return view('backend.content.eyelash.index', compact('result'));
    }

    public function length()
    {
        $result = $this->LengthType->all_query()->get();
        return view('backend.content.eyelash.index', compact('result'));
    }

    public function eyelashList()
    {
        $productCategory = $this->ProductCategory->all_query()->find(1);
        $products = $this->Product->all_query()->where('category_id', 1)->get();
        return view('backend.content.eyelash.list', compact('productCategory', 'products'));
    }

    public function detail($id)
    {
        $product = $this->Product->all_query()->findOrFail($id);
        $curlTypes = $this->CurlType->all_query()->get();
        $thicknessTypes = $this->ThicknessType->all_query()->get();
        $lengthTypes = $this->LengthType->all_query()->get();
        return view('backend.content.eyelash.detail', compact('product', 'curlTypes', 'thicknessTypes', 'lengthTypes'));
    }

}
