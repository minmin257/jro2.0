<?php

namespace App\Http\Controllers\backend\content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\FooterRepository;
use App\Http\Requests\backend\UpdateFooterRequest;

class FooterController extends Controller
{
    protected $Footer;

    public function __construct(FooterRepository $Footer)
    {
        $this->Footer = $Footer;
    }

    public function index()
    {
        $footer = $this->Footer->get_first();
        return view('backend.content.footer.index', compact('footer'));
    }

    public function update(UpdateFooterRequest $request)
    {
        $this->Footer->update($request);
        return back()->with('message', '更新完成');
    }

}
