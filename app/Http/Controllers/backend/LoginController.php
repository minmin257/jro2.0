<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\backend\LoginRequest;

class LoginController extends Controller
{
    public function __construct()
	{
		$this->middleware('guest:web', ['except'=>'logout']);
	}
	
    public function index()
    {
    	return view('backend.session.login');
    }

    public function session_in(LoginRequest $request)
    {
    	$credentials = [
    		'account'=>$request->account,
    		'password'=>$request->password,
    	];

    	if(!Auth()->attempt($credentials))
    	{
    		return back()->withErrors([
    			'message'=>'帳號或密碼有誤，請重新確認輸入'
    		]);
    	}
    	else
    	{
    		return redirect()->route('backend.index');
    	}
    }

    public function logout()
    {
        Auth()->logout();
        return redirect()->route('backend');
    }
}
