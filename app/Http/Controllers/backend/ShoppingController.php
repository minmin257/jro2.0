<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Repositories\SystemRepository;
use App\Repositories\PaymentMethodRepository;
use App\Repositories\NoteRepository;
use App\Repositories\ShippingRepository;
use App\Repositories\ReceiptRepository;
use App\Repositories\CompleteRepository;
use App\Http\Requests\backend\UpdateFreeRequest;
use App\Http\Requests\backend\UpdateMethodRequest;
use App\Http\Requests\backend\UpdateReceiptRequest;
use App\Http\Requests\backend\UpdateNoteRequest;
use App\Http\Requests\backend\UpdateCompleteRequest;
use Illuminate\Http\Request;

class ShoppingController extends Controller
{
    protected $System, $PaymentMethod, $Note, $Shipping, $Receipt, $Complete;

    public function __construct(SystemRepository $System, PaymentMethodRepository $PaymentMethod, NoteRepository $Note, ShippingRepository $Shipping, ReceiptRepository $Receipt, CompleteRepository $Complete)
    {
        $this->System = $System;
        $this->PaymentMethod = $PaymentMethod;
        $this->Note = $Note;
        $this->Shipping = $Shipping;
        $this->Receipt = $Receipt;
        $this->Complete = $Complete;
    }

    public function index()
    {
        $system = $this->System->get_first();
        $method = $this->PaymentMethod->get_all()->whereNotIn('id', [1])->first();
        return view('backend.shopping.index', compact('system', 'method'));
    }

    public function update(UpdateFreeRequest $request)
    {
        $this->System->update_free($request);
        $this->PaymentMethod->update($request);
        return back()->with('message', '更新完成');
    }

    public function shipping()
    {
        $shippings = $this->Shipping->get_all()->whereNotIn('id',[3]);
        return view('backend.shopping.shipping', compact('shippings'));
    }

    public function update_shipping(UpdateMethodRequest $request)
    {
        $this->Shipping->update($request);
        return back()->with('message', '更新完成');
    }

    public function receipt()
    {
        $system = $this->System->get_first();
        $receipt = $this->Receipt->get_first();
        return view('backend.shopping.receipt', compact('system', 'receipt'));
    }

    public function update_receipt(UpdateReceiptRequest $request)
    {
        $this->System->update_receipt($request);
        $this->Receipt->update($request);
        return back()->with('message', '更新完成');
    }

    public function expired()
    {
        $system = $this->System->get_first();
        return view('backend.shopping.expired', compact('system'));
    }

    public function update_expired(UpdateReceiptRequest $request)
    {
        $this->System->update_expired($request);
        return back()->with('message', '更新完成');
    }

    public function note()
    {
        $note = $this->Note->get_first();
        return view('backend.shopping.note', compact('note'));
    }

    public function update_note(UpdateNoteRequest $request)
    {
        $this->Note->update($request);
        return back()->with('message', '更新完成');
    }

    public function complete()
    {
        $complete = $this->Complete->get_first();
        return view('backend.shopping.complete', compact('complete'));
    }

    public function update_complete(UpdateNoteRequest $request)
    {
        $this->Complete->update($request);
        return back()->with('message', '更新完成');
    }

}
