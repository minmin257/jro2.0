<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\Repositories\PermissionRepository;
use App\Http\Requests\backend\ProfileRequest;
use App\Http\Requests\backend\CreateUserRequest;
use App\Http\Requests\backend\PermissionRequest;

class AuthorityController extends Controller
{
    protected $User, $Permission;

	public function __construct(UserRepository $User, PermissionRepository $Permission)
    {
        $this->middleware('HasPermission:權限管理', ['only' => ['management']]);
        $this->User = $User;
        $this->Permission = $Permission;
    }

    public function index()
    {
        return view('backend.authority.index');
    }

    public function update(ProfileRequest $request)
    {
    	$this->User->update($request);
    	return back()->with('message', '更新完成');
    }

    public function management()
    {
        $users = $this->User->all_query()->whereNotIn('id',[1])->get();
        return view('backend.authority.management',compact('users'));
    }

    public function create(CreateUserRequest $request)
    {
        $this->User->create($request);
    }

    public function edit($id)
    {
        $user = $this->User->all_query()->findOrFail($id);
        if ($id == 1) {
            abort(404);
        }
        return view('backend.authority.edit',compact('user'));
    }

    public function delete(Request $request)
    {
        $this->User->delete($request);
    }

    public function permission($id)
    {
        $user = $this->User->all_query()->findOrFail($id);
        if ($id == 1) {
            abort(404);
        }
        $permissions = $this->Permission->get_all();
        return view('backend.authority.permission',compact('user','permissions'));
    }

    public function create_permission(PermissionRequest $request)
    {
        $this->User->delete_permission($request);
        $this->User->create_permission($request);
        return back()->with('message', '更新完成');
    }
}
