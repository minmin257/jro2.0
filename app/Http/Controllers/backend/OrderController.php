<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Repositories\OrderRepository;
use App\Repositories\OrderTypeRepository;
use App\Repositories\FooterRepository;
use Illuminate\Http\Request;
use App\Http\Requests\backend\UpdateOrderRequest;
use App\Http\Requests\backend\UpdateRemittanceRequest;

class OrderController extends Controller
{
    protected $Order, $OrderType, $Footer;

    public function __construct(OrderRepository $Order, OrderTypeRepository $OrderType, FooterRepository $Footer)
    {
        $this->middleware('HasPermission:訂單管理');
        $this->Order = $Order;
        $this->OrderType = $OrderType;
        $this->Footer = $Footer;
    }

    public function index()
    {
        $orders = $this->Order->get_all();
        return view('backend.order.index', compact('orders'));
    }

    public function arrearage()
    {
        $orders = $this->Order->get_all()->where('type_id', 1);
        return view('backend.order.index', compact('orders'));
    }

    public function shipment()
    {
        $orders = $this->Order->get_all()->where('type_id', 4);
        return view('backend.order.index', compact('orders'));
    }

    public function completed()
    {
        $orders = $this->Order->get_all()->where('type_id', 5);
        return view('backend.order.index', compact('orders'));
    }

    public function overdue()
    {
        $orders = $this->Order->get_all()->where('type_id', 2);
        return view('backend.order.index', compact('orders'));
    }

    public function cancel()
    {
        $orders = $this->Order->get_all()->where('type_id', 3);
        return view('backend.order.index', compact('orders'));
    }

    public function edit($id)
    {
        $order = $this->Order->get_all()->find($id);
        $types = $this->OrderType->get_all();
        return view('backend.order.detail', compact('order', 'types'));
    }

    public function update(UpdateOrderRequest $request)
    {
        $this->Order->update($request);
        return back()->with('message', '更新完成');
    }

    public function remittance($id)
    {
        $order = $this->Order->get_all()->find($id);
        if ($order->payment_id != 1) {
            abort(404);
        }
        return view('backend.order.remittance', compact('order'));
    }

    public function update_remittance(UpdateRemittanceRequest $request)
    {
        $this->Order->update_remittance($request);
        return back()->with('message', '更新完成');
    }

    public function export_shipment($id)
    {
        $footer = $this->Footer->get_first();
        $order = $this->Order->get_all()->find($id);
        $view = View('backend.export.shipment',compact('order', 'footer'));
        $pdf = \App::make('dompdf.wrapper');
        $pdf->setPaper('A4', 'landscape');
        $pdf->loadHTML($view);
        return $pdf->stream();
    }

}
