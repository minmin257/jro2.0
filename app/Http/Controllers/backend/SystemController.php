<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\SystemRepository;
use App\Repositories\MailAccountRepository;
use App\Http\Requests\backend\SystemRequest;
use App\Http\Requests\backend\MailAccountRequest;
use App\Services\MailService;

class SystemController extends Controller
{
    protected $System, $MailAccount;

    protected $MailService;

	public function __construct(SystemRepository $System, MailAccountRepository $MailAccount, MailService $MailService)
    {
        $this->middleware('HasPermission:網站設定管理', ['only' => ['index']]);
        $this->System = $System;
        $this->MailAccount = $MailAccount;
        $this->MailService = $MailService;
    }

    public function index()
    {
    	$system = $this->System->get_first();
    	return view('backend.system.index',compact('system'));
    }

    public function update(SystemRequest $request)
    {
    	$this->System->update($request);
    	return back()->with('message', '更新完成');
    }

    public function mail_setting()
    {
        $mailAccount = $this->MailAccount->get_first();
        return view('backend.system.mail_setting',compact('mailAccount'));
    }

    public function update_mail_account(MailAccountRequest $request)
    {
        if($request->ajax())
        {
            return $this->MailService->CheckMailAccount($request->all());
        }
        else
        {
            $this->MailAccount->update_mail_name($request);
            return back()->with('message', '更新完成');
        }
    }

}
