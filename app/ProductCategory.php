<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $fillable = [
        'title', 'src', 'sort', 'state', 'delete'
    ];

    public function product()
    {
        return $this->hasMany(Product::class, 'category_id', 'id')->orderBy('sort', 'desc');
    }

    public function phone()
    {
        return $this->hasOne(PhoneOrder::class, 'category_id', 'id');
    }

}
