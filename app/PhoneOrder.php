<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhoneOrder extends Model
{
    protected $fillable = [
        'category_id', 'html'
    ];
}
