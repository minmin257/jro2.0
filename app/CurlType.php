<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CurlType extends Model
{
    protected $fillable = [
        'title', 'delete'
    ];
}
