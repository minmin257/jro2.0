<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curl extends Model
{
    protected $fillable = [
        'product_id', 'curl_id', 'state', 'out', 'delete'
    ];

    public function type()
    {
        return $this->belongsTo(CurlType::class, 'curl_id', 'id');
    }

}
