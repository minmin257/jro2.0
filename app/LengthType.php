<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LengthType extends Model
{
    protected $fillable = [
        'title', 'delete'
    ];
}
