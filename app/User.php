<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account', 'password', 'name', 'state', 'delete'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function user_permissions()
    {
        return $this->hasMany(UserHasPermission::class);
    }

    public function hasPermission(string $permission)
    {
        $permissions_id = $this->user_permissions()->pluck('permission_id');

        if(Permission::whereIn('id',$permissions_id)->where('title',$permission)->get()->count()>0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
