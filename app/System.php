<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class System extends Model
{
    protected $fillable = [
        'sitename', 'meta_description', 'meta_keyword', 'maintain', 'message', 'expired', 'receipt', 'free', 'percent'
    ];
}
