<?php

    namespace App\Services;

    use Illuminate\Support\Facades\Mail;
    use App\Repositories\MailAccountRepository;
    use App\Mail\CheckMailAccount;
    use App\Mail\OrderCompleted;

    class MailService
    {
        protected $MailAccount;

        public function __construct(MailAccountRepository $MailAccount)
        {
            $this->MailAccount = $MailAccount;
        }

        public function CheckMailAccount(array $request)
        {
            try {
                \Config::set('mail.mailers.smtp.username', $request['email']);
                \Config::set('mail.mailers.smtp.password', $request['password']);
                Mail::to($request['email'])->queue(new CheckMailAccount());
                // 發信成功才會更新
                $this->MailAccount->update($request);
            } catch (\Exception $e) {
                return response()->json([
                    'success' => false,
                    'errors' => array('mail_failure'=>'郵件發送失敗，請檢查輸入信箱或密碼是否正確'),
                ], 402);
            }

        }

        /**
         * 發送Email
         * @param array $request
         */
        public function OrderCompleted($order)
        {
            $id = $order->id;
            $orderer = session()->get('orderer');

            try {
                $mailAccount = $this->MailAccount->get_first();
                \Config::set('mail.mailers.smtp.username', $mailAccount->email);
                \Config::set('mail.mailers.smtp.password', decrypt($mailAccount->password));
                Mail::to($orderer['email'])->queue(new OrderCompleted($order));
            } catch (\Exception $e) {
                session()->forget('cart');
                session()->forget('checkout');
                session()->forget('orderer');
                return response()->json([
                    'success' => false,
                    'errors' => array('mail_failure'=>'訂單已成立，但郵件發送失敗'),
                    'id' => $id,
                ], 402);
            }
        }

    }

?>
