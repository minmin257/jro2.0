<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thickness extends Model
{
    protected $fillable = [
        'product_id', 'thickness_id', 'state', 'out', 'delete'
    ];

    public function type()
    {
        return $this->belongsTo(ThicknessType::class, 'thickness_id', 'id');
    }

}
