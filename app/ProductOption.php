<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductOption extends Model
{
    protected $fillable = [
        'product_id', 'curl_id', 'thickness_id', 'length_id', 'state', 'out', 'delete'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function curl()
    {
        return $this->belongsTo(CurlType::class);
    }

    public function thickness()
    {
        return $this->belongsTo(ThicknessType::class);
    }

    public function length()
    {
        return $this->belongsTo(LengthType::class);
    }

}
