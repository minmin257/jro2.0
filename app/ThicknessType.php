<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ThicknessType extends Model
{
    protected $fillable = [
        'title', 'delete'
    ];
}
