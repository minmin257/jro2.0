<?php

    namespace App\Mail;

    use Illuminate\Bus\Queueable;
    use Illuminate\Mail\Mailable;
    use Illuminate\Queue\SerializesModels;
    use Illuminate\Contracts\Queue\ShouldQueue;
    use App\MailAccount;

    class OrderCompleted extends Mailable
    {
        use Queueable, SerializesModels;

        /**
         * Create a new message instance.
         *
         * @return void
         */
        public $order;

        public function __construct($order)
        {
            $this->order = $order;
        }

        /**
         * Build the message.
         *
         * @return $this
         */
        public function build()
        {
            $mailAccount = MailAccount::first();
            $email = $mailAccount->email;
            $name = $mailAccount->name;
            $subject = $mailAccount->subject;
            $recipient = $mailAccount->recipient;
            $recipient_mails = explode(',', str_replace(' ','',$recipient));

            if($recipient)
            {
                return $this->from($email, $name)
                    ->subject($subject)
                    ->markdown('mails.completed')
                    ->cc($recipient_mails);
            }
            else
            {
                return $this->from($email, $name)
                    ->subject($subject)
                    ->markdown('mails.completed');
            }
        }
    }
