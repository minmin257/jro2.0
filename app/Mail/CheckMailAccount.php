<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\MailAccount;

class CheckMailAccount extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mailAccount = MailAccount::first();
        $email = $mailAccount->email;
        return $this->from($email, '設定通知')
                ->subject('信箱設定成功通知')
                ->markdown('mails.check_mail_account');
    }
}
