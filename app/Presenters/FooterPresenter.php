<?php

    namespace App\Presenters;

    use App\Repositories\FooterRepository;
    use App\Repositories\SystemRepository;

    class FooterPresenter
    {
        protected $Footer, $System;

        public function __construct(FooterRepository $Footer, SystemRepository $System)
        {
            $this->Footer = $Footer;
            $this->System = $System;
        }

        public function getFooter()
        {
            return $this->Footer->get_first();
        }

        public function getSystem()
        {
            return $this->System->get_first();
        }

    }
