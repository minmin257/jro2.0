<?php

    namespace App\Presenters;

    use App\Repositories\SubjectRepository;
    use App\Repositories\ProductCategoryRepository;
    use App\Repositories\NewsRepository;

    class HeaderPresenter
    {
        protected $Subject, $ProductCategory, $News;

        public function __construct(SubjectRepository $Subject, ProductCategoryRepository $ProductCategory, NewsRepository $News)
        {
            $this->Subject = $Subject;
            $this->ProductCategory = $ProductCategory;
            $this->News = $News;
        }

        public function getSubjects()
        {
            return $this->Subject->get_all();
        }

        public function getCategories()
        {
            return $this->ProductCategory->all_query()->where('state',1)->get();
        }

        public function getNews()
        {
            return $this->News->get_first();
        }

    }
