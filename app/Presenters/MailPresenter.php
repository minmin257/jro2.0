<?php

    namespace App\Presenters;

    use App\Repositories\MailAccountRepository;
    use App\Repositories\SystemRepository;

    class MailPresenter
    {
        protected $MailAccount, $System;

        public function __construct(MailAccountRepository $MailAccount, SystemRepository $System)
        {
            $this->MailAccount = $MailAccount;
            $this->System = $System;
        }

        public function getMailAccount()
        {
            return $this->MailAccount->get_first();
        }

        public function getSystem()
        {
            return $this->System->get_first();
        }
    }
