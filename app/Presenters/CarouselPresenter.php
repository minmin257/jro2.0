<?php

    namespace App\Presenters;

    use App\Repositories\HomeBannerRepository;

    class CarouselPresenter
    {
        protected $HomeBanner;

        public function __construct(HomeBannerRepository $HomeBanner)
        {
            $this->HomeBanner = $HomeBanner;
        }

        public function getHomeBanners()
        {
            return $this->HomeBanner->all_query()->where('state',1)->get();
        }
    }
