<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Remittance extends Model
{
    protected $fillable = [
        'order_id', 'date', 'amount', 'account'
    ];
}
