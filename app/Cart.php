<?php

    namespace App;

    class Cart
    {
        public $items = null;

        public function __construct($oldCart)
        {
            if($oldCart)
            {
                $this->items = $oldCart->items;
            }
        }

        public function add($item, $id, $quantity)
        {
            $storedItem = ['item' => $item, 'quantity' => (int)$quantity];
            if ($this->items) {
                if (array_key_exists($id, $this->items)) {
                    $this->items[$id]['quantity'] += $quantity;
                    $storedItem = $this->items[$id];
                }
            }

            $this->items[$id] = $storedItem;
        }

        public function remove($id)
        {
            // remove the item
            unset($this->items[$id]);
        }

        public function update($id, $quantity)
        {
            $this->items[$id]['quantity'] = $quantity;
        }

    }
