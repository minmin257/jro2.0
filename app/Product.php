<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'category_id', 'title', 'spec', 'html', 'price', 'sort', 'state', 'out', 'delete'
    ];

    public function photo()
    {
        return $this->hasMany(ProductPhoto::class)->orderBy('sort','desc');
    }

    public function category()
    {
        return $this->belongsTo(ProductCategory::class);
    }

    public function option()
    {
        return $this->hasMany(ProductOption::class)->orderBy('curl_id', 'asc')->orderBy('thickness_id', 'asc')->orderBy('length_id', 'asc');
    }

}
