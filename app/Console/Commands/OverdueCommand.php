<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Order;
use App\System;

class OverdueCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'overdue:payment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '每天執行查看是否為逾期付款訂單';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // 逾期天數
        $expired = System::first()->expired;
        $today = strtotime('today');
        $orders = Order::where('type_id', 1)->get();
        foreach ($orders as $order)
        {
            // 加上逾期天數
            $str = strtotime('+'.$expired.' day', strtotime($order->created_at));
            if ($str < $today) {
                $order->update([
                    'type_id'=>2
                ]);
            }
        }

        dd('done.');
    }
}
