<?php

    namespace App\Repositories;

    use Illuminate\Http\Request;
    use App\Product;
    use App\ProductOption;
    use App\ProductPhoto;

    class ProductRepository
    {
        protected $Product, $ProductOption, $ProductPhoto;

        public function __construct(Product $Product, ProductOption $ProductOption, ProductPhoto $ProductPhoto)
        {
            $this->model = $Product;
            $this->ProductOption = $ProductOption;
            $this->ProductPhoto = $ProductPhoto;
        }

        public function all_query()
        {
            return $this->model->where('delete',0)->orderBy('sort','desc');
        }

        public function create(Request $request)
        {
            $product = $this->model->create([
                'category_id'=>$request['category'],
                'title'=>$request['title'],
                'spec'=>$request['spec'],
                'html'=>$request['html'],
                'price'=>$request['price'],
                'sort'=>$request['sort']
            ]);

            if ($request['category'] != 1) {
                $this->ProductOption->create([
                    'product_id'=>$product->id,
                    'curl_id'=>null,
                    'thickness_id'=>null,
                    'length_id'=>null
                ]);
            }

            $this->ProductPhoto->create([
                'product_id'=>$product->id,
                'src'=>$request['filepath']
            ]);
        }

        public function update(Request $request)
        {
            foreach($request['id'] as $key => $id)
            {
                $this->model->findOrFail($id)->update([
                    'title'=>$request['title'][$key],
                    'sort'=>$request['sort'][$key],
                    'state'=>$request['state'][$key],
                    'out'=>$request['out'][$key]
                ]);
            }
        }

        public function delete(Request $request)
        {
            $this->model->findOrFail($request['id'])->update([
                'delete'=>1
            ]);
        }

        public function update_detail(Request $request)
        {
            $this->model->findOrFail($request->id)->update([
                'category_id'=>$request['category'],
                'title'=>$request['title'],
                'spec'=>$request['spec'],
                'html'=>$request['html'],
                'price'=>$request['price'],
                'sort'=>$request['sort'],
            ]);
        }

    }
?>
