<?php

    namespace App\Repositories;
    use Illuminate\Http\Request;
    use App\Subject;

    class SubjectRepository
    {
        protected $Subject;

        public function __construct(Subject $Subject)
        {
            $this->model = $Subject;
        }

        public function get_all()
        {
            return $this->model->get();
        }
    }
?>
