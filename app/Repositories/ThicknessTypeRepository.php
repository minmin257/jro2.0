<?php

    namespace App\Repositories;

    use Illuminate\Http\Request;
    use App\ThicknessType;
    use App\ProductOption;
    use App\Thickness;

    class ThicknessTypeRepository
    {
        protected $ThicknessType, $ProductOption, $Thickness;

        public function __construct(ThicknessType $ThicknessType, ProductOption $ProductOption, Thickness $Thickness)
        {
            $this->model = $ThicknessType;
            $this->ProductOption = $ProductOption;
            $this->Thickness = $Thickness;
        }

        public function all_query()
        {
            return $this->model->where('delete', 0)->orderBy('title', 'asc');
        }

        public function create(Request $request)
        {
            $this->model->create([
                'title'=>$request['title']
            ]);
        }

        public function delete(Request $request)
        {
            $this->model->findOrFail($request->id['id'])->update([
                'delete'=>1,
            ]);

            foreach ($this->ProductOption->where('thickness_id', $request->id['id'])->get() as $key => $item) {
                $item->update([
                    'delete'=>1
                ]);
            }

            foreach ($this->Thickness->where('thickness_id', $request->id['id'])->get() as $key => $item) {
                $item->update([
                    'delete'=>1
                ]);
            }
        }

    }
?>
