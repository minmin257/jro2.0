<?php

    namespace App\Repositories;

    use Illuminate\Http\Request;
    use App\Note;

    class NoteRepository
    {
        protected $Note;

        public function __construct(Note $Note)
        {
            $this->model = $Note;
        }

        public function get_first()
        {
            return $this->model->first();
        }

        public function update(Request $request)
        {
            $this->model->first()->update([
                'html'=>$request['html']
            ]);
        }

    }
?>
