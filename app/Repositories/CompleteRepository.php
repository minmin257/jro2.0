<?php

    namespace App\Repositories;

    use Illuminate\Http\Request;
    use App\Complete;

    class CompleteRepository
    {
        protected $Complete;

        public function __construct(Complete $Complete)
        {
            $this->model = $Complete;
        }

        public function get_first()
        {
            return $this->model->first();
        }

        public function update(Request $request)
        {
            $this->model->first()->update([
                'html'=>$request['html']
            ]);
        }

    }
?>
