<?php

    namespace App\Repositories;
    use Illuminate\Http\Request;
    use App\Order;
    use App\OrderDetail;
    use App\Remittance;
    use App\System;

    class OrderRepository
    {
        protected $Order, $OrderDetail, $Remittance, $System;

        public function __construct(Order $Order, OrderDetail $OrderDetail, Remittance $Remittance, System $System)
        {
            $this->model = $Order;
            $this->OrderDetail = $OrderDetail;
            $this->Remittance = $Remittance;
            $this->System = $System;
        }

        public function get_all()
        {
            return $this->model->orderBy('created_at', 'desc')->get();
        }

        public function create()
        {
            $checkout = session()->get('checkout');
            $orderer = session()->get('orderer');
            $system = $this->System->first();

            $order = Order::create([
                'type_id'=>($checkout['payment'] == 1) ? 1 : 4,
                'number'=>strtotime('now'),
                'buyer'=>$orderer['buyer'],
                'buyer_tel'=>$orderer['buyer_tel'],
                'buyer_addr'=>$orderer['buyer_addr'],
                'email'=>$orderer['email'],
                'consignee'=>$orderer['consignee'],
                'consignee_tel'=>$orderer['consignee_tel'],
                'consignee_addr'=>$orderer['consignee_addr'],
                'receipt_id'=>isset($orderer['receipt']) ? $orderer['receipt'] : 1,
                'title'=>isset($orderer['title']) ? $orderer['title'] : null,
                'VAT'=>isset($orderer['VAT']) ? $orderer['VAT'] : null,
                'contribute'=>isset($orderer['contribute']) ? $orderer['contribute'] : null,
                'price'=>$checkout['subtotal'],
                'discount'=>($checkout['subtotal'] >= $system->free) ? $checkout['subtotal'] * $system->percent / 100 : null,
                'shipping_id'=>$checkout['shipping'],
                'payment_id'=>$checkout['payment'],
                'total'=>$checkout['total'],
                'demand'=>$orderer['demand']
            ]);

            foreach ($checkout['id'] as $key => $product) {
                $this->OrderDetail::create([
                    'order_id'=>$order->id,
                    'product_id'=>$product,
                    'quantity'=>$checkout['quantity'][$key]
                ]);
            }

            return $order;
        }

        public function update(Request $request)
        {
            $order = $this->model->find($request->id);
            $order->update([
                'type_id'=>$request['type'],
                'receipt_number'=>$request['receipt_number'],
                'title'=>$request['title'],
                'VAT'=>$request['VAT']
            ]);

            if ($request->filled('date') && $request->filled('amount') && $request->filled('account')) {
                if (!$order->remittance) {
                    $this->Remittance->create([
                        'order_id'=>$order->id,
                        'date'=>$request['date'],
                        'amount'=>$request['amount'],
                        'account'=>$request['account']
                    ]);
                }
            }
        }

        public function update_type($id, Request $request)
        {
            $this->model->find($id)->update([
                'type_id'=>4
            ]);

            $this->Remittance->updateOrCreate(['order_id' => $id], [
                'order_id'=>$id,
                'date'=>$request['date'],
                'amount'=>$request['amount'],
                'account'=>$request['account']
            ]);
        }

        public function update_remittance(Request $request)
        {
            $this->Remittance->where('order_id', $request->id)->first()->update([
                'date'=>$request['date'],
                'amount'=>$request['amount'],
                'account'=>$request['account']
            ]);
        }

    }
?>
