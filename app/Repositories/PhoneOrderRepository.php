<?php

    namespace App\Repositories;

    use Illuminate\Http\Request;
    use App\PhoneOrder;

    class PhoneOrderRepository
    {
        protected $PhoneOrder;

        public function __construct(PhoneOrder $PhoneOrder)
        {
            $this->model = $PhoneOrder;
        }

        public function get_first()
        {
            return $this->model->first();
        }

        public function update(Request $request)
        {
            $this->model->first()->update([
                'html'=>$request['html']
            ]);
        }

    }
?>
