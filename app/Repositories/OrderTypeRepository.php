<?php

    namespace App\Repositories;
    use Illuminate\Http\Request;
    use App\OrderType;

    class OrderTypeRepository
    {
        protected $OrderType;

        public function __construct(OrderType $OrderType)
        {
            $this->model = $OrderType;
        }

        public function get_all()
        {
            return $this->model->get();
        }

    }
?>
