<?php

    namespace App\Repositories;
    use Illuminate\Http\Request;
    use App\ReceiptType;

    class ReceiptTypeRepository
    {
        protected $ReceiptType;

        public function __construct(ReceiptType $ReceiptType)
        {
            $this->model = $ReceiptType;
        }

        public function get_all()
        {
            return $this->model->get();
        }
    }
?>
