<?php

    namespace App\Repositories;

    use Illuminate\Http\Request;
    use App\Thickness;

    class ThicknessRepository
    {
        protected $Thickness;

        public function __construct(Thickness $Thickness)
        {
            $this->model = $Thickness;
        }

        public function get_all()
        {
            return $this->model->where('delete', 0)->get();
        }

        public function create(Request $request)
        {
            $thickness = $this->model->where('product_id', $request->id)->where('thickness_id', $request->thickness)->first();
            if (!isset($thickness)) {
                $this->model->create([
                    'product_id'=>$request->id,
                    'thickness_id'=>$request->thickness,
                ]);
            }
        }

        public function update(Request $request)
        {
            foreach($request['id'] as $key => $id)
            {
                $this->model->findOrFail($id)->update([
                    'state'=>$request['state'][$key],
                    'out'=>$request['out'][$key]
                ]);
            }
        }

    }
?>
