<?php

    namespace App\Repositories;

    use Illuminate\Http\Request;
    use App\ProductPhoto;

    class ProductPhotoRepository
    {
        protected $ProductPhoto;

        public function __construct(ProductPhoto $ProductPhoto)
        {
            $this->model = $ProductPhoto;
        }

        public function create(Request $request)
        {
            $explode = explode(',', $request['filepath']);
            foreach ($explode as $src) {
                $this->model->create([
                    'product_id'=>$request['id'],
                    'src'=>$src,
                    'sort'=>$request['sort'],
                ]);
            }
        }

        public function update(Request $request)
        {
            $this->model->findOrFail($request['id'])->update([
                'sort'=>$request['sort'],
            ]);
        }

        public function delete(Request $request)
        {
            $this->model->findOrFail($request['id'])->delete();
        }

    }
?>
