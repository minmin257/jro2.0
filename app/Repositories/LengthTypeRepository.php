<?php

    namespace App\Repositories;

    use Illuminate\Http\Request;
    use App\LengthType;
    use App\ProductOption;
    use App\Length;

    class LengthTypeRepository
    {
        protected $LengthType, $ProductOption, $Length;

        public function __construct(LengthType $LengthType, ProductOption $ProductOption, Length $Length)
        {
            $this->model = $LengthType;
            $this->ProductOption = $ProductOption;
            $this->Length = $Length;
        }

        public function all_query()
        {
            return $this->model->where('delete', 0)->orderByRaw('LENGTH(title)', 'asc')->orderBy('title', 'asc');
        }

        public function create(Request $request)
        {
            $this->model->create([
                'title'=>$request['title']
            ]);
        }

        public function delete(Request $request)
        {
            $this->model->findOrFail($request->id['id'])->update([
                'delete'=>1,
            ]);

            foreach ($this->ProductOption->where('length_id', $request->id['id'])->get() as $key => $item) {
                $item->update([
                    'delete'=>1
                ]);
            }

            foreach ($this->Length->where('length_id', $request->id['id'])->get() as $key => $item) {
                $item->update([
                    'delete'=>1
                ]);
            }
        }

    }
?>
