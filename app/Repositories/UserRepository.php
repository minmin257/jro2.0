<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\User;
use App\UserHasPermission;

class UserRepository
{
    protected $User, $UserHasPermission;

    public function __construct(User $User, UserHasPermission $UserHasPermission)
    {
        $this->model = $User;
        $this->UserHasPermission = $UserHasPermission;
    }

    public function all_query()
    {
        return $this->model->where('delete',0);
    }

    public function create(Request $request)
    {
        $this->model->create([
            'account'=>$request['account'],
            'password'=>bcrypt($request['password']),
            'name'=>$request['name'],
            'state'=>$request['state'],
        ]);
    }

    public function update(Request $request)
    {
		if($request->filled('password'))
        {
            if($request->has('state'))
            {
                $this->model->findOrFail($request['id'])->update([
                    'name'=>$request['name'],
                    'password'=>bcrypt($request['password']),
                    'state'=>$request['state'],
                ]);
            }
            else
            {
                $this->model->findOrFail($request['id'])->update([
                    'name'=>$request['name'],
                    'password'=>bcrypt($request['password']),
                ]);
            }
        }
        else
        {
            if($request->has('state'))
            {
                $this->model->findOrFail($request['id'])->update([
                    'name'=>$request['name'],
                    'state'=>$request['state'],
                ]);
            }
            else
            {
                $this->model->findOrFail($request['id'])->update([
                    'name'=>$request['name'],
                ]);
            }
        }
    }

    public function delete(Request $request)
    {
        $this->model->findOrFail($request['id'])->update([
            'delete'=>1,
        ]);
    }

    public function delete_permission(Request $request)
    {
        $this->UserHasPermission->where('user_id',$request['id'])->delete();
    }

    public function create_permission(Request $request)
    {
        foreach($request['state'] as $key => $value)
        {
            if($value)
            {
                $this->UserHasPermission->create([
                    'user_id'=>$request['id'],
                    'permission_id'=>$value,
                ]);
            }
        }
    }

}
?>
