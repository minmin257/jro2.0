<?php

    namespace App\Repositories;

    use Illuminate\Http\Request;
    use App\CurlType;
    use App\ProductOption;
    use App\Curl;

    class CurlTypeRepository
    {
        protected $CurlType, $ProductOption, $Curl;

        public function __construct(CurlType $CurlType, ProductOption $ProductOption, Curl $Curl)
        {
            $this->model = $CurlType;
            $this->ProductOption = $ProductOption;
            $this->Curl = $Curl;
        }

        public function all_query()
        {
            return $this->model->where('delete', 0)->orderBy('title', 'asc')->orderByRaw('LENGTH(title)', 'asc');
        }

        public function create(Request $request)
        {
            $this->model->create([
                'title'=>$request['title']
            ]);
        }

        public function delete(Request $request)
        {
            $this->model->findOrFail($request->id['id'])->update([
                'delete'=>1,
            ]);

            foreach ($this->ProductOption->where('curl_id', $request->id['id'])->get() as $key => $item) {
                $item->update([
                    'delete'=>1
                ]);
            }

            foreach ($this->Curl->where('curl_id', $request->id['id'])->get() as $key => $item) {
                $item->update([
                    'delete'=>1
                ]);
            }

        }

    }
?>
