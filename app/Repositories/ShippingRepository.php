<?php

    namespace App\Repositories;

    use Illuminate\Http\Request;
    use App\Shipping;

    class ShippingRepository
    {
        protected $Shipping;

        public function __construct(Shipping $Shipping)
        {
            $this->model = $Shipping;
        }

        public function get_all()
        {
            return $this->model->get();
        }

        public function update(Request $request)
        {
            foreach($request['id'] as $key => $id)
            {
                $this->model->findOrFail($id)->update([
                    'title'=>$request['title'][$key],
                    'fee'=>$request['fee'][$key]
                ]);
            }
        }

    }
?>
