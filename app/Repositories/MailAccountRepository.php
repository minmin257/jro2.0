<?php

    namespace App\Repositories;

    use Illuminate\Http\Request;
    use App\MailAccount;

    class MailAccountRepository
    {
        protected $MailAccount;

        public function __construct(MailAccount $MailAccount)
        {
            $this->model = $MailAccount;
        }

        public function get_first()
        {
            return $this->model->first();
        }

        public function update(array $request)
        {
            $this->model->first()->update([
                'email'=>$request['email'],
                'password'=>encrypt($request['password']),
            ]);
        }

        public function update_mail_name(Request $request)
        {
            $this->model->first()->update([
                'name'=>$request['name'],
                'subject'=>$request['subject'],
                'recipient'=>$request['recipient'],
                'html'=>$request['html'],
            ]);
        }

    }
?>
