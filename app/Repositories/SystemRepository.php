<?php

    namespace App\Repositories;

    use Illuminate\Http\Request;
    use App\System;

    class SystemRepository
    {
        protected $System;

        public function __construct(System $System)
        {
            $this->model = $System;
        }

        public function get_first()
        {
            return $this->model->first();
        }

        public function update(Request $request)
        {
            $this->model->first()->update([
                'sitename'=>$request['sitename'],
                'meta_description'=>$request['meta_description'],
                'meta_keyword'=>$request['meta_keyword'],
                'maintain'=>$request['maintain'],
                'message'=>$request['message'],
            ]);
        }

        public function update_free(Request $request)
        {
            $this->model->first()->update([
                'free'=>$request['free']
            ]);
        }

        public function update_receipt(Request $request)
        {
            $this->model->first()->update([
                'receipt'=>$request['receipt']
            ]);
        }

        public function update_expired(Request $request)
        {
            $this->model->first()->update([
                'expired'=>$request['expired']
            ]);
        }

    }
?>
