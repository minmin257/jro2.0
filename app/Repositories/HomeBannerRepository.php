<?php

    namespace App\Repositories;

    use Illuminate\Http\Request;
    use App\HomeBanner;

    class HomeBannerRepository
    {
        protected $HomeBanner;

        public function __construct(HomeBanner $HomeBanner)
        {
            $this->model = $HomeBanner;
        }

        public function all_query()
        {
            return $this->model->where('delete',0)->orderBy('sort','desc');
        }

        public function create(Request $request)
        {
            $this->model->create([
                'src'=>$request['filepath'],
                'mobile_src'=>$request['filepath-mobile'],
                'sort'=>$request['sort'],
            ]);
        }

        public function update(Request $request)
        {
            if($request->filled('id'))
            {
                foreach($request['id'] as $key => $id)
                {
                    $this->model->findOrFail($id)->update([
                        'src'=>$request['filepath'][$key],
                        'mobile_src'=>$request['filepath-mobile'][$key],
                        'sort'=>$request['sort'][$key],
                        'state'=>$request['state'][$key],
                    ]);
                }
            }
        }

        public function delete(Request $request)
        {
            $this->model->findOrFail($request['id'])->update([
                'delete'=>1,
            ]);
        }

    }
?>
