<?php

    namespace App\Repositories;

    use Illuminate\Http\Request;
    use App\PaymentMethod;

    class PaymentMethodRepository
    {
        protected $PaymentMethod;

        public function __construct(PaymentMethod $PaymentMethod)
        {
            $this->model = $PaymentMethod;
        }

        public function get_all()
        {
            return $this->model->get();
        }

        public function update(Request $request)
        {
            $this->model->find(2)->update([
						'title'=>'貨到付款(酌收'.$request['fee'].'元手續費)',
                'fee'=>$request['fee']
            ]);
        }

    }
?>
