<?php

    namespace App\Repositories;

    use Illuminate\Http\Request;
    use App\Length;

    class LengthRepository
    {
        protected $Length;

        public function __construct(Length $Length)
        {
            $this->model = $Length;
        }

        public function get_all()
        {
            return $this->model->where('delete', 0)->get();
        }

        public function create(Request $request)
        {
            $length = $this->model->where('product_id', $request->id)->where('length_id', $request->length)->first();
            if (!isset($length)) {
                $this->model->create([
                    'product_id'=>$request->id,
                    'length_id'=>$request->length,
                ]);
            }
        }

        public function update(Request $request)
        {
            foreach($request['id'] as $key => $id)
            {
                $this->model->findOrFail($id)->update([
                    'state'=>$request['state'][$key],
                    'out'=>$request['out'][$key]
                ]);
            }
        }

    }
?>
