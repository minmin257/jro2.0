<?php

    namespace App\Repositories;

    use Illuminate\Http\Request;
    use App\Teach;

    class TeachRepository
    {
        protected $Teach;

        public function __construct(Teach $Teach)
        {
            $this->model = $Teach;
        }

        public function get_all()
        {
            return $this->model->get();
        }

        public function update(Request $request)
        {
            foreach($request['id'] as $key => $id)
            {
                $this->model->findOrFail($id)->update([
                    'title'=>$request['title'][$key],
                    'src'=>$request['filepath'][$key]
                ]);
            }
        }

    }
?>
