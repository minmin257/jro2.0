<?php

    namespace App\Repositories;

    use Illuminate\Http\Request;
    use App\ProductOption;

    class ProductOptionRepository
    {
        protected $ProductOption;

        public function __construct(ProductOption $ProductOption)
        {
            $this->model = $ProductOption;
        }

        public function all_query()
        {
            return $this->model->where('product_options.delete', 0);
        }

        public function create(Request $request)
        {
            $this->model->create([
                'product_id'=>$request->id,
                'curl_id'=>$request->curl,
                'thickness_id'=>$request->thickness,
                'length_id'=>$request->length,
            ]);
        }

        public function update(Request $request)
        {
            foreach($request['id'] as $key => $id)
            {
                $this->model->findOrFail($id)->update([
                    'state'=>$request['state'][$key],
                    'out'=>$request['out'][$key],
                ]);
            }
        }

        public function delete(Request $request)
        {
            $this->model->findOrFail($request['id'])->update([
                'delete'=>1
            ]);
        }

    }
?>
