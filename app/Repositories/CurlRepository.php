<?php

    namespace App\Repositories;

    use Illuminate\Http\Request;
    use App\Curl;

    class CurlRepository
    {
        protected $Curl;

        public function __construct(Curl $Curl)
        {
            $this->model = $Curl;
        }

        public function get_all()
        {
            return $this->model->where('delete', 0)->get();
        }

        public function create(Request $request)
        {
            $curl = $this->model->where('product_id', $request->id)->where('curl_id', $request->curl)->first();
            if (!isset($curl)) {
                $this->model->create([
                    'product_id'=>$request->id,
                    'curl_id'=>$request->curl,
                ]);
            }
        }

        public function update(Request $request)
        {
            foreach($request['id'] as $key => $id)
            {
                $this->model->findOrFail($id)->update([
                    'state'=>$request['state'][$key],
                    'out'=>$request['out'][$key]
                ]);
            }
        }

    }
?>
