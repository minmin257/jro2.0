<?php

    namespace App\Repositories;

    use Illuminate\Http\Request;
    use App\News;

    class NewsRepository
    {
        protected $News;

        public function __construct(News $News)
        {
            $this->model = $News;
        }

        public function get_first()
        {
            return $this->model->first();
        }

        public function update(Request $request)
        {
            $this->model->first()->update([
                'title'=>$request['title'],
                'src'=>$request['filepath']
            ]);
        }

    }
?>
