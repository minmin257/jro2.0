<?php

    namespace App\Repositories;
    use Illuminate\Http\Request;
    use App\Receipt;

    class ReceiptRepository
    {
        protected $Receipt;

        public function __construct(Receipt $Receipt)
        {
            $this->model = $Receipt;
        }

        public function get_first()
        {
            return $this->model->first();
        }

        public function update(Request $request)
        {
            $this->model->first()->update([
                'title'=>$request['title']
            ]);
        }

    }
?>
