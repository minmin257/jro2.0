<?php

    namespace App\Repositories;

    use Illuminate\Http\Request;
    use App\Footer;

    class FooterRepository
    {
        protected $Footer;

        public function __construct(Footer $Footer)
        {
            $this->model = $Footer;
        }

        public function get_first()
        {
            return $this->model->first();
        }

        public function update(Request $request)
        {
            $this->model->first()->update([
                'title'=>$request['title'],
                'seal'=>$request['seal'],
                'src'=>$request['filepath'],
                'line'=>$request['line'],
                'tel'=>$request['tel']
            ]);
        }

    }
?>
