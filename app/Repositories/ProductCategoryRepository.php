<?php

    namespace App\Repositories;

    use Illuminate\Http\Request;
    use App\ProductCategory;

    class ProductCategoryRepository
    {
        protected $ProductCategory;

        public function __construct(ProductCategory $ProductCategory)
        {
            $this->model = $ProductCategory;
        }

        public function all_query()
        {
            return $this->model->where('delete',0)->orderBy('sort','desc');
        }

        public function create(Request $request)
        {
            $this->model->create([
                'title'=>$request['title'],
                'src'=>$request['filepath'],
                'sort'=>$request['sort']
            ]);
        }

        public function update(Request $request)
        {
            foreach($request['id'] as $key => $id)
            {
                $this->model->findOrFail($id)->update([
                    'title'=>$request['title'][$key],
                    'src'=>$request['filepath'][$key],
                    'sort'=>$request['sort'][$key],
                    'state'=>$request['state'][$key]
                ]);
            }
        }

        public function delete(Request $request)
        {
            $this->model->findOrFail($request['id'])->update([
                'delete'=>1
            ]);
        }

    }
?>
