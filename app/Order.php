<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'type_id', 'number', 'buyer', 'buyer_tel', 'buyer_addr', 'email', 'consignee', 'consignee_tel', 'consignee_addr', 'receipt_id', 'receipt_number', 'title', 'VAT', 'contribute', 'price', 'discount', 'shipping_id', 'payment_id', 'total', 'demand'
    ];

    public function method()
    {
        return $this->belongsTo(PaymentMethod::class, 'payment_id', 'id');
    }

    public function receipt()
    {
        return $this->belongsTo(ReceiptType::class);
    }

    public function type()
    {
        return $this->belongsTo(OrderType::class);
    }

    public function shipping()
    {
        return $this->belongsTo(Shipping::class);
    }

    public function detail()
    {
        return $this->hasMany(OrderDetail::class);
    }

    public function remittance()
    {
        return $this->hasOne(Remittance::class);
    }

}
