<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeBanner extends Model
{
    protected $fillable = [
        'src', 'mobile_src', 'sort', 'state', 'delete'
    ];
}
