<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Footer extends Model
{
    protected $fillable = [
        'title', 'seal', 'src', 'line', 'tel'
    ];
}
