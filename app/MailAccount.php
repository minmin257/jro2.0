<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailAccount extends Model
{
    protected $fillable = [
        'subject', 'name', 'email', 'password', 'recipient', 'html'
    ];
}
