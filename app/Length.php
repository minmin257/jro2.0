<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Length extends Model
{
    protected $fillable = [
        'product_id', 'length_id', 'state', 'out', 'delete'
    ];

    public function type()
    {
        return $this->belongsTo(LengthType::class, 'length_id', 'id');
    }

}
